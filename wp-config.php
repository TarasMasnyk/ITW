<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'itw');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'UUPuoPaO%7Fl@FsxOZJg|V:-)>@{+MCy;GH-:?}wn2nSG!P+@3w%=!y|^ $ 31r5');
define('SECURE_AUTH_KEY',  'T79(%|p^M6C9O^)aIM.nRw|4-;UVdI^=c/}N5+)j>({4uyEoS]]=P++BSEsQH[wC');
define('LOGGED_IN_KEY',    'kk^!I@5}{,g6yvZjkG?PiL9w?/z+}rde=YTWkxhEs}m<1IiaMJnc,}$[I4J]KYBk');
define('NONCE_KEY',        'oAW46- +O7z+EV-|*@uuf|E{#pn/mPdFHwx@<x/9H1>_2|}r`^<rbi+ $U;?ttk/');
define('AUTH_SALT',        '}i$_FEOdXrT|*MIg]*UMK#FwYEcIQ@cmAkm]s|b<W[b+1*A Njie!YfSWS3 |d+u');
define('SECURE_AUTH_SALT', '3SGbCYdV0KkI/x-EA-!v{AiLN%AH@kd+u#sJ-0c8KQDAMah._]|UpLY}{b{FpoWT');
define('LOGGED_IN_SALT',   ':/4?9oJt+A,q+@Ao]E/Wo$cxWoKj=}f>|]gA0leNuQ=(0p+%IP?pYzwxRw+xdL^-');
define('NONCE_SALT',       '}t5n|t-n(fO?-Rh|c6ya[pu= .]S~4I`-<M.LY?[okS;/>n^+g-*stX;6~an5E3;');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
