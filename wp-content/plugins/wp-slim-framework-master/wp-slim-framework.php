<?php
/**
 * Created by JetBrains PhpStorm.
 * Date: 7/30/13
 * Time: 10:20 AM
 * Plugin Name: Wordpress Slim framework
 * Description: Slim framework integration with Wordpress
 * Version: 1.0
 * Author: Constantin Botnari
 * License: GPLv2
 */
require_once 'Slim/Slim.php';
require_once 'SlimWpOptions.php';

\Slim\Slim::registerAutoloader();
new \Slim\SlimWpOptions();

add_filter('rewrite_rules_array', function ($rules) {
    $new_rules = array(
        '('.get_option('slim_base_path','slim/api/').')' => 'index.php',
    );
    $rules = $new_rules + $rules;
    return $rules;
});

add_action('init', function () {
    if (strstr($_SERVER['REQUEST_URI'], get_option('slim_base_path','slim/api/'))) {
        $slim = new \Slim\Slim();
        do_action('slim_mapping',$slim);
        $slim->run();
        exit;
    }
});

add_action('slim_mapping',function($slim){
    $slim->post('/slim/api/object/',function(){
        $arrayKeys = array(
                'length',
                'source_power',
                'tip_otroshartel',
                'tip_otroshartel_d',
                'mount',
                'source_power_in',
                'tipe_streat',
                'tipe_rast',
                'tipe_lamp_rast', );


        $fieldKeys = array(
//            'length',
//            'source_power',
//            'tip_otroshartel',
//            'tip_otroshartel_d',
//            'mount',
//            'source_power_in',
//            'tipe_streat',
//            'tipe_rast',
//            'tipe_lamp_rast',
            'imege_slid_1',
            'imege_slid_2',
            'imege_slid_3',
            'imege_slid_4',
            'imege_slid_5',
            'imege_slid_6',
            'single_middle_1',
            'single_middle_2',
            'single_middle_3',
            'single_middle_4',
            'single_middle_5',
            'single_middle_6',
            'single_middle_7',
            'download_description',
            'download_catalog',
            'item_detaile_1',
            'item_detaile_2',
            'item_detaile_3',
            'item_detaile_4',
            'item_detaile_5',
            'item_detaile_6',
            'item_detaile_7',
            'item_detaile_8',
            'item_detaile_9',
            'item_detaile_10',
            'item_detaile_11',
            'item_detaile_12', );

        $q_args = array(
            'post_type'         => "item",
            'type'         =>  $_POST['taxonomy'],
            'meta_query'        => array('relation ' => 'AND'),
        );
        foreach($arrayKeys as $key) {
            $q_args['meta_query'][] = checkParameter($key);
        }

        $query = new WP_Query($q_args);
        $postCustomFields = array();
        foreach($query->posts as $post) {
            foreach($fieldKeys as $key) {
                if(get_field($key, $post->ID))
                    $postCustomFields[$key] = get_field($key, $post->ID);

            }
        }

        echo json_encode($postCustomFields);
    });
});

function checkParameter($key) {
    if ( array_key_exists($key, $_POST)) {
        return array(
                'key' => $key,
                'value' => serialize(array($_POST[$key])),

        );
    }
}