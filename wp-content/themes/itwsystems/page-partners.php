<?php /* Template Name: Partners [Template] */ ?>
<?php get_header(); ?>
    <section class="mainContent__block block__content">
        <h2 class="block__title"><?php the_title(); ?></h2>
        <section class="mainContainer text text-center">
            <?php
            if (have_posts()) while (have_posts())
            {
                the_post();
                the_content();
            }
            ?>
        </section>
    </section>
<!--
    <section class="mainContent__block block__partners__get">
        <h3 class="block__title">Что же получают наши партнеры?</h3>

        <div class="advantages__block__items mainContainer">
            <div class="flux_box">
                <div class="advantages__block__item">
                    <span class="item__icon icon_pig"></span>
                    <p>Lorem lipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
            </div>
            <div class="flux_box">
                <div class="advantages__block__item">
                    <span class="item__icon icon_fulltime"></span>
                    <p>Lorem lipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
            </div>
            <div class="flux_box">
                <div class="advantages__block__item">
                    <span class="item__icon icon_moneyrevert"></span>
                    <p>Lorem lipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
            </div>
            <div class="flux_box">
                <div class="advantages__block__item">
                    <span class="item__icon icon_payments"></span>
                    <p>Lorem lipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
            </div>
            <div class="flux_box">
                <div class="advantages__block__item">
                    <span class="item__icon icon_light"></span>
                    <p>Lorem lipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
            </div>
            <div class="flux_box">
                <div class="advantages__block__item">
                    <span class="item__icon icon_speed"></span>
                    <p>Lorem lipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
            </div>
            <div class="flux_box">
                <div class="advantages__block__item">
                    <span class="item__icon icon_unlim"></span>
                    <p>Lorem lipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
            </div>
            <div class="flux_box">
                <div class="advantages__block__item">
                    <span class="item__icon icon_15years"></span>
                    <p>Lorem lipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </section>

    <section class="mainContent__block block__development__dynamics">
        <h3 class="block__title">Динамика развития компании</h3>
        <section class="mainContainer">
            <div class="block__dynamic__development__aside__l aside">
                <img src="http://placehold.it/170x170" />
                <div class="dynamics__list">
                    <p class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit commodo.</p>
                    <p class="text">Duis eget lorem condimentum lacus eleifend bibendum ac sed nisl.</p>
                    <p class="text">Cras congue eros augue, ut ullamcorper magna fringilla quis.</p>
                </div>
            </div>
            <div class="block__dynamic__development__aside__r aside">
                <img src="http://placehold.it/170x170" />
                <div class="dynamics__list">
                    <p class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit commodo.</p>
                    <p class="text">Duis eget lorem condimentum lacus eleifend bibendum ac sed nisl.</p>
                    <p class="text">Cras congue eros augue, ut ullamcorper magna fringilla quis.</p>
                </div>
            </div>
            <div class="clearfix"></div>
        </section>
    </section>

    <section class="mainContent__block block__experts__feature">
        <h3 class="block__title">Прогнозы наших експертов</h3>
        <section class="mainContainer">
            <div class="block__experts__feature__aside__l aside">
                <img src="http://placehold.it/170x170" />
                <div class="features__list">
                    <p class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit commodo.</p>
                    <p class="text">Duis eget lorem condimentum lacus eleifend bibendum ac sed nisl.</p>
                    <p class="text">Cras congue eros augue, ut ullamcorper magna fringilla quis.</p>
                </div>
            </div>
            <div class="block__experts__feature__aside__r aside">
                <img src="http://placehold.it/170x170" />
                <div class="features__list">
                    <p class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit commodo.</p>
                    <p class="text">Duis eget lorem condimentum lacus eleifend bibendum ac sed nisl.</p>
                    <p class="text">Cras congue eros augue, ut ullamcorper magna fringilla quis.</p>
                </div>
            </div>
            <div class="clearfix"></div>
        </section>
    </section>

    <section class="mainContent__block block__partners__become">
        <h3 class="block__title">Как стать партнером?</h3>
        <section class="mainContainer">
            <p class="text">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec viverra lorem fermentum ex faucibus sodales.
                Morbi in ante ullamcorper, faucibus odio in, efficitur odio. Fusce porta magna vitae sollicitudin porta.
                In hac habitasse platea dictumst. Sed at pharetra mi, at suscipit lacus. Nunc dignissim fermentum velit non suscipit.
                Ut id mauris convallis diam laoreet semper. Ut pharetra arcu a dictum condimentum.
            </p>

            <section class="become__partner__steps">
                <div class="become__partner__steps__item">
                    <div class="item__step">
                        <span class="item__icon"><img src="<?php echo get_template_directory_uri(); ?>/images/icons_png/icon_speed.png" /></span>
                        <span class="item__step__title"><span>1</span> шаг</span>
                    </div>
                    <p class="text text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
                <div class="become__partner__steps__item">
                    <div class="item__step">
                        <span class="item__icon"><img src="<?php echo get_template_directory_uri(); ?>/images/icons_png/service_office.png" /></span>
                        <span class="item__step__title"><span>2</span> шаг</span>
                    </div>
                    <p class="text text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
                <div class="become__partner__steps__item">
                    <div class="item__step">
                        <span class="item__icon"><img src="<?php echo get_template_directory_uri(); ?>/images/icons_png/icon_unlim.png" /></span>
                        <span class="item__step__title"><span>3</span> шаг</span>
                    </div>
                    <p class="text text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
                <div class="become__partner__steps__item">
                    <div class="item__step">
                        <span class="item__icon"><img src="<?php echo get_template_directory_uri(); ?>/images/icons_png/icon_moneyrevert.png" /></span>
                        <span class="item__step__title"><span>4</span> шаг</span>
                    </div>
                    <p class="text text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
                <div class="clearfix"></div>
            </section>


        </section>
    </section>

    <section class="mainContent__block block__partners__requires">
        <h3 class="block__title">Требования к партнерам</h3>
        <section class="mainContainer">
            <div class="flux_box">
                <p class="require_step">
                    <span class="fa fa-check"></span>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                </p>
                <div class="requires__list">
                    <p class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    <p class="text">Duis eget lorem condimentum lacus eleifend.</p>
                    <p class="text">Cras congue eros augue, ut ullamcorper magna.</p>
                </div>
            </div>
            <div class="flux_box">
                <p class="require_step">
                    <span class="fa fa-check"></span>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                </p>
                <div class="requires__list">
                    <p class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    <p class="text">Duis eget lorem condimentum lacus eleifend.</p>
                    <p class="text">Cras congue eros augue, ut ullamcorper magna.</p>
                </div>
            </div>
            <div class="flux_box">
                <p class="require_step">
                    <span class="fa fa-check"></span>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                </p>
                <div class="requires__list">
                    <p class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    <p class="text">Duis eget lorem condimentum lacus eleifend.</p>
                    <p class="text">Cras congue eros augue, ut ullamcorper magna.</p>
                </div>
            </div>
            <div class="clearfix"></div>
        </section>
    </section>

    <section class="mainContent__block block__contacts white">
        <h3 class="block__title">Станьте нашим партнером</h3>
        <section class="mainContainer flux_box">
            <?php include ("includes/contacts.form.php"); ?>
        </section>
    </section>-->
<?php get_footer(); ?>