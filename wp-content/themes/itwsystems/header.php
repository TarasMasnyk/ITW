<?php require_once "includes/language.php" ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
    <title><?php bloginfo('name'); ?><?php wp_title(); ?></title>
    <meta name="viewport" content="width=device-width">
    <meta name="revisit-after" content="1 day" />
    <meta name="description" content="">
    <meta name="keywords" content="">

    <meta name="viewport" content="initial-scale=1.0, user-scalable=no, maximum-scale=1" />

    <link rel="profile" href="http://gmpg.org/xfn/11">

    <link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php bloginfo('rss2_url'); ?>" />
    <link rel="alternate" type="text/xml" title="RSS .92" href="<?php bloginfo('rss_url'); ?>" />
    <link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="<?php bloginfo('atom_url'); ?>" />
    <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/images/favicon.gif" />

    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

    <!-- Bootstrap :: Styles & Icons -->
    <link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/libs/bootstrap/css/bootstrap.css" media="screen" />
    <link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/libs/bootstrap/css/bootstrap-theme.css" media="screen" />

    <!-- CSS :: JavaScript Libs -->
    <link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/libs/slideme/slideme.css" media="screen" />
    <link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/libs/fonts/awesome/scss/font-awesome.css" media="screen" />
    <link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/libs/twentytwenty/css/foundation.css" media="screen" />
    <link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/libs/twentytwenty/css/twentytwenty.css" media="screen" />
    <link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/libs/colorbox/colorbox.css" media="screen" />
    <link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/libs/ion/css/ion.rangeSlider.css" media="screen" />
    <link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/libs/ion/css/ion.rangeSlider.skinFlat.css" media="screen" />

    <!-- STYLES :: Default styles -->
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
    <link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/theme.css" media="screen" />
    <link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/customstyle.css" media="screen" />

     
    <!-- LIBS :: JavaScript Libs -->
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/libs/jquery/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/libs/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/libs/slideme/jquery.slideme.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/libs/twentytwenty/jquery.event.move.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/libs/twentytwenty/jquery.twentytwenty.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/libs/colorbox/jquery.colorbox.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/libs/ion/ion.rangeSlider.js"></script>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <!-- LIBS :: jQuery Ui -->
      <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/libs/jQuery_ui/jquery-ui.min.js"></script>
    <!--BXSlider-->
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/libs/js/jquery.bxslider.min.js"></script>
    <link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/jquery.bxslider.css" media="screen" />
    <!--Carousel-->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/owl.carousel.min.js"></script>
    <!--Cockies-->
        <script src="<?php echo get_template_directory_uri(); ?>/libs/js/js.cookie.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/libs/js/jquery.mousewheel.min.js"></script>
        
       <!--  <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/assets/owl.carousel.min.css" media="screen" /> -->
         <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/assets/owl.theme.default.min.css" media="screen" />
       <script>
            $(document).ready(function()
            {
                $('.bxslider').bxSlider();
            });
        </script>
    <style type="text/css">
        <?php $FS = explode('/', get_option('theme_fontslh', "14px/20px")); ?>
        .mainContent__block .text {
            font-size: <?php echo $FS[0]; ?>;
            line-height: <?php echo $FS[1]; ?>;
        }
        <?php if (is_user_logged_in()) { ?> .mainHeader {top: 30px;} <?php } ?>

        .mainContent__block {min-height: 500px;}
    </style>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <header class="mainHeader">
        <section class="mainContainer">
            <div class="logotype">
                <a href="/">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/g.logo.png" />
                </a>
            </div>
            <?php $l = getLangM(); ?>
            <?php
            wp_nav_menu(array(
                'theme_location'  => 'mainMenu',
                'menu'            => 'mainMenu',
                'container'       => 'nav',
                'container_class' => 'mainHeader__navigation',
                'container_id'    => '',
                //'menu_class'      => 'menu',
                'menu_id'         => 'mainMenu',
                'echo'            => true,
                'fallback_cb'     => 'wp_page_menu',
                'items_wrap'      => '<ul id="%1$s" class="%2$s list-unstyled">%3$s</ul>',
                'depth'           => 0
            ));
            ?>

            
            <div class="clearfix"></div>
        </section>
        <aside class="searchToolBar">
                <span class="glyphicon glyphicon-search" id="searchbarButton"></span>
                <form action="<?php bloginfo('siteurl'); ?>" method="get" id="searchform">
                    <div class="genericSearchBox">

                        <label><span class="icon"><i class="fa fa-search"></i></span><input type="search" id="s" name="s" <?php echo $l == 'ru' ? 'placeholder="Найти.."' : 'placeholder="Search.."'; ?>/></label>
                    </div>
                </form>
            </aside>
        <div class="setLanguage" >
            
            <span><?php echo strtoupper($l); ?></span>
            <ul class="list-unstyled" >
                <li><a href="/en/">EN</a></li>
                <li><a href="/ru/">RU</a></li>
            </ul>
        </div>
        <script>
            $('.setLanguage').hover(function() {
                $( this ).find( "ul" ).stop( true, true ).fadeIn();
            }, function() {
                $( this ).find( "ul" ).stop( true, true ).fadeOut();
            });
        </script>
        
        

    </header>