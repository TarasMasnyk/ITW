<?php /* Template Name: Home [Template] */ ?>
<?php get_header(); ?>

    <section class="mainContent_Slider custom_style" id="top_slider">
        <ul class="mainContent_Slider_list slideme list-unstyled">
            <?php $l = getLangM(); ?>
            
            <?php
            $fa = get_post_custom(get_the_ID());

            $_mData = array();
            foreach ($fa as $_k => $_v)
            {
                if (substr($_k, 0, 6) == "slider")
                {
                    $_mData[substr($_k, 0, 8)][substr($_k, 9)] = str_replace(array("(Русский) ", "(Русский)", "(English) ", "(English)"), "", $_v[0]);
                }
            }

            foreach ($_mData as $slider => $slider_item)
            {
                ?>
                <li data-sliderID="<?php echo $slider; ?>" class="list_style">
                    <div class="mainContainer top_slider_style">
                            <h2 class="abstract-title" style="font-size: <?php echo $slider_item['title_fs']; ?>;"><?php echo $slider_item['title']; ?></h2>
                            <!-- <p><?php echo $slider_item['subtitle']; ?></p> -->
                        </div>
                        <div class="button_container">
                            <div class="button_block">
                                <div class="buttom_area catalog_button">
                                    
                                    <a class="button btn-big btn-yellow" href="/item/"><span class="img_catalog_button"></span><span class="btn-light"></span>
                                    <p><?php echo $l == 'ru' ? "Каталог " : "Catalog"; ?></p></a>
                                </div>
                                <div class="buttom_area project_button">
                                    
                                    <a class="button btn-big btn-green" href="/project/"><span class="img_project_button"></span>
                                    <p><?php echo $l == 'ru' ? "Проекты" : "Our Projects"; ?></p></a>
                                </div>
                            </div>
                        </div>
                        <div class="glass_background">
                            <div class="title_background">
                             <img src="<?php echo $slider_item['image']; ?>" />
                            </div>
                        </div>
                </li>
                <?php
            }
            ?>
        </ul>
    </section>
    <section class="mainContent__block block__clients">
        <div class="mainContainer">
            <h2 class="block__title"><?php echo $l == 'ru' ? "Наши клиенты" : "Our clients"; ?></h2>
            <ul class="block__clients__items list-unstyled">
                <li class="item"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/clients/bat.png" /></a></li>
                <li class="item"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/clients/cola.png" /></a></li>
                <li class="item"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/clients/kraftfoods.png" /></a></li>
                <li class="item"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/clients/metalvis.png" /></a></li>
                <li class="item"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/clients/ekol.png" /></a></li>
                <li class="item"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/clients/fozzy.png" /></a></li>
            </ul>
            <a class="block__more-link" href="/clients/"><i class="fa fa-angle-double-down"></i> <span><?php echo $l == 'ru' ? "Все клиенты" : "All customers"; ?></span></a>
        </div>
    </section>
    <section class="mainContent__block block__inform">
        <div class="block_employer">
            <div class="cover_block">
                <div class="hover_header">
                    <div class="inform_header">
                        <h2 class="block__title"><?php echo $l == 'ru' ? "Руководителю" : "For СЕО"; ?></h2>
                        <span class="img_employer"></span>
                    </div>
                </div>
                <div class="block_content hover_employer_content">
            <div class="employer" >
                <?php
                $the_engineer=get_field('ceo_text');              
                 echo $the_engineer;?>
            </div>
        </div>
        </div>
    </div>
         <div class="block_engineer">  
         <div class="cover_block"> 
            <div class="hover_header">
                <div class="inform_header">
                    <h2 class="block__title"><?php echo $l == 'ru' ? "Инженеру" : "For Engineer"; ?></h2>
                    <span class="img_engineer"></span>
                </div>
            </div>
                <div class="block_content hover_engineer_content">
            <div class="engineer">

               <div id="accordion">
                    <?php for ($i=1; $i < 22; $i++) : ?>
                    <?php  if ( get_field(getField("label_", "$i")) ):  ?>
                         <h3><?php the_field(getField("label_", "$i")); ?></h3>
                        <?php endif; ?>
                       <?php  if ( get_field(getField("text_", "$i"))) : ?>

                        <div><p><?php the_field(getField("text_", "$i")); ?></p></div>
                        <?php endif; ?>
                    <?php endfor; ?>
                </div> 
            </div> 
            </div>
        </div>
    </div>
        <div class="clearfix"></div> 
    </section>
    <!--<section class="mainContainer">
        <?php
        # background: white url(../images/advantages.block.jpg) center center no-repeat;
        if ($_COOKIE['qtrans_front_language'] == 'en') { ?>
        <section class="advantages__block" style="background: white url(<?php echo get_template_directory_uri(); ?>/images/advantages.block_en.jpg) center center no-repeat;">
        <?php } else { ?>
        <section class="advantages__block" style="background: white url(<?php echo get_template_directory_uri(); ?>/images/advantages.block.jpg) center center no-repeat;">
        <?php } ?>
            <?php
            unset ($_mData); $_mData = array();
            foreach ($fa as $_k => $_v)
            {
                if (substr($_k, 0, 9) == "advantage")
                {
                    $_mData[substr($_k, 0, 11)][substr($_k, 12)] = str_replace(array("(Русский) ", "(Русский)", "(English) ", "(English)"), "", $_v[0]);
                }
            }

            foreach ($_mData as $advantage => $advantage_item)
            {
                ?>
                <div class="advantage__item qu<?php echo $advantage; ?>">
                    <span class="advantage_title"><?php echo $advantage_item['title']; ?></span>
                    <span class="advantage_icon"><img src="<?php echo get_template_directory_uri(); ?>/images/icons_png/<?php echo $advantage_item['icon']; ?>"</span>
                </div>
                <?php
            }
            ?>
        </section>
    </section>-->
    <!--Калькулятор-->
   <!-- <section class="mainContent__block block__constructor">
        <h2 class="block__title">Расчет вашего проекта</h2>
        <section class="mainContainer"><?php include ("includes/cc_m.calculator.php"); ?></section>
    </section>-->
     
     <section class="mainContent__block block__works">
        <h2 class="block__title"><?php echo $l == 'ru' ? "Наши работы" : "Our projects"; ?></h2>
        <div class="mainContent_Slider slider__pf__wrapper" id="pf_slider">
            <ul class="mainContent_Slider_list slideme list-unstyled" id="mainSlider">
                <?php

                  $args = array('showposts'=>5, 'post_type' => 'project');
                  
                  $query = new WP_Query($args);
                 
                  while($query -> have_posts()) : $query -> the_post();
                    $logoUrl = get_post_custom_values('logo', $post->ID); 
                    $ft_befor= get_post_custom_values('ft_befor',$post->ID);
                    $threeD_befor= get_post_custom_values('threeD_befor',$post->ID);
                    $ft_after= get_post_custom_values('ft_after',$post->ID);
                    $threeD_after= get_post_custom_values('threeD_after',$post->ID);
                    $number_of_lamps= get_post_custom_values('number_of_lamps',$post->ID);
                    $square=get_post_custom_values('square',$post->ID);
                    $start_year=get_post_custom_values('start_year', $post->ID);
                    $the_cats= get_the_terms( $post->ID, 'object' );
                    foreach($the_cats as $the_cat){
                     $the_single_cat=$the_cat->name;}
                    ?>
                  
                <li>
                    <div class="background_slider">
                        <div class="mainContainer">
                            <div class="slider__pf " id="twentytwenty-id-<?php the_ID(); ?>">
                                <img data-href="<?= $ft_befor[0]; ?>" data-src="<?= $threeD_befor[0]; ?>" src="<?= $threeD_befor[0]; ?>">
                                <img data-href="<?= $ft_after[0]; ?>" data-src="<?= $threeD_after[0]; ?>" src="<?= $threeD_after[0]; ?>">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="slider_infor">
                            <div class="slider_infor_logo">
                                <a href="<?php the_permalink(); ?>"><img src="<?= $logoUrl[0]; ?>"></a>
                            </div>
                            <div class="slider_infor_content">
                                <h3><?php echo $the_single_cat; ?></h3>
                                 <ul>
                                     <li><?php echo $l == 'ru' ? "Обьект" : "Project"; ?>: <?php the_title(); ?></li>
                                     <li><?php echo $l == 'ru' ? "Площадь" : "Area"; ?>: <span><?php echo $square[0]; ?></span></li>
                                     <li><?php echo $l == 'ru' ? "Количество светильников" : "Number of lights"; ?>: <span><?php echo $number_of_lamps[0]; ?></span></li>
                                     <li><?php echo $l == 'ru' ? "Год внедрения" : "Year of implementation"; ?>: <span><?php echo $start_year[0]; ?></span></li>
                                 </ul>
                            </div>
                        </div>
                    </div>
                </li>
                <?php 
                endwhile; ?>
            </ul>
        </div>
    </section>

     <section class="mainContent__block block__about">
         
        <div class="mainContainer text">
            <div class="about">
                <h2 class="block__title"><?php echo $l == 'ru' ? "О нас" : "About Us"; ?></h2>
                <p><?php echo $l == 'ru' ? 
                    "С 2010 года мы помогаем нашим партнерам минимизировать затраты на электроэнергию благодаря 
                    эффективным и безопасным для человека и природы системам светодиодного освещения. Главный 
                    офис и производство находятся в Украине. Наши офисы расположены в США и Европе, в частности 
                    наши представительства находятся в Польше, Словакии, Эстонии. Мы работаем для международных 
                    и локальных компаний." 
                    : 
                    "ITW SYSTEMS is an ECO-friendly company, specializing in design and implementation of energy-saving
                     lighting equipment and solutions for the commercial, municipal and government sectors.
                     Since 2010, we help our partners to minimize energy costs through e ec ve and safe for 
                     humans and environment systems of LED ligh ng. The headquarters is located in the USA, engineer and 
                     manufacturing team is located in Ukraine. We have rep o ces in Poland, Slovakia and Estonia. We work 
                     with the multinational and local companies. "; ?></p>
            </div>
            <div class="why__we">
                <h3 class="block__title"><?php echo $l == 'ru' ? "Почему мы:" : "Why ITW-SYSTEMS:"; ?></h3>
                <ul>
                    <li><?php echo $l == 'ru' ? "Более 500 энергоэффективных проектов." : "More than 500 large energy-efficient projects realized."; ?></li>
                    <li><?php echo $l == 'ru' ? "Собственное высокотехнологичное производство." : "Our own high-tech manufacturing"; ?></li>
                    <li><?php echo $l == 'ru' ? "Самая надежная элементная база - OSRAM, LG, CREE, NICHICON, SEOUL." : "The most reliable components - OSRAM, LG, CREE, NICHICON, SEOUL."; ?></li>
                    <li><?php echo $l == 'ru' ? "Индивидуальное решение для каждого клиента." : " Customized and optimized solution for each client."; ?></li>
                    <li><?php echo $l == 'ru' ? "Гарантия не менее 5 лет." : "Warranty of at least 5 years."; ?></li>
                    <li><?php echo $l == 'ru' ? "Мы инвестируем в разработку новых моделей светильников." : "We invest in development of new products."; ?></li>
                </ul>
            </div> 
            <div class="clearfix"></div>      
        </div> 
        <div class="block__about_background"></div>
      
    </section>
<?php get_footer(); ?>