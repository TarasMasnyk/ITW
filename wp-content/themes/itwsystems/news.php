<?php /* Template Name: Blog [Template] */ ?>
<?php get_header(); ?>
    <section class="news__container">
        <section class="mainContainer">
            <h2 class="main__title"><span><?php the_title(); ?></span></h2>
            <section class="news__posts">

                <?php
                $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

                $args = array(
                    'posts_per_page'    => 6,
                    'cat'               => 16,
                    'orderby'           => 'name',
                    'order'             => 'ASC',
                    'paged'             => $paged
                );

                $custom_query = new WP_Query( $args );

                while($custom_query->have_posts()) {
                    $custom_query->the_post();

                    $thumbnail_attributes = wp_get_attachment_image_src(get_post_thumbnail_id(), 'source');
                    ?>
                    <article class="news__article">
                        <picture class="news__article__picture">
                            <a href="<?php the_permalink() ?>"><img src="<?php echo $thumbnail_attributes[0]; ?>" /></a>
                        </picture>
                        <div class="news__article__content">
                            <div class="news__article__title"><?php the_title(); ?></div>
                            <div class="text"><?php the_content(); ?></div>

                            <div class="news__article__data"><?php echo get_the_date("j F Y"); ?></span></div>
                            <div class="news__article__counters">
                                <span class="news__article__counters__views"><i class="fa fa-eye"></i> <?php echo getPostViews(get_the_ID()); ?></span>
                                <span class="news__article__counters__comments"><i class="fa fa-comments"></i> <?php echo get_comments_number(get_the_ID()); ?></span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </article>
                    <?php
                }

                pagination($custom_query->max_num_pages);
                ?>
            </section>
            <?php include("includes/news_aside.php"); ?>
            <div class="clearfix"></div>
        </section>
    </section>
<?php get_footer(); ?>