<?php /* Template Name: Career [Template] */ ?>
<?php get_header(); ?>
    <section class="mainContent__block block__content">
        <h2 class="block__title"><?php the_title(); ?></h2>		
 <section class="mainContent__block block__contacts block__career">
        <h3 class="block__title">Монтажник-электрик</h3>
        <h4 class="text-center">(45 000 рублей)</h4>
        <section class="mainContainer">
            <div class="block__contacts__aside__l career_text__block">
                <h5>Требования:</h5>
                <p class="tick-list text"><span>1</span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam at velit eu nulla iaculis congue a non nisl. Nam auctor semper scelerisque.</p>
                <p class="tick-list text"><span>2</span>Pellentesque est velit, pulvinar a orci a, euismod egestas diam. Vestibulum fringilla nisl nulla, vel vestibulum turpis convallis et.</p>
                <p class="tick-list text"><span>3</span>Morbi non mauris mauris. In id dignissim ligula.</p>
                <p class="tick-list text"><span>4</span>Suspendisse convallis velit ac risus vestibulum rutrum. Nam aliquet est a purus porttitor, id varius quam pretium. Nam dictum laoreet sem, eget ultrices magna maximus scelerisque. Donec id ligula nulla. Aliquam dictum facilisis consectetur. Etiam nulla quam, volutpat eget posuere in, suscipit vel mi.</p>
            </div>
            <div class="block__contacts__aside__r career_text__block">
                <h5>Обязонности:</h5>
                <p class="tick-list text"><span>1</span><b>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam at velit eu nulla iaculis congue a non nisl. Nam auctor semper scelerisque.</b></p>
                <p class="tick-list text"><span>2</span><b>Pellentesque est velit, pulvinar a orci a, euismod egestas diam. Vestibulum fringilla nisl nulla, vel vestibulum turpis convallis et.</b></p>
                <p class="tick-list text"><span>3</span><b>Morbi non mauris mauris. In id dignissim ligula.</b></p>
                <p class="tick-list text"><span>4</span><b>Suspendisse convallis velit ac risus vestibulum rutrum. Nam aliquet est a purus porttitor, id varius quam pretium. Nam dictum laoreet sem, eget ultrices magna maximus scelerisque. Donec id ligula nulla. Aliquam dictum facilisis consectetur. Etiam nulla quam, volutpat eget posuere in, suscipit vel mi.</b></p>
            </div>
            <div class="text-center">
                 <button class="button btn-big btn-yellow text-center" type="submit">Отправить заявку</button>
            </div>
            <div class="clearfix"></div>
        </section>
    </section>
    <section class="mainContent__block block__contacts block__career">
        <h3 class="block__title">Монтажник-электрик</h3>
        <h4 class="text-center">(45 000 рублей)</h4>
        <section class="mainContainer">
            <div class="block__contacts__aside__l career_text__block">
                <h5>Требования:</h5>
                <p class="tick-list text"><span>1</span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam at velit eu nulla iaculis congue a non nisl. Nam auctor semper scelerisque.</p>
                <p class="tick-list text"><span>2</span>Pellentesque est velit, pulvinar a orci a, euismod egestas diam. Vestibulum fringilla nisl nulla, vel vestibulum turpis convallis et.</p>
                <p class="tick-list text"><span>3</span>Morbi non mauris mauris. In id dignissim ligula.</p>
                <p class="tick-list text"><span>4</span>Suspendisse convallis velit ac risus vestibulum rutrum. Nam aliquet est a purus porttitor, id varius quam pretium. Nam dictum laoreet sem, eget ultrices magna maximus scelerisque. Donec id ligula nulla. Aliquam dictum facilisis consectetur. Etiam nulla quam, volutpat eget posuere in, suscipit vel mi.</p>
            </div>
            <div class="block__contacts__aside__r career_text__block">
                <h5>Обязонности:</h5>
                <p class="tick-list text"><span>1</span><b>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam at velit eu nulla iaculis congue a non nisl. Nam auctor semper scelerisque.</b></p>
                <p class="tick-list text"><span>2</span><b>Pellentesque est velit, pulvinar a orci a, euismod egestas diam. Vestibulum fringilla nisl nulla, vel vestibulum turpis convallis et.</b></p>
                <p class="tick-list text"><span>3</span><b>Morbi non mauris mauris. In id dignissim ligula.</b></p>
                <p class="tick-list text"><span>4</span><b>Suspendisse convallis velit ac risus vestibulum rutrum. Nam aliquet est a purus porttitor, id varius quam pretium. Nam dictum laoreet sem, eget ultrices magna maximus scelerisque. Donec id ligula nulla. Aliquam dictum facilisis consectetur. Etiam nulla quam, volutpat eget posuere in, suscipit vel mi.</b></p>
            </div>
            <div class="text-center">
                <button class="button btn-big btn-yellow text-center" type="submit">Отправить заявку</button>
            </div>
            <div class="clearfix"></div>
        </section>
    </section>
    <section class="mainContent__block block__contacts block__career">
        <h3 class="block__title">Монтажник-электрик</h3>
        <h4 class="text-center">(45 000 рублей)</h4>
        <section class="mainContainer">
            <div class="block__contacts__aside__l career_text__block">
                <h5>Требования:</h5>
                <p class="tick-list text"><span>1</span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam at velit eu nulla iaculis congue a non nisl. Nam auctor semper scelerisque.</p>
                <p class="tick-list text"><span>2</span>Pellentesque est velit, pulvinar a orci a, euismod egestas diam. Vestibulum fringilla nisl nulla, vel vestibulum turpis convallis et.</p>
                <p class="tick-list text"><span>3</span>Morbi non mauris mauris. In id dignissim ligula.</p>
                <p class="tick-list text"><span>4</span>Suspendisse convallis velit ac risus vestibulum rutrum. Nam aliquet est a purus porttitor, id varius quam pretium. Nam dictum laoreet sem, eget ultrices magna maximus scelerisque. Donec id ligula nulla. Aliquam dictum facilisis consectetur. Etiam nulla quam, volutpat eget posuere in, suscipit vel mi.</p>
            </div>
            <div class="block__contacts__aside__r career_text__block">
                <h5>Обязонности:</h5>
                <p class="tick-list text"><span>1</span><b>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam at velit eu nulla iaculis congue a non nisl. Nam auctor semper scelerisque.</b></p>
                <p class="tick-list text"><span>2</span><b>Pellentesque est velit, pulvinar a orci a, euismod egestas diam. Vestibulum fringilla nisl nulla, vel vestibulum turpis convallis et.</b></p>
                <p class="tick-list text"><span>3</span><b>Morbi non mauris mauris. In id dignissim ligula.</b></p>
                <p class="tick-list text"><span>4</span><b>Suspendisse convallis velit ac risus vestibulum rutrum. Nam aliquet est a purus porttitor, id varius quam pretium. Nam dictum laoreet sem, eget ultrices magna maximus scelerisque. Donec id ligula nulla. Aliquam dictum facilisis consectetur. Etiam nulla quam, volutpat eget posuere in, suscipit vel mi.</b></p>
            </div>
            <div class="text-center">
                <button class="button btn-big btn-yellow text-center" type="submit">Отправить заявку</button>
             </div>
            <div class="clearfix"></div>
        </section>
    </section>
    <section class="mainContent__block block__contacts block__career">
        <h3 class="block__title">Монтажник-электрик</h3>
        <h4 class="text-center">(45 000 рублей)</h4>
        <section class="mainContainer">
            <div class="block__contacts__aside__l career_text__block">
                <h5>Требования:</h5>
                <p class="tick-list text"><span>1</span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam at velit eu nulla iaculis congue a non nisl. Nam auctor semper scelerisque.</p>
                <p class="tick-list text"><span>2</span>Pellentesque est velit, pulvinar a orci a, euismod egestas diam. Vestibulum fringilla nisl nulla, vel vestibulum turpis convallis et.</p>
                <p class="tick-list text"><span>3</span>Morbi non mauris mauris. In id dignissim ligula.</p>
                <p class="tick-list text"><span>4</span>Suspendisse convallis velit ac risus vestibulum rutrum. Nam aliquet est a purus porttitor, id varius quam pretium. Nam dictum laoreet sem, eget ultrices magna maximus scelerisque. Donec id ligula nulla. Aliquam dictum facilisis consectetur. Etiam nulla quam, volutpat eget posuere in, suscipit vel mi.</p>
            </div>
            <div class="block__contacts__aside__r career_text__block">
                <h5>Обязонности:</h5>
                <p class="tick-list text"><span>1</span><b>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam at velit eu nulla iaculis congue a non nisl. Nam auctor semper scelerisque.</b></p>
                <p class="tick-list text"><span>2</span><b>Pellentesque est velit, pulvinar a orci a, euismod egestas diam. Vestibulum fringilla nisl nulla, vel vestibulum turpis convallis et.</b></p>
                <p class="tick-list text"><span>3</span><b>Morbi non mauris mauris. In id dignissim ligula.</b></p>
                <p class="tick-list text"><span>4</span><b>Suspendisse convallis velit ac risus vestibulum rutrum. Nam aliquet est a purus porttitor, id varius quam pretium. Nam dictum laoreet sem, eget ultrices magna maximus scelerisque. Donec id ligula nulla. Aliquam dictum facilisis consectetur. Etiam nulla quam, volutpat eget posuere in, suscipit vel mi.</b></p>
            </div>
            <div class="text-center">
                <button class="button btn-big btn-yellow text-center" type="submit">Отправить заявку</button>
            </div>
            <div class="clearfix"></div>
        </section>
    </section>
</section>
<?php get_footer(); ?>