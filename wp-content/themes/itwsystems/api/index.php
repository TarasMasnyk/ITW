<?php
# Slim Framework
require 'Slim/Slim.php';
\Slim\Slim::registerAutoloader();

# Инициализируем Slim
$app = new \Slim\Slim();

# Warehouse Calculator (CC)

$app->post('/calculator/warehouse(/)', "c_warehouse");
$app->post('/calculator/office(/)', "c_offices");
$app->post('/mail(/)', "sendmail_catalog");
$app->post('/feedback(/)', "sendmail_feedback");
$app->post('/object/', "postDataItems");

# Запускаем Slim
$app->run();

function postDataItems() {
//    $var = query_posts(array('post_type' => 'item',
//        'type'      => $_POST['taxanomy']));
//    var_dump($var);

    echo json_encode($_POST);
}

function sendmail_catalog ()
{
    $answer = array('code' => 0, 'error_msg' => false);

    $_DATA['email'] = $_POST['email'];
    $_DATA['name'] = $_POST['name'];
    $_DATA['company'] = $_POST['company'];

    $body = '<b>E-mail</b>: ' . $_DATA['email'] . "<br />\n";
    $body .= '<b>Имя</b>: ' . $_DATA['name'] . "<br />\n";
    $body .= '<b>Компания</b>: ' . $_DATA['company'];

    if (Send("ITW Catalog", $_DATA['email'], "ITW Manager", "info@itw-systems.com", "Был запрошен каталог", $body))
    {
        $answer['code'] = 200;
    }
    else
    {
        $answer['code'] = 500;
        $answer['error_msg'] = "Не удалось отправить письмо на почту";
    }

    if($answer['code'] == 200 || !$answer['error_msg']) unset($answer['error_msg']);
    echo json_encode($answer);
}

function sendmail_feedback ()
{
    $answer = array('code' => 0, 'error_msg' => false);

    $_DATA['name'] = $_POST['bc_name'];
    $_DATA['company'] = $_POST['bc_company'];
    $_DATA['phone'] = $_POST['bc_phone'];
    $_DATA['time'] = $_POST['bc_time'];
    $_DATA['comment'] = $_POST['bc_comment'];

    $body = '<b>Имя</b>: ' . $_DATA['name'] . "<br />\n";
    $body .= '<b>Компания</b>: ' . $_DATA['company'] . "<br />\n";
    $body .= '<b>Контактный телефон</b>: ' . $_DATA['phone'] . "<br />\n";
    $body .= '<b>Удобное время для звонка</b>: ' . $_DATA['time'] . "<br />\n";
    $body .= '<b>Сообщение</b>:<br />' . $_DATA['comment'];

    $toMail = array(
        'info@itw-systems.com',
        'mailing@itw-systems.com'
    );

    foreach ($toMail as $email)
    {
        if (Send("ITW Feedback", "feedback@itw-system.com", "ITW Manager", $email, "Обратная связь", $body))
        {
            $answer['code'] = 200;
        }
        else
        {
            $answer['code'] = 500;
            $answer['error_msg'] = "Не удалось отправить письмо на почту";
            break;
        }
    }

    if($answer['code'] == 200 || !$answer['error_msg']) unset($answer['error_msg']);
    echo json_encode($answer);
}

function Send($name_from, $email_from, $name_to, $email_to, $subject, $body){
    return SendMail($name_from, $email_from, $name_to, $email_to, 'utf-8', 'KOI8-R', $subject, $body);
}

function SendMail($name_from,$email_from,$name_to,$email_to,$data_charset,$send_charset='KOI8-R',$subject,$body){
    $to = mime_header_encode($name_to, $data_charset, $send_charset) . ' <' . $email_to . '>';
    $subject = mime_header_encode($subject, $data_charset, $send_charset);
    $from = mime_header_encode($name_from, $data_charset, $send_charset) .' <' . $email_from . '>';
    if($data_charset != $send_charset){$body = iconv($data_charset, $send_charset, $body);}
    $headers = "From: $from\r\n";
    $headers .= "Content-type: text/html; charset=$send_charset\r\n";
    return mail($to, $subject, $body, $headers);
}

function mime_header_encode($str, $data_charset, $send_charset){
    if($data_charset != $send_charset){$str = iconv($data_charset, $send_charset, $str);}
    return '=?' . $send_charset . '?B?' . base64_encode($str) . '?=';
}

function c_warehouse ()
{
    $answer = array('code' => 0, 'error_msg' => false);

    foreach ($_POST as $var => $value)
    {
        switch ($var)
        {
            case 'A': {
                if ($value && !empty ($value) && is_numeric ($value)) $answer['data'][$var] = (float) $value;
                else $answer['error_msg'][] = "Field \"" . $var . "\" is missed. Please check data.";
            }
                break;
            case 'B': {
                if ($value && !empty ($value) && is_numeric ($value)) $answer['data'][$var] = (float) $value;
                else $answer['error_msg'][] = "Field \"" . $var . "\" is missed. Please check data.";
            }
                break;
            case 'Nal': {
                if ($value && !empty ($value) && is_numeric ($value)) $answer['data'][$var] = (float) $value;
                else $answer['error_msg'][] = "Field \"" . $var . "\" is missed. Please check data.";
            }
                break;
            case 'Np'     : {
                if ($value && !empty ($value) && is_numeric ($value)) $answer['data'][$var] = (float) $value;
                else $answer['error_msg'][] = "Field \"" . $var . "\" is missed. Please check data.";
            }
                break;
            case 'E'     : {
                if ($value && !empty ($value) && is_numeric ($value)) $answer['data'][$var] = (float) $value;
                else $answer['error_msg'][] = "Field \"" . $var . "\" is missed. Please check data.";
            }
                break;
            case 'hp'     : {
                if ($value == 0 || $value && !empty ($value) && is_numeric ($value)) $answer['data'][$var] = (float) $value;
                else $answer['error_msg'][] = "Field \"" . $var . "\" is missed. Please check data.";
            }
                break;
            case 'Kz'     : {
                if ($value && !empty ($value) && is_numeric ($value)) $answer['data'][$var] = (float) $value;
                else $answer['error_msg'][] = "Field \"" . $var . "\" is missed. Please check data.";
            }
                break;

            default: {}
        }
    }

    if (!$answer['error_msg'])
    {
        /**
         * | Входные данные |
         * A    - Длина аллеи
         * B    - Ширина аллеи
         * Nal  - Кол-во аллей
         * Np   - Высота подвеса светильника
         * E    - Нормируемая освещенность
         * hp   - Рабочая плоскость
         * Kz   - Коэффициент запаса
         *
         * | Сталые |
         * nUD  = 1.225;    (Коэф. Шага Д-Алюминий)
         * nUC  = 1.225;    (Коэф. Шага С-Алюминий)
         * z    = 1;        (Коэффициент неравномерности освещения)
         *
         * | Расчеты |
         * # Высота подвеса светильников над рабочей поверхностью
         * Hp = Np - hp
         *
         * # Площадь освещаемого помещения
         * [B < 3.3] S = A * B * Nal * 1,0816
         * [B = 3.3] S = A * B * Nal
         * [B > 3.3] S = A * B * Nal * 0.93
         *
         * # Кол-во светильников на 1 аллеи
         * Na = nUD * A / Hp
         *
         * # Количество аллей
         * Nb = ceil(B / Hp) * Nal
         *
         * # Количество светильников
         * N = Nb * Na
         *
         * # Расстояние между светильниками
         * T = round(A / Na, 2)
         *
         * # Индекс помещения Д-Алюминий
         * [(Nal * A * B) / (Hp * (A + B * Nal)) <= {VAL}] i = {VAL}
         *
         * # Коэффициент использования светового потока Д отражатель
         * KiD =
         *
         * # Коэффициент использования светового потока С отражатель
         * KiC =
         *
         * # Световой поток Д-Алюминий
         * [Hp <= 5.5] LfD = 0
         * LfD = (E * Kz * S * z) / (Na * KiD) / Nal
         *
         * # Световой поток C-Алюминий
         * [Hp > 11] LfC = 0
         * LfC = (E * Kz * S * z) / (Na * KiC) / Nal
         */

        if ($answer['data']['A'] < $answer['data']['B'])
        {
            $A = $answer['data']['A'];
            $answer['data']['A'] = $answer['data']['B'];
            $answer['data']['B'] = $A;
        }

        $answer['data']['NAL'] = 7;
        $answer['data']['nUD'] = 1.225;
        $answer['data']['nUC'] = 1.225;
        $answer['data']['z'] = 1;


        $answer['data']['Hp'] = $answer['data']['Np'] - $answer['data']['hp'];

        switch (true)
        {
            case ($answer['data']['B'] < 3.3): $answer['data']['S'] = round($answer['data']['A'] * $answer['data']['B'] * $answer['data']['NAL'] * 1.0816);
                break;
            case ($answer['data']['B'] = 3.3): $answer['data']['S'] = round($answer['data']['A'] * $answer['data']['B'] * $answer['data']['NAL']);
                break;
            case ($answer['data']['B'] > 3.3): $answer['data']['S'] = round($answer['data']['A'] * $answer['data']['B'] * $answer['data']['NAL'] * 0.93);
                break;
        }

        $answer['data']['Na'] = ceil($answer['data']['nUD'] * $answer['data']['A'] / $answer['data']['Hp']);
        $answer['data']['Nb'] = ceil($answer['data']['B'] / $answer['data']['Hp']) * $answer['data']['Nal'];
        $answer['data']['N'] = ceil($answer['data']['Nal'] * $answer['data']['Na']);
        $answer['data']['T'] = round($answer['data']['A'] / $answer['data']['Na'], 2);

        $KoeI = ($answer['data']['NAL'] * $answer['data']['A'] * $answer['data']['B']) / ($answer['data']['Hp'] * ($answer['data']['A'] + $answer['data']['B'] * $answer['data']['NAL']));
        $answer['data']['i'] = KoeI($KoeI);


        $answer['data']['KiD'] = KoefI($answer['data']['i'], "D");
        $answer['data']['KiC'] = KoefI($answer['data']['i'], "C");

        if ($answer['data']['Hp'] <= 5.5) $answer['data']['LfD'] = 0;
        else $answer['data']['LfD'] = ($answer['data']['E'] * $answer['data']['Kz'] * $answer['data']['S'] * $answer['data']['z']) / ($answer['data']['Na'] * $answer['data']['KiD']) / $answer['data']['NAL'];

        $answer['data']['LfD'] = ceil($answer['data']['LfD'] / 50) * 50;

        if ($answer['data']['Hp'] > 11) $answer['data']['LfC'] = 0;
        else $answer['data']['LfC'] = ($answer['data']['E'] * $answer['data']['Kz'] * $answer['data']['S'] * $answer['data']['z']) / ($answer['data']['Na'] * $answer['data']['KiC']) / $answer['data']['NAL'];

        $answer['data']['LfC'] = ceil($answer['data']['LfC'] / 50) * 50;

        $answer['code'] = 200;
    }
    else die(json_encode($answer));

    if($answer['code'] == 200 || !$answer['error_msg']) unset($answer['error_msg']);
    echo json_encode($answer);
}

function c_offices ()
{
    $answer = array('code' => 0, 'error_msg' => false);

    foreach ($_POST as $var => $value)
    {
        switch ($var)
        {
            case 'A': {
                if ($value && !empty ($value) && is_numeric ($value)) $answer['data'][$var] = (float) $value;
                else $answer['error_msg'][] = "Field \"" . $var . "\" is missed. Please check data.";
            }
                break;
            case 'B': {
                if ($value && !empty ($value) && is_numeric ($value)) $answer['data'][$var] = (float) $value;
                else $answer['error_msg'][] = "Field \"" . $var . "\" is missed. Please check data.";
            }
                break;
            case 'Np'     : {
                if ($value && !empty ($value) && is_numeric ($value)) $answer['data'][$var] = (float) $value;
                else $answer['error_msg'][] = "Field \"" . $var . "\" is missed. Please check data.";
            }
                break;
            case 'hp'     : {
                if ($value == 0 || $value && !empty ($value) && is_numeric ($value)) $answer['data'][$var] = (float) $value;
                else $answer['error_msg'][] = "Field \"" . $var . "\" is missed. Please check data.";
            }
                break;
            case 'E'     : {
                if ($value && !empty ($value) && is_numeric ($value)) $answer['data'][$var] = (float) $value;
                else $answer['error_msg'][] = "Field \"" . $var . "\" is missed. Please check data.";
            }
                break;
            case 'Kz'     : {
                if ($value && !empty ($value) && is_numeric ($value)) $answer['data'][$var] = (float) $value;
                else $answer['error_msg'][] = "Field \"" . $var . "\" is missed. Please check data.";
            }
                break;

            default: {}
        }
    }

    if (!$answer['error_msg'])
    {
        /**
         * | Входные данные |
         * A    - Длина помещения
         * B    - Ширина помещения
         * Np   - Высота подвеса светильника
         * E    - Нормируемая освещенность
         * hp   - Рабочая плоскость
         * Kz   - Коэффициент запаса
         *
         * | Сталые |
         * z    = 1;        (Коэффициент неравномерности освещения)
         * Ks   = 0,876635514;
         *
         * | Расчеты |
         * # Высота подвеса светильников над рабочей поверхностью
         * Hp = Np - hp
         *
         * # Площадь освещаемого помещения
         * S = A * B
         *
         * # Индекс помещения админ
         * i = (A * B) / (Hp * (A + B))
         *
         * # Коэффициент использования светового потока админ
         * KiA = TABLE
         *
         * # Коэффициент использования светового потока пром
         * KiP = TABLE
         *
         * # Световой поток админ на помещение
         * FlA = ceil((E * Kz * S * z / KiA) / 50) * 50
         *
         * # Световой поток пром на помещение
         * FlP = ceil((E * Kz * S * z / KiP) / 50) * 50
         *
         * # Количество светильников по стороне А
         * [A / B >= 3.3] Na = ceil(A / Hp, 1)
         * [A / B < 3.3] Na = ceil(Ks * A / Hp, 1)
         *
         * # Количество светильников по стороне B (пром)
         * [B / Hp <= 1.15] NbP = 1
         * [B / Hp > 1.15] NbP = Ks * A / Hp
         *
         * # Количество светильников по стороне B (админ)
         * [B / Hp <= 1.099] NbA = 1
         * [B / Hp > 1.099] NbA = Ks * A / Hp
         *
         * # Необходимое количество админ светильников
         * NA = Na * NbA
         *
         * # Необходимое количество пром светильников
         * NP = Na * NbP
         *
         * # Световой поток административного cветильника
         * [FlA / NA > 12000] Fa = E * Kz * S * z / ((Na + 1) * (NbA + 1) * KiA)
         * [FlA / NA < 12000] Fa = E * Kz * S * z / (NA * KiA)
         *
         * # Световой поток пром ветильника без отражателей
         * [FlP / NB > 12000] Fp = E * Kz * S * z / ((Na + 1) * (NbP +1) * KiP)
         * [FlP / NB < 12000] Fp = E * Kz * S * z / (NP * KiP)
         */

        $answer['data']['z'] = 1;
        $answer['data']['Ks'] = 0.876635514;

        if ($answer['data']['A'] < $answer['data']['B'])
        {
            $A = $answer['data']['A'];
            $answer['data']['A'] = $answer['data']['B'];
            $answer['data']['B'] = $A;
        }

        $answer['data']['Hp'] = $answer['data']['Np'] - $answer['data']['hp'];
        $answer['data']['S'] = $answer['data']['A'] * $answer['data']['B'];

        $KoeI = ($answer['data']['A'] * $answer['data']['B']) / ($answer['data']['Hp'] * ($answer['data']['A'] + $answer['data']['B']));
        $answer['data']['i'] = KoeI($KoeI);

        $answer['data']['KiA'] = KoefI($answer['data']['i'], "A");
        $answer['data']['KiP'] = KoefI($answer['data']['i'], "P");

        $answer['data']['FlA'] = ceil(($answer['data']['E'] * $answer['data']['Kz'] * $answer['data']['S'] * $answer['data']['z'] / $answer['data']['KiA']) / 50) * 50;
        $answer['data']['FlP'] = ceil(($answer['data']['E'] * $answer['data']['Kz'] * $answer['data']['S'] * $answer['data']['z'] / $answer['data']['KiP']) / 50) * 50;

        switch (true)
        {
            case ($answer['data']['A'] / $answer['data']['B'] >= 3.3): $answer['data']['Na'] = ceil($answer['data']['A'] / $answer['data']['Hp']);
                break;

            case ($answer['data']['A'] / $answer['data']['B'] < 3.3): $answer['data']['Na'] = ceil($answer['data']['Ks'] * $answer['data']['A'] / $answer['data']['Hp']);
                break;
        }

        switch (true)
        {
            case ($answer['data']['B'] / $answer['data']['Hp'] <= 1.099): $answer['data']['NbA'] = 1;
                break;

            case ($answer['data']['B'] / $answer['data']['Hp'] > 1.099): $answer['data']['NbA'] = ceil(($answer['data']['Ks'] * $answer['data']['B']) / $answer['data']['Hp']);
                break;
        }

        switch (true)
        {
            case ($answer['data']['B'] / $answer['data']['Hp'] <= 1.150): $answer['data']['NbP'] = 1;
                break;

            case ($answer['data']['B'] / $answer['data']['Hp'] > 1.150): $answer['data']['NbP'] = ceil(($answer['data']['Ks'] * $answer['data']['B']) / $answer['data']['Hp']);
                break;
        }

        $answer['data']['NA'] = $answer['data']['Na'] * $answer['data']['NbA'];
        $answer['data']['NP'] = $answer['data']['Na'] * $answer['data']['NbP'];

        switch (true)
        {
            case ($answer['data']['FlA'] / $answer['data']['NA'] > 12000):
                $answer['data']['Fa'] = $answer['data']['E'] * $answer['data']['Kz'] * $answer['data']['S'] * $answer['data']['z'] / (($answer['data']['Na'] + 1) * ($answer['data']['NbA'] + 1) * $answer['data']['KiA']);
                break;

            case ($answer['data']['FlA'] / $answer['data']['NA'] < 12000):
                $answer['data']['Fa'] = $answer['data']['E'] * $answer['data']['Kz'] * $answer['data']['S'] * $answer['data']['z'] / ($answer['data']['Na'] * $answer['data']['KiA']);
                break;

            default: $answer['data']['Fa'] = "N/A";
        }

        $answer['data']['Fa'] = ceil(($answer['data']['Fa'] / 2) / 50) * 50;

        switch (true)
        {
            case ($answer['data']['FlP'] / $answer['data']['NP'] > 12000):
                $answer['data']['Fp'] = $answer['data']['E'] * $answer['data']['Kz'] * $answer['data']['S'] * $answer['data']['z'] / (($answer['data']['Na'] + 1) * ($answer['data']['NbP'] + 1) * $answer['data']['KiP']);
                break;

            case ($answer['data']['FlP'] / $answer['data']['NP'] < 12000):
                $answer['data']['Fp'] = $answer['data']['E'] * $answer['data']['Kz'] * $answer['data']['S'] * $answer['data']['z'] / ($answer['data']['NP'] * $answer['data']['KiP']);
                break;

            default: $answer['data']['Fp'] = "N/A";
        }

        $answer['data']['Fp'] = ceil($answer['data']['Fp'] / 50) * 50;

        $answer['code'] = 200;
    }
    else die(json_encode($answer));

    if($answer['code'] == 200 || !$answer['error_msg']) unset($answer['error_msg']);
    echo json_encode($answer);
}

function KoeI ($i)
{
    switch ($i)
    {
        case ($i <= 0.5): $value = 0.5;
            break;
        case ($i <= 0.6): $value = 0.6;
            break;
        case ($i <= 0.7): $value = 0.7;
            break;
        case ($i <= 0.8): $value = 0.8;
            break;
        case ($i <= 0.9): $value = 0.9;
            break;
        case ($i <= 1): $value = 1;
            break;
        case ($i <= 1.1): $value = 1.1;
            break;
        case ($i <= 1.25): $value = 1.25;
            break;
        case ($i <= 1.5): $value = 1.5;
            break;
        case ($i <= 1.75): $value = 1.75;
            break;
        case ($i <= 2): $value = 2;
            break;
        case ($i <= 2.25): $value = 2.25;
            break;
        case ($i <= 2.5): $value = 2.5;
            break;
        case ($i <= 3): $value = 3;
            break;
        case ($i <= 3.5): $value = 3.5;
            break;
        case ($i <= 4): $value = 4;
            break;
        case ($i <= 5): $value = 5;
            break;

        default : $value = 0;
    }

    return $value;
}

function KoefI ($i, $type)
{
    if ($type == 'A') switch ($i)
    {
        case ($i == 0.5):   $value = 0.355;
            break;
        case ($i == 0.6):   $value = 0.425;
            break;
        case ($i == 0.7):   $value = 0.475;
            break;
        case ($i == 0.8):   $value = 0.525;
            break;
        case ($i == 0.9):   $value = 0.560;
            break;
        case ($i == 1):     $value = 0.585;
            break;
        case ($i == 1.1):   $value = 0.620;
            break;
        case ($i == 1.25):  $value = 0.660;
            break;
        case ($i == 1.5):   $value = 0.720;
            break;
        case ($i == 1.75):  $value = 0.755;
            break;
        case ($i == 2):     $value = 0.790;
            break;
        case ($i == 2.25):  $value = 0.815;
            break;
        case ($i == 2.5):   $value = 0.845;
            break;
        case ($i == 3):     $value = 0.880;
            break;
        case ($i == 3.5):   $value = 0.925;
            break;
        case ($i == 4):     $value = 0.935;
            break;
        case ($i == 5):     $value = 0.975;
            break;

        default :           $value = 0.000;
    }
    elseif ($type == 'D') switch ($i)
    {
        case ($i == 0.5):   $value = 0.065;
            break;
        case ($i == 0.6):   $value = 0.086;
            break;
        case ($i == 0.7):   $value = 0.114;
            break;
        case ($i == 0.8):   $value = 0.141;
            break;
        case ($i == 0.9):   $value = 0.165;
            break;
        case ($i == 1):     $value = 0.195;
            break;
        case ($i == 1.1):   $value = 0.219;
            break;
        case ($i == 1.25):  $value = 0.242;
            break;
        case ($i == 1.5):   $value = 0.285;
            break;
        case ($i == 1.75):  $value = 0.348;
            break;
        case ($i == 2):     $value = 0.398;
            break;
        case ($i == 2.25):  $value = 0.442;
            break;
        case ($i == 2.5):   $value = 0.489;
            break;
        case ($i == 3):     $value = 0.550;
            break;
        case ($i == 3.5):   $value = 0.585;
            break;
        case ($i == 4):     $value = 0.621;
            break;
        case ($i == 5):     $value = 0.700;
            break;

        default :           $value = 0.000;
    }
    elseif ($type == 'C') switch ($i)
    {
        case ($i == 0.5):   $value = 0.000;
            break;
        case ($i == 0.6):   $value = 0.000;
            break;
        case ($i == 0.7):   $value = 0.000;
            break;
        case ($i == 0.8):   $value = 0.000;
            break;
        case ($i == 0.9):   $value = 0.000;
            break;
        case ($i == 1):     $value = 0.000;
            break;
        case ($i == 1.1):   $value = 0.000;
            break;
        case ($i == 1.25):  $value = 0.000;
            break;
        case ($i == 1.5):   $value = 0.193;
            break;
        case ($i == 1.75):  $value = 0.229;
            break;
        case ($i == 2):     $value = 0.270;
            break;
        case ($i == 2.25):  $value = 0.322;
            break;
        case ($i == 2.5):   $value = 0.379;
            break;
        case ($i == 3):     $value = 0.433;
            break;
        case ($i == 3.5):   $value = 0.482;
            break;
        case ($i == 4):     $value = 0.538;
            break;
        case ($i == 5):     $value = 0.585;
            break;

        default :           $value = 0.000;
    }
    elseif ($type == 'P') switch ($i)
    {
        case ($i == 0.5):   $value = 0.319;
            break;
        case ($i == 0.6):   $value = 0.374;
            break;
        case ($i == 0.7):   $value = 0.423;
            break;
        case ($i == 0.8):   $value = 0.447;
            break;
        case ($i == 0.9):   $value = 0.506;
            break;
        case ($i == 1):     $value = 0.517;
            break;
        case ($i == 1.1):   $value = 0.543;
            break;
        case ($i == 1.25):  $value = 0.592;
            break;
        case ($i == 1.5):   $value = 0.631;
            break;
        case ($i == 1.75):  $value = 0.658;
            break;
        case ($i == 2):     $value = 0.699;
            break;
        case ($i == 2.25):  $value = 0.718;
            break;
        case ($i == 2.5):   $value = 0.737;
            break;
        case ($i == 3):     $value = 0.760;
            break;
        case ($i == 3.5):   $value = 0.779;
            break;
        case ($i == 4):     $value = 0.803;
            break;
        case ($i == 5):     $value = 0.825;
            break;

        default :           $value = 0.000;
    }
    else $value = 0;

    return $value;
}