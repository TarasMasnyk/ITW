    <section class="mainFooter">
        <section class="mainContainer">
            <div class="footer__block__logotype pull-left">
                <?php $options = get_option( 'wpuniq_theme_options' ); ?>
                <?php $l = getLangM(); ?>
                <a href="/">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/w.logo.png" />

                </a>
                <p><?php echo $l == 'ru' ? "$options[textfooterrus]" : "$options[textfootereng]"; ?></p>
            </div>
            

            <ul class="footer__list__company pull-left">
                
                <li class="title"><a href="/about/"><?php echo $l == 'ru' ? "О компании" : "About Us"; ?></a></li>
                <li><a href="/advantages/"><?php echo $l == 'ru' ? "Преимущества" : "Advantages"; ?></a></li>
                <li><a href="/clients/"><?php echo $l == 'ru' ? "Клиенты" : "Clients"; ?></a></li>
                <li><a href="/blog/"><?php echo $l == 'ru' ? "Блог" : "Blog"; ?></a></li>
                <li><a href="/news/"><?php echo $l == 'ru' ? "Новости" : "News"; ?></a></li>
                <li><a href="#"><?php echo $l == 'ru' ? "Сертификаты" : "Certifications"; ?></a></li>
                <li><a href="#"><?php echo $l == 'ru' ? "Карьера" : "Career"; ?></a></li>
                <li><a href="/contacts/"><?php echo $l == 'ru' ? "Контакты" : "Contacts"; ?></a></li>
                <li><a href="#"><?php echo $l == 'ru' ? "Команда" : "Team"; ?></a></li>
            </ul>
            <ul class="footer__list__service pull-left">
                <li class="title"><a href="/project/"><?php echo $l == 'ru' ? "Услуги" : "Applications"; ?></a></li>
                <li><a href="/project/#warehouses"><?php echo $l == 'ru' ? "Складское освещение" : "Warehouses"; ?></a></li>
                <li><a href="/project/#industrial"><?php echo $l == 'ru' ? "Промышленное освещение" : "Industrial lighting"; ?></a></li>
                <li><a href="/project/#trade"><?php echo $l == 'ru' ? "Торговое освещение" : "Retail lighting"; ?></a></li>
                <li><a href="/project/#office"><?php echo $l == 'ru' ? "Офисное освещение" : "Office lighting"; ?></a></li>
                <li><a href="/project/#outdoor"><?php echo $l == 'ru' ? "Уличное освещение" : "Outdoor lighting"; ?></a></li>
                <li><a href="/project/#public"><?php echo $l == 'ru' ? "ЖКХ" : "Residential Lighting"; ?></a></li>
                <li><a href="/project/"><?php echo $l == 'ru' ? "Проектные работы" : "Projects Implementation"; ?></a></li>
            </ul>
            <ul class="footer__list__partners pull-left">
                <li class="title"><a href="/partners/"><?php echo $l == 'ru' ? "Партнерам" : "For Partners"; ?></a>
                    <ul class="partners_style">
                        <li style="margin:0;" class="title"><a href="#"><?php echo $l == 'ru' ? "Для партнеров" : "For Partners"; ?></a></li>
                        <li class="title"><a href="#"><?php echo $l == 'ru' ? "Информация для партнеров" : "Information for partners"; ?></a></li>
                        <li class="title"><a href="#"><?php echo $l == 'ru' ? "Стать партнером" : "Become a partner"; ?></a></li>
                    </ul>
                    </li>
                <li class="title"><a href="/project/"><?php echo $l == 'ru' ? "Наши проекты" : "Our Projects"; ?></a></li>
                <li class="title"><a href="/item/"><?php echo $l == 'ru' ? "Каталог товаров" : "Catalog"; ?></a></li>
            </ul>
            <div class="footer__block__contacts pull-right">
                <span class="phone"><i class="fa fa-phone"></i><?php echo $options[telefone]; ?></span>
                <span class="email"><i class="fa fa-envelope"></i><?php echo $options[email]; ?></span>
                <!--<span ><i class="fa fa-paper-plane-o"></i><a  href="#mailmunch-pop-202664">Subscribe to newsletter</a></span>-->
                <ul class="footer__socials list-unstyled">
                    <li class="title"><?php echo $l == 'ru' ? "Соцсети:" : "Follow us:"; ?></li>
                    <li class="icon"><a href="https://<?php echo $options[vkontacte]; ?>" target="_blank"><p class="vkontacte">B</p></a></li>
                    <li class="icon"><a href="https://<?php echo $options[odnoklasniki]; ?>" target="_blank"><i class="fa fa-odnoklassniki"></i></a></li>
                    <li class="icon"><a href="https://<?php echo $options[facebook]; ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    <li class="icon"><a href="https://<?php echo $options[twitter]; ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
                    <li class="clearfix"></li>
                </ul>
                <div class="clearfix"></div>
                <p>©<?php echo date("Y");?><?php echo $l == 'ru' ? " - ООО «Интелтек Украина». Все права защищены." : " -  IntelTech Ukraine. All rights reserved."; ?></p>
            </div>
            <div class="clearfix"></div>
        </section>
    </section>
    <!-- JS Functions -->
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/libs/js/main.js"></script>

<?php wp_footer(); ?>
</body>
</html>