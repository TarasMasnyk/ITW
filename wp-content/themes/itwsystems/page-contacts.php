<?php /* Template Name: Contacts [Template] */ ?>
<?php get_header(); ?>
    <section class="mainContent__block block__content">
        <?php $l = $_COOKIE['qtrans_front_language']; ?>
        <h2 class="block__title"><?php the_title(); ?></h2>
        <h3 class="text-center"><?php echo $l == 'ru' ? "Наши отделения:" : "Our branches:"; ?></h3>
        <section class="mainContainer">
            <div class="flux_box" style="float: none; margin: 15px auto;">
                <p><?php echo $l == 'ru' ? "г. Киев, ул. Шолуденко 27/6, офис 139" : "Kiev, Sholudenko street 27/6, 139 office"; ?></p>
                <span><i class="fa fa-phone"></i> (044) 222 - 83 - 23</span>
                <span><i class="fa fa-envelope"></i> info@itw-systems.com</span>
            </div>
            <div class="clearfix"></div>
        </section>
    </section>

    <section class="mainContent__block block__location">
        <?php include ("includes/location.map.php"); ?>
    </section>
<?php get_footer(); ?>