
<?php get_header(); ?>
 <!-- <div class="mainContainer breadcrumbs_block">
        <?php custom_breadcrumbs(); ?>
     <div class="clearfix"></div>
    </div> --> 
<section class="mainContent_Slider custom_style single_project" id="top_slider">
        <ul class="mainContent_Slider_list slideme list-unstyled">
            <?php $l = getLangM(); ?>
            
            <?php
            $fa = get_post_custom(get_the_ID());

            $_mData = array();
            foreach ($fa as $_k => $_v)
            {
                if (substr($_k, 0, 6) == "slider")
                {
                    $_mData[substr($_k, 0, 8)][substr($_k, 9)] = str_replace(array("(Русский) ", "(Русский)", "(English) ", "(English)"), "", $_v[0]);
                }
            }

            foreach ($_mData as $slider => $slider_item)
            {
                ?>
                <li data-sliderID="<?php echo $slider; ?>" class="list_style">
                    <div class="mainContainer top_slider_style">
                            <h2 class="abstract-title" style="font-size: <?php echo $slider_item['title_fs']; ?>;"><?php echo $slider_item['title']; ?></h2>
                        </div>
                          <img src="<?php echo $slider_item['image']; ?>" />   
                </li>
                <?php
            }
            ?>
        </ul>
    </section>
    <?php

                  if (have_posts())
                    {
                        while (have_posts())
                        {
                            the_post();
                            $logoUrl = get_post_custom_values('logo', $post->ID); 
                            /*Slider Image */
                            $ft_befor= get_post_custom_values('ft_befor',$post->ID);
                            $threeD_befor= get_post_custom_values('threeD_befor',$post->ID);
                            $ft_after= get_post_custom_values('ft_after',$post->ID);
                            $threeD_after= get_post_custom_values('threeD_after',$post->ID);
                            /* Project Inform*/
                            $number_of_lamps= get_post_custom_values('number_of_lamps',$post->ID);
                            $square=get_post_custom_values('square',$post->ID);
                            $power_lx_after=get_post_custom_values('power_lx_after', $post->ID);
                            $power_lx_befor=get_post_custom_values('power_lx_befor', $post->ID);
                            $power_wt_after=get_post_custom_values('power_wt_after', $post->ID);
                            $power_wt_befor=get_post_custom_values('power_wt_befor', $post->ID);
                            $free_power=get_post_custom_values('free_power', $post->ID);
                            $payback=get_post_custom_values('payback', $post->ID);
                           
                           /*Taxonomy*/
                            $the_cats= get_the_terms( $post->ID, 'object' );          
                            foreach($the_cats as $the_cat){
                             $the_single_cat=$the_cat->name;}
                    ?>
   <section class="block_atb-project cf">
        <div class="container_block-atb">
            <div class="block_text-atb cf">
                <h2 class="block_text-title"><?php echo $l == 'ru' ? "о проекте" : "about the project"; ?></h2>
                <div class="block-atb-text"><?php the_content(); ?></div>
                </div>
            <div class="block_logo-atb cf">
            <figure class="figure-block">
                <img src="<?php echo $logoUrl[0];?>" alt="#">
                <figcaption class="figcaption"><?php echo $the_single_cat;?></figcaption>
            </figure>
                
            </div>
        </div>
    </section>
    <section class="mainContent__block block__works single_project">
        <div class="mainContent_Slider slider__pf__wrapper">
            <ul class="mainContent_Slider_list slideme list-unstyled">
                <li>
                    <div class="background_slider">
                        <div class="mainContainer">
                            <div class="slider__pf " id="twentytwenty-id-<?php the_ID(); ?>">
                                <img data-href="<?= $ft_befor[0]; ?>" data-src="<?= $threeD_befor[0]; ?>" src="<?= $threeD_befor[0]; ?>">
                                <img data-href="<?= $ft_after[0]; ?>" data-src="<?= $threeD_after[0]; ?>" src="<?= $threeD_after[0]; ?>">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="slider_infor">
                            <div class="block-text cf">
                                <h4 class="block-shine-information-title block-item-title"><?php echo $l == 'ru' ? "данные" : "information"; ?></h4>
                                <div class="block-item">
                                    <ul>
                                        <l1 class="block-list-information cf">
                                            <span class="list-span-left"><?php echo $l == 'ru' ? "Площадь" : "Area"; ?>: </span>
                                            <span class="span-list-span-right"><?php echo $square[0]; ?></span>
                                        </l1>
                                        <l1 class="block-list-information cf">
                                            <span class="list-span-left"><?php echo $l == 'ru' ? "Количество светильников" : "Number of lights"; ?>:</span>
                                            <span class="span-list-span-right"><?php echo $number_of_lamps[0]; ?></span>
                                        </l1>
                                        <l1 class="block-list-information cf">
                                            <span class="list-span-left"><?php echo $l == 'ru' ? "Освещенность" : "Luminosity"; ?>:</span>
                                            <span class="span-list-span-right">
                                                <span class="small-text-span"><?php echo $l == 'ru' ? "Было" : "Befor"; ?> </span>
                                                <span class="text-information-number"><?php echo $power_lx_befor[0]; ?> lx</span>
                                                <span class="small-text-span small-text-span-have "><?php echo $l == 'ru' ? "Стало" : "After"; ?></span><?php echo $power_lx_after[0]; ?> lx</span>
                                        </l1>
                                        <l1 class="block-list-information cf">
                                            <span class="list-span-left"><?php echo $l == 'ru' ? "Мощность системы" : "Systems power "; ?>:</span>
                                            <span class="span-list-span-right">
                                                <span class="small-text-span"><?php echo $l == 'ru' ? "Было" : "Befor"; ?> </span>
                                                <span class="text-information-number "><?php echo $power_wt_befor[0]; ?> кВт</span>
                                                <span class="small-text-span small-text-span-have"><?php echo $l == 'ru' ? "Стало" : "After"; ?></span><?php echo $power_wt_after[0]; ?> кВт</span>
                                        </l1>
                                        <l1 class="block-list-information cf">
                                            <span class="list-span-left"><?php echo $l == 'ru' ? "Высвобождаемая мощность" : "Releasable power "; ?>:</span>
                                            <span class="span-list-span-right"><?php echo $free_power[0]; ?>кВт</span>
                                        </l1>
                                        <l1 class="block-list-information cf">
                                            <span class="list-span-left"><?php echo $l == 'ru' ? "Срок oкупаемости" : "Payback period"; ?>:</span>
                                            <span class="span-list-span-right"><?php echo $payback[0]; ?> <?php echo $l == 'ru' ? "месяцев" : "months"; ?></span>
                                        </l1>
                                    </ul>
                                </div>
                            </div>
                            <div class="block-shine-information">
                                <h4 class="block-shine-information-title block-item-title"><?php echo $l == 'ru' ? "тип используемых светильников" : "type of lamps used"; ?></h4>
                                <div class="block-industry-led cf">
                                    <?php 

                                    $posts = get_field('relationship_field_name');
                                    
                                    if( $posts ): 
                                        
                                        ?>
                                        <ul>
                                        <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
                                            <?php setup_postdata($post); 
                                            $attachmentId = get_post_thumbnail_id($post->ID);
                                            $thymbUrl = wp_get_attachment_url($attachmentId, 'full', true);

                                            ?>
                                            <div class="block-img-industry cf">

                                                <img src="<?php echo $thymbUrl; ?>" alt="#">
                                                <a href="<?php the_permalink(); ?>"> <p class="text-led"><?php the_title(); ?></p></a>
                                            </div>
                                        <?php endforeach; ?>
                                        </ul>
                                        <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
        </div>

    </section>

        <?php }
    } else { ?><p>No projects</p> <?php } ?>
<?php get_footer(); ?>