<?php /* Template Name: Advantages [Template] */ ?>
<?php get_header(); ?>
<section class="mainContent__block block__content">
    <h2 class="block__title"><?php the_title(); ?></h2>
    <section class="mainContainer"><?php echo nl2br($post->post_content); ?></section>
</section>
<?php get_footer(); ?>
