<?php /* Template Name: Services [Template] */ ?>
<?php get_header(); ?>
    <section class="mainContent__block block__content">
        <h2 class="block__title"><?php the_title(); ?></h2>
        <section class="mainContainer">
            <div class="advantages__block__rounded">
                <div class="advantages__block_centred">
                    <span>
                        ITW SYSTEMS
                        <span>We Bring intelligence to Light</span>
                    </span>
                </div>
                <div class="advantages__block__items">
                    <div class="advantages__block__item scheme line_1 item_1">
                        <a href="/services/warehouse/"></a>
                        <span class="item__icon icon_warehouse"></span>
                        <p><?php echo get_post_meta(get_the_ID(), "service_warehouses")[0]; ?></p>
                    </div>
                    <div class="advantages__block__item scheme line_1 item_2">
                        <a href="/services/production/"></a>
                        <span class="item__icon icon_production"></span>
                        <p><?php echo get_post_meta(get_the_ID(), "service_manufacturing")[0]; ?></p>
                    </div>
                    <div class="advantages__block__item scheme line_2 item_3">
                        <a href="/services/trade-house/"></a>
                        <span class="item__icon icon_trade"></span>
                        <p><?php echo get_post_meta(get_the_ID(), "service_retail_spaces")[0]; ?></p>
                    </div>
                    <div class="advantages__block__item scheme line_2 item_4">
                        <a href="/services/office/"></a>
                        <span class="item__icon icon_office"></span>
                        <p><?php echo get_post_meta(get_the_ID(), "service_office_spaces")[0]; ?></p>
                    </div>
                    <div class="advantages__block__item scheme line_3 item_5">
                        <a href="/services/parking/"></a>
                        <span class="item__icon icon_park"></span>
                        <p><?php echo get_post_meta(get_the_ID(), "service_park_lighting")[0]; ?></p>
                    </div>
                    <div class="advantages__block__item scheme line_3 item_6">
                        <a href="/services/utilities/"></a>
                        <span class="item__icon icon_utilities"></span>
                        <p><?php echo get_post_meta(get_the_ID(), "service_utilities")[0]; ?></p>
                    </div>
                    <div class="advantages__block__item scheme line_4 item_7">
                        <a href="/services/projects/"></a>
                        <span class="item__icon icon_projects"></span>
                        <p><?php echo get_post_meta(get_the_ID(), "service_projects")[0]; ?></p>
                    </div>
                </div>
            </div>
        </section>
    </section>
<?php get_footer(); ?>