<article>
    <header style="width: 100%; height: 45px; margin: 0 0 30px 0; line-height: 38px; border-bottom: 5px solid #006148; background: rgba(0,0,0, .015); color: #202020;">
        <h4 style="height: 40px; line-height: 40px; margin: 0; padding: 0 0 0 15px;"><?php the_title(); ?></h4>
    </header>
    <p class="text"><?php the_content(); ?></p>
    <div class="text-right"><a href="<?php the_permalink() ?>" class="button btn-yellow pull-right;">Подробнее</a></div>
</article>