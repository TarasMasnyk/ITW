<?php /* Template Name: Clients [Template] */ ?>
<?php get_header(); ?>
    <section class="mainContent__block block__content">
        <h2 class="block__title"><?php the_title(); ?></h2>
        <section class="mainContainer">
            <ul class="clients__list list-unstyled">
                <li class="clients__list__item">
                    <a href="/projects/">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/clients/items/logo_1.png" />
                    </a>
                </li>
                <li class="clients__list__item">
                    <a href="/projects/">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/clients/items/logo_2.png" />
                    </a>
                </li>
                <li class="clients__list__item">
                    <a href="/projects/">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/clients/items/logo_3.png" />
                    </a>
                </li>
                <li class="clients__list__item">
                    <a href="/projects/">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/clients/items/logo_4.png" />
                    </a>
                </li>
                <li class="clients__list__item">
                    <a href="/projects/">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/clients/items/logo_5.png" />
                    </a>
                </li>
                <li class="clients__list__item">
                    <a href="/projects/">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/clients/items/logo_6.png" />
                    </a>
                </li>
                <li class="clients__list__item">
                    <a href="/projects/">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/clients/items/logo_7.png" />
                    </a>
                </li>
                <li class="clients__list__item">
                    <a href="/projects/">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/clients/items/logo_8.png" />
                    </a>
                </li>
                <li class="clients__list__item">
                    <a href="/projects/">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/clients/items/logo_9.png" />
                    </a>
                </li>
                <li class="clients__list__item">
                    <a href="/projects/">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/clients/items/logo_10.png" />
                    </a>
                </li>
                <li class="clients__list__item">
                    <a href="/projects/">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/clients/items/logo_11.png" />
                    </a>
                </li>
                <li class="clients__list__item">
                    <a href="/projects/">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/clients/items/logo_12.png" />
                    </a>
                </li>
                <li class="clients__list__item">
                    <a href="/projects/">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/clients/items/logo_13.png" />
                    </a>
                </li>
                <li class="clients__list__item">
                    <a href="/projects/">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/clients/items/logo_14.png" />
                    </a>
                </li>
                <li class="clients__list__item">
                    <a href="/projects/">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/clients/items/logo_15.png" />
                    </a>
                </li>
                <li class="clients__list__item">
                    <a href="/projects/">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/clients/items/logo_16.png" />
                    </a>
                </li>
                <li class="clients__list__item">
                    <a href="/projects/">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/clients/items/logo_17.png" />
                    </a>
                </li>
                <li class="clients__list__item">
                    <a href="/projects/">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/clients/items/logo_18.png" />
                    </a>
                </li>
                <li class="clients__list__item">
                    <a href="/projects/">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/clients/items/logo_19.png" />
                    </a>
                </li>
                <li class="clearfix"></li>
            </ul>
        </section>
    </section>
<?php get_footer(); ?>