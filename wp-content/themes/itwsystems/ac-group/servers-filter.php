<?php ///* Template Name: Servers [Template] */ ?>
<?php
get_header();
get_sidebar();
global $post;

$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$q_args = array (
    'post_type'         => "server",
    'post_per_page'     => 4,
    'caller_get_posts'  => 1,
    'paged'             => $paged,
    'meta_query'        => array('relation' => 'OR')
);

if (isset ($_GET['manufacturer']))
{
    foreach ($_GET['manufacturer'] as $manufacturer_id)
    {
        $q_args['meta_query'][] = array(
            'relation'  => "OR",
            array (
                'key'       => 'manufacturer_id',
                'value'     => "a:1:{i:0;s:3:\"" . $manufacturer_id . "\";}",
                'compare'   => "="
            )
        );
    }
}

if (isset ($_GET['class']))
{
    foreach ($_GET['class'] as $class)
    {
        $q_args['meta_query'][] = array(
            'relation'  => "AND",
            array (
                'key'       => 'class',
                'value'     => $class,
                'compare'   => "="
            )
        );
    }
}

if (isset ($_GET['price']))
{
    $_price = array();
    foreach ($_GET['price'] as $price)
    {
        $_price[] = $price;
    }

    $q_args['meta_query'][] = array(
        'relation'  => "AND",
        array (
            'key'       => 'price',
            'value'     => ceil($_price[0] / 75),
            'compare'   => ">="
        ),
        array (
            'key'       => 'price',
            'value'     => ceil($_price[1] / 75),
            'compare'   => "<="
        )
    );
}

$servers = new WP_Query($q_args);

$filters = array (
    'manufacturer',
    'price',
    'class',
    'processor',
    'pnum',
    'ram',
    'hdd'
);

wp_reset_query();
?>
    <div id="category-wrap">
        <div class="container">
            <div class="row">
                <?php include ("includes/filter-block.php"); ?>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                    <div id="offers">

                        <div class="offers-title">Серверы</div>

                        <?php
                        $pID = 0;

                        foreach ($servers->posts as $_id => $post_data)
                        {
                            $pID = showOffers($post_data, $pID);
                        }
                        ?>

                        <?php  pagination($servers->max_num_pages); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
wp_reset_query();
get_template_part('help', 'block');
get_footer();
?>