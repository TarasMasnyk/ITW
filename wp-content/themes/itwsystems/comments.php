<h2 class="comments__num__title">Комментарии (<?php echo get_comments_number(get_the_ID()); ?>)</h2>
<?php if (post_password_required()) return; ?>
<div class="comments__inner__form">
    <?php

    $args = array(
        'logged_in_as' => '<img src="' . get_template_directory_uri() . '/images/noavatar.png" class="avatar" />',
        'comment_field' => '<textarea id="comment" name="comment" cols="45" rows="8" aria-required="true" placeholder="Написать комментарий"></textarea>',
        'comment_notes_before' => '',
        'class_submit' => "button btn-yellow btn-big",
        'label_submit' => "Комментировать",

        'fields' => array(
            'author' => '<input id="author" name="author" type="text" value="' . esc_attr($commenter['comment_author']) . '" size="30"' . $aria_req . ' placeholder="Ваше имя" />',
            'email' => '<input id="email" name="email" type="text" value="' . esc_attr($commenter['comment_author_email']) . '" size="30"' . $aria_req . ' placeholder="E-mail" />',
            'url' => false
        )
    );

    comment_form($args);
    ?>
</div>
<?php
if (have_comments())
{
    ?>
    <div class="comments__item">
        <?php
        wp_list_comments(array(
            'style'       => 'div',
            'short_ping'  => true,
            'avatar_size' => 95,
            'callback'    => 'itw_comment'
        ));
        ?>
        <div class="clearfix"></div>
    </div>
<?php
}