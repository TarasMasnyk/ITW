<?php /* Template Name: Partners Become [Template] */ ?>
<?php get_header(); ?>
    <section class="mainContent__block block__content">
        <h2 class="block__title"><?php the_title(); ?></h2>
        <section class="mainContainer"></section>
    </section>
<?php get_footer(); ?>