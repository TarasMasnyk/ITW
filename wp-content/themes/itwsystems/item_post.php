<?php
/**
 * Register `item` post type
 */
function item_post_type() {
   
   // Labels
	$labels = array(
		'name' => _x("Item", "post type general name"),
		'singular_name' => _x("Item", "post type singular name"),
		'menu_name' => 'Item Catalog',
		'add_new' => _x("Add New Item", "item item"),
		'add_new_item' => __("Add New Item"),
		'edit_item' => __("Edit Item"),
		'new_item' => __("New Item"),
		'view_item' => __("View Item"),
		'search_items' => __("Search Item"),
		'not_found' =>  __("No Item Found"),
		'not_found_in_trash' => __("No Item Found in Trash"),
		'parent_item_colon' => ''
	);
	$rewrite = array(
    'slug'                => 'item',
    'with_front'          => true,
    'pages'               => true,
    'feeds'               => true,
    );
	// Register post type
	register_post_type('item' , array(
		'labels' => $labels,
		'public' => true,
		'has_archive' => true,
		'menu_icon' => 'dashicons-format-gallery',
		'rewrite' => $rewrite,
		'supports' => array('title', 'editor', 'thumbnail')
	) );
}
add_action( 'init', 'item_post_type', 0 );
/**
 * Register `type` taxonomy
 */
function item_taxonomy() {
	
	// Labels
	$singular = 'Type';
	$plural = 'types';
	$labels = array(
		'name' => _x( $plural, "taxonomy general name"),
		'singular_name' => _x( $singular, "taxonomy singular name"),
		'search_items' =>  __("Search $singular"),
		'all_items' => __("All $singular"),
		'parent_item' => __("Parent $singular"),
		'parent_item_colon' => __("Parent $singular:"),
		'edit_item' => __("Edit $singular"),
		'update_item' => __("Update $singular"),
		'add_new_item' => __("Add New $singular"),
		'new_item_name' => __("New $singular Name"),
	);

	// Register and attach to 'item' post type
	register_taxonomy( strtolower($singular), 'item', array(
		'public' => true,
		'show_ui' => true,
        'show_admin_column' => true,
		'show_in_nav_menus' => true,
		'hierarchical' => true,
		'query_var' => true,
		'rewrite' => false,
		'labels' => $labels
	) );
}
add_action( 'init', 'item_taxonomy', 0 );


//Add Custome Field
add_action( 'admin_init', 'my_admin' );
// add_action('save_post', 'save_item_img');
add_action('save_post', 'save_item_catalog_data');
add_action('save_post', 'save_item_data');

function my_admin() {
    // add_meta_box( 'item_image_box', 'Item Image', 'display_item_meta_box', 'item', 'normal', 'high');
    add_meta_box( 'item_catalog', 'Catalog PDF', 'display_item_catalog_box', 'item', 'normal', 'high');
    add_meta_box( 'item_detailse_box', 'Item Details', 'display_item_detailse_box', 'item', 'normal', 'high');
   
}
    function display_item_catalog_box(){
        global $post;
        $custom =get_post_custom($post->ID);
        $item_catalog = $custom["item_catalog"][0];
?>
    <p>Загрузите ваш файл в папку "../yourwebsite/wp-comtent/themes/itwsystems/upload/" и впишите сюда его имя "example.pdf"</p>
    <label style="width:70px; display:inline-block;" class="custome_fild">File Name:</label><input  style="width:300px;" name="item_catalog" value="<?php echo $item_catalog; ?>" /><br/>
<?php }

      function display_item_meta_box(){
         global $post;
         $custom = get_post_custom($post->ID);
         $item_off = $custom["item_off"][0];
         $item_on = $custom["item_on"][0];
        
?>
     <p>Вставьте URL вашей image<p>
   <label style="width:70px; display:inline-block;" class="custome_fild">Item Off:</label><input  style="width:300px;" name="item_off" value="<?php echo $item_off; ?>" /><br/>
     <label style="width:70px; display:inline-block;" class="custome_fild">Item On:</label><input  style="width:300px;" name="item_on" value="<?php echo $item_on; ?>" /><br/>
 <?php
     }

    function display_item_detailse_box(){
    global $post;
        $custom = get_post_custom($post->ID);
        $item_detaile_1 = $custom["item_detaile_1"][0];
        $item_detaile_2 = $custom["item_detaile_2"][0];
        $item_detaile_3 = $custom["item_detaile_3"][0];
        $item_detaile_4 = $custom["item_detaile_4"][0];
        $item_detaile_5 = $custom["item_detaile_5"][0];
        $item_detaile_6 = $custom["item_detaile_6"][0];
        $item_detaile_7 = $custom["item_detaile_7"][0];
        $item_detaile_8 = $custom["item_detaile_8"][0];
        $item_detaile_9 = $custom["item_detaile_9"][0];
        $item_detaile_10 = $custom["item_detaile_10"][0];
        $item_detaile_11 = $custom["item_detaile_11"][0];
        $item_detaile_12 = $custom["item_detaile_12"][0];          
?>
    <label style="width:140px; display:inline-block;" class="custome_fild">Производитель светодиодов: </label><input  style="width:300px;" name="item_detaile_1" value="<?php echo $item_detaile_1; ?>" /><br/>
   	<label style="width:140px; display:inline-block;" class="custome_fild">Плафон:</label><input  style="width:300px;" name="item_detaile_2" value="<?php echo $item_detaile_2; ?>" /><br/>
    <label style="width:140px; display:inline-block;" class="custome_fild">Степень зациты:</label><input  style="width:300px;" name="item_detaile_3" value="<?php echo $item_detaile_3; ?>" /><br/>
	<label style="width:140px; display:inline-block;" class="custome_fild">Источник питания:</label><input  style="width:300px;" name="item_detaile_4" value="<?php echo $item_detaile_4; ?>" /><br/>
    <label style="width:140px; display:inline-block;" class="custome_fild">Коэффициент мощности:</label><input  style="width:300px;" name="item_detaile_5" value="<?php echo $item_detaile_5; ?>" /><br/>
    <label style="width:140px; display:inline-block;" class="custome_fild">Оптическая система:</label><input  style="width:300px;" name="item_detaile_6" value="<?php echo $item_detaile_6; ?>" /><br/>
    <label style="width:140px; display:inline-block;" class="custome_fild">Температура эксплуатации:</label><input  style="width:300px;" name="item_detaile_7" value="<?php echo $item_detaile_7; ?>" /><br/>
    <label style="width:140px; display:inline-block;" class="custome_fild">(тяжелых условий эксплуатации:</label><input  style="width:300px;" name="item_detaile_8" value="<?php echo $item_detaile_8; ?>" /><br/>
    <label style="width:140px; display:inline-block;" class="custome_fild">Возможный выбор длины:</label><input  style="width:300px;" name="item_detaile_9" value="<?php echo $item_detaile_9; ?>" /><br/>
    <label style="width:140px; display:inline-block;" class="custome_fild">Световой поток:</label><input  style="width:300px;" name="item_detaile_10" value="<?php echo $item_detaile_10; ?>" /><br/>
    <label style="width:140px; display:inline-block;" class="custome_fild">Мощность:</label><input  style="width:300px;" name="item_detaile_11" value="<?php echo $item_detaile_11; ?>" /><br/>
    <label style="width:140px; display:inline-block;" class="custome_fild">Расположение источника питания:</label><input  style="width:300px;" name="item_detaile_12" value="<?php echo $item_detaile_12; ?>" /><br/>
<?php	
    }
    function save_item_catalog_data(){
        global $post;
        update_post_meta($post->ID, "item_catalog", $_POST["item_catalog"]);
    }
    function save_item_img(){
    global $post;
    update_post_meta($post->ID, "item_off", $_POST["item_off"]);
    update_post_meta($post->ID, "item_on", $_POST["item_on"]);
	}
	function save_item_data(){
	global $post;
	update_post_meta($post->ID, "item_detaile_1", $_POST["item_detaile_1"]);
    update_post_meta($post->ID, "item_detaile_2", $_POST["item_detaile_2"]);
    update_post_meta($post->ID, "item_detaile_3", $_POST["item_detaile_3"]);
    update_post_meta($post->ID, "item_detaile_4", $_POST["item_detaile_4"]);
    update_post_meta($post->ID, "item_detaile_5", $_POST["item_detaile_5"]);
    update_post_meta($post->ID, "item_detaile_6", $_POST["item_detaile_6"]);
    update_post_meta($post->ID, "item_detaile_7", $_POST["item_detaile_7"]);
    update_post_meta($post->ID, "item_detaile_8", $_POST["item_detaile_8"]);
    update_post_meta($post->ID, "item_detaile_9", $_POST["item_detaile_9"]);
    update_post_meta($post->ID, "item_detaile_10", $_POST["item_detaile_10"]);
    update_post_meta($post->ID, "item_detaile_11", $_POST["item_detaile_11"]);
    update_post_meta($post->ID, "item_detaile_12", $_POST["item_detaile_12"]);	
	}