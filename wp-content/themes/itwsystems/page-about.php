<?php /* Template Name: About [Template] */ ?>
<?php get_header(); ?>
    <section class="mainContent__block block__content">
        <h2 class="block__title"><?php the_title(); ?></h2>
        <section class="mainContainer text">
            <?php
            $l = getLangM();
            ?>
            <?php
            if (have_posts()) while (have_posts())
            {
                the_post();
                the_content();
            }
            ?>
        </section>
    </section>

    <section class="mainContent__block block__solutions">
        <h3 class="block__title"><?php echo $l == 'ru' ? "Индивидуальные проектные решения" : "Individual Project Solutions"; ?></h3>
        <section class="mainContainer">
            <div class="flux_box">
                <img src="<?php echo get_template_directory_uri(); ?>/images/indent/cola.png" />
                <h4><?php echo $l == 'ru' ? "Coca Cola" : "Coca Cola"; ?></h4>
            </div>
            <div class="flux_box">
                <img src="<?php echo get_template_directory_uri(); ?>/images/indent/chemical.png" />
                <h4><?php echo $l == 'ru' ? "Борщаговский Хим Фарм завод" : "Borshchahivskiy CPP"; ?></h4>
            </div>
            <div class="flux_box">
                <img src="<?php echo get_template_directory_uri(); ?>/images/indent/fozzy.png" />
                <h4><?php echo $l == 'ru' ? "Fozzy" : "Fozzy"; ?></h4>
            </div>
            <div class="flux_box">
                <img src="<?php echo get_template_directory_uri(); ?>/images/indent/danon.png" />
                <h4><?php echo $l == 'ru' ? "Данон" : "Danone"; ?></h4>
            </div>
            <div class="flux_box">
                <img src="<?php echo get_template_directory_uri(); ?>/images/indent/logistic.png" />
                <h4><?php echo $l == 'ru' ? "Логистик+" : "Logistic+"; ?></h4>
            </div>
            <div class="clearfix"></div>
        </section>
    </section>

    <section class="mainContent__block block__production">
        <h3 class="block__title"><?php echo $l == 'ru' ? "Собственное производство" : "Own manufacture"; ?></h3>
        <section class="mainContainer">
            <div class="flux_box">
                <a href="<?php echo get_template_directory_uri(); ?>/images/indent/1.jpg" class="colorbox"><img src="<?php echo get_template_directory_uri(); ?>/images/indent/1.png" /></a>
            </div>
            <div class="flux_box">
                <a href="<?php echo get_template_directory_uri(); ?>/images/indent/2.jpg" class="colorbox"><img src="<?php echo get_template_directory_uri(); ?>/images/indent/2.png" /></a>
            </div>
            <div class="flux_box">
                <a href="<?php echo get_template_directory_uri(); ?>/images/indent/3.jpg" class="colorbox"><img src="<?php echo get_template_directory_uri(); ?>/images/indent/3.png" /></a>
            </div>
            <div class="flux_box">
                <a href="<?php echo get_template_directory_uri(); ?>/images/indent/4.jpg" class="colorbox"><img src="<?php echo get_template_directory_uri(); ?>/images/indent/4.png" /></a>
            </div>
            <div class="flux_box">
                <a href="<?php echo get_template_directory_uri(); ?>/images/indent/5.jpg" class="colorbox"><img src="<?php echo get_template_directory_uri(); ?>/images/indent/5.png" /></a>
            </div>
            <div class="flux_box">
                <a href="<?php echo get_template_directory_uri(); ?>/images/indent/6.jpg" class="colorbox"><img src="<?php echo get_template_directory_uri(); ?>/images/indent/6.png" /></a>
            </div>
            <div class="clearfix"></div>
        </section>
    </section>

    <section class="mainContent__block block__contacts">
        <h3 class="block__title"><?php echo $l == 'ru' ? "Обратная связь" : "Feedback"; ?></h3>
        <section class="mainContainer">
            <div class="block__contacts__aside__l">
                <p class="tick-list text"><span>*</span>
                    <?php echo $l == 'ru' ? "Напишите нам и мы обязательно свяжемся с Вами в ближайшее время" : "Write to us and we will contact you in the shortest possible time"; ?></p>
            </div>
            <div class="block__contacts__aside__r">
                <?php include ("includes/contacts.form.php"); ?>
            </div>
            <div class="clearfix"></div>
        </section>
    </section>

    <section class="mainContent__block block__location">
        <h3 class="block__title"><?php echo $l == 'ru' ? "Наши координаты" : "Our address"; ?></h3>
        <?php include ("includes/location.map.php"); ?>
    </section>
<?php get_footer(); ?>