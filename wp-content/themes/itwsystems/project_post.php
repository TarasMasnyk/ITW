<?php
/**
 * Register `project` post type
 */
function project_post_type() {
   
   // Labels
	$labels = array(
		'name' => _x("Project", "post type general name"),
		'singular_name' => _x("Project", "post type singular name"),
		'menu_name' => 'Project Profiles',
		'add_new' => _x("Add New Project", "project item"),
		'add_new_item' => __("Add New Profile"),
		'edit_item' => __("Edit Profile"),
		'new_item' => __("New Profile"),
		'view_item' => __("View Profile"),
		'search_items' => __("Search Profiles"),
		'not_found' =>  __("No Profiles Found"),
		'not_found_in_trash' => __("No Profiles Found in Trash"),
		'parent_item_colon' => ''
	);
	$rewrite = array(
    'slug'                => 'project',
    'with_front'          => true,
    'pages'               => true,
    'feeds'               => true,
    );
	// Register post type
	register_post_type('project' , array(
		'labels' => $labels,
		'public' => true,
		'has_archive' => true,
		'menu_icon' => 'dashicons-format-gallery',
		'rewrite' => $rewrite,
		'supports' => array('title', 'editor', 'thumbnail')
	) );
}
add_action( 'init', 'project_post_type', 0 );
/**
 * Register `object` taxonomy
 */
function project_taxonomy() {
	
	// Labels
	$singular = 'Object';
	$plural = 'Objects';
	$labels = array(
		'name' => _x( $plural, "taxonomy general name"),
		'singular_name' => _x( $singular, "taxonomy singular name"),
		'search_items' =>  __("Search $singular"),
		'all_items' => __("All $singular"),
		'parent_item' => __("Parent $singular"),
		'parent_item_colon' => __("Parent $singular:"),
		'edit_item' => __("Edit $singular"),
		'update_item' => __("Update $singular"),
		'add_new_item' => __("Add New $singular"),
		'new_item_name' => __("New $singular Name"),
	);

	// Register and attach to 'project' post type
	register_taxonomy( strtolower($singular), 'project', array(
		'public' => true,
		'show_ui' => true,
        'show_admin_column' => true,
		'show_in_nav_menus' => true,
		'hierarchical' => true,
		'query_var' => true,
		'rewrite' => false,
		'labels' => $labels
	) );
}
add_action( 'init', 'project_taxonomy', 0 );

//Add Castome Field

    add_action("admin_init", "admin_init");
    add_action('save_post', 'save_img');
    add_action('save_post', 'save_data');
 
    function admin_init(){
    	add_meta_box("prodInfo-data-meta", "Project Data", "meta_data_options", "project", "advanced", "low");
        add_meta_box("prodInfo-meta", "Project Image", "meta_options", "project", "advanced", "low");
        }
     
     
    function meta_options(){
        global $post;
        $custom = get_post_custom($post->ID);
        $threeD_befor = $custom["threeD_befor"][0];
        $threeD_after = $custom["threeD_after"][0];
        $ft_befor = $custom["ft_befor"][0];
        $ft_after = $custom["ft_after"][0];
?>
    <p>Вставьте URL вашей image<p>
    <p>для корректного и быстрого отображения слайдера размер картинки должен быть 1024х624рх в формате jpg, по возможности чтобы мало весили</p>
    <label style="width:70px; display:inline-block;" class="custome_fild">3D befor:</label><input  style="width:300px;" name="threeD_befor" value="<?php echo $threeD_befor; ?>" /><br/>
    <label style="width:70px; display:inline-block;" class="custome_fild">3D after:</label><input  style="width:300px;" name="threeD_after" value="<?php echo $threeD_after; ?>" /><br/>
    <label style="width:70px; display:inline-block;" class="custome_fild">Ft befor:</label><input style="width:300px;" name="ft_befor" value="<?php echo $ft_befor; ?>" /><br/>
    <label style="width:70px; display:inline-block;" class="custome_fild">Ft after:</label><input style="width:300px;" name="ft_after" value="<?php echo $ft_after; ?>" /><br/>
<?php
    }
 function meta_data_options(){
        global $post;

        $custom = get_post_custom($post->ID);
        $logo = $custom["logo"][0];
        $square = $custom["square"][0];
        $number_of_lamps = $custom["number_of_lamps"][0];
        $start_year = $custom["start_year"][0];
        $power_lx_after = $custom["power_lx_after"][0];
        $power_lx_befor = $custom["power_lx_befor"][0];
        $power_wt_after = $custom["power_wt_after"][0];
        $power_wt_befor = $custom["power_wt_befor"][0];
        $free_power = $custom["free_power"][0];
        $payback = $custom["payback"][0];
?>
    <p>Вставьте URL logo вашей image<p>
    <label style="width:70px; display:inline-block;" class="custome_fild">Logo:</label><input  style="width:300px;" name="logo" value="<?php echo $logo; ?>" /><br/>
    <label style="width:120px; display:inline-block;" class="custome_fild">Площадь:</label><input  style="width:300px;" name="square" value="<?php echo $square; ?>" /><br/>
    <label style="width:120px; display:inline-block;" class="custome_fild">Количество светильников:</label><input style="width:300px;" name="number_of_lamps" value="<?php echo $number_of_lamps; ?>" /><br/>
    <label style="width:120px; display:inline-block;" class="custome_fild">Освещенность:</label>Было:<input  style="width:100px;" name="power_lx_befor" value="<?php echo $power_lx_befor; ?>" />Стало:<input  style="width:100px;" name="power_lx_after" value="<?php echo $power_lx_after; ?>" /><br/>
    <label style="width:120px; display:inline-block;" class="custome_fild">Год внедрения:</label><input style="width:300px;" name="start_year" value="<?php echo $start_year; ?>" /><br/>
    <label style="width:120px; display:inline-block;" class="custome_fild">Мощность системы:</label>Было:<input  style="width:100px;" name="power_wt_befor" value="<?php echo $power_wt_befor; ?>" />Стало:<input  style="width:100px;" name="power_wt_after" value="<?php echo $power_wt_after; ?>" /><br/>
    <label style="width:120px; display:inline-block;" class="custome_fild">Высвобождаемая мощность:</label><input  style="width:300px;" name="free_power" value="<?php echo $free_power; ?>" /><br/>
    <label style="width:120px; display:inline-block;" class="custome_fild">Срок окупаемости:</label><input style="width:300px;" name="payback" value="<?php echo $payback; ?>" /><br/>
<?php
    }
 
function save_img(){
    global $post;
    update_post_meta($post->ID, "threeD_befor", $_POST["threeD_befor"]);
    update_post_meta($post->ID, "threeD_after", $_POST["threeD_after"]);
    update_post_meta($post->ID, "ft_befor", $_POST["ft_befor"]);
    update_post_meta($post->ID, "ft_after", $_POST["ft_after"]);
}
function save_data(){
    global $post;
    update_post_meta($post->ID, "power_lx_after", $_POST["power_lx_after"]);
    update_post_meta($post->ID, "power_lx_befor", $_POST["power_lx_befor"]);
    update_post_meta($post->ID, "logo", $_POST["logo"]);
    update_post_meta($post->ID, "square", $_POST["square"]);
    update_post_meta($post->ID, "number_of_lamps", $_POST["number_of_lamps"]);
    update_post_meta($post->ID, "start_year", $_POST["start_year"]);
    update_post_meta($post->ID, "power_wt_befor", $_POST["power_wt_befor"]);
    update_post_meta($post->ID, "power_wt_after", $_POST["power_wt_after"]);
    update_post_meta($post->ID, "free_power", $_POST["free_power"]);
    update_post_meta($post->ID, "payback", $_POST["payback"]);
}
?>