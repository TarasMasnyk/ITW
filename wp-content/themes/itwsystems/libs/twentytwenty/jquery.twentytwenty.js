
(function($){

  $.fn.twentytwenty = function(options) {
    var options = $.extend({default_offset_pct: 0.5, orientation: 'horizontal'}, options);
    
   
    
    // var lenguage='en';
    
    
    
    return this.each(function() {
      if ($('.setLanguage span').text()==='RU'){
      var lenguage ='ru';
      } else {
        var lenguage = 'en';
      }
      var leng_befor = (lenguage==='ru')?'было':'before';
      var leng_after = (lenguage==='ru')?'стало':'after';
      var leng_fraze = (lenguage==='ru')?'включить фиктивные цвета':'Enable False colors';
      var sliderPct = options.default_offset_pct;
      var container = $(this);
      var sliderOrientation = options.orientation;
      var beforeDirection = (sliderOrientation === 'vertical') ? 'down' : 'left';
      var afterDirection = (sliderOrientation === 'vertical') ? 'up' : 'right';
      
      
      container.wrap("<div class='twentytwenty-wrapper twentytwenty-" + sliderOrientation + "'></div>");
      container.append("<div class='twentytwenty-overlay'></div>");
      var beforeImg = container.find("img:first");
      var afterImg = container.find("img:last");
      container.append("<div class='twentytwenty-handle new_style'></div>");
      var slider = container.find(".twentytwenty-handle");
      slider.append("<div class='before_button'><p>"+leng_befor+"</p></div>");
      slider.append("<div class='scrolle'></div>");
      slider.append("<div class='after_button'><p> "+leng_after+"</p></div>");
      slider.append(
      "<div class='swich_background'>" +
          "<div class='flipswitch'>" +
              "<input type='checkbox' name='flipswitch' class='flipswitch-cb' id='fs-'>" +
              "<label class='flipswitch-label' for='fs-'>" +
                  "<div class='flipswitch-inner'></div>" +
                  "<div class='flipswitch-switch'></div>" +
              "</label>" +
          "</div>" +
      "<p> "+leng_fraze+"</p>" +
      "</div>");
      /*slider.append("<div class='swich_box'></div>");
      slider.append("<div class='flipswitch-switch'></div>");
      $("div.flipswitch-switch").wrap("<label class='flipswitch-label' for='fs'></label>");
      $(".flipswitch-switch").before("<div class='flipswitch-inner'></div>");
      $(".lipswitch-label").wrap("<div class='flipswitch'></div>");
      $(".lipswitch-label").before("<input type='checkbox' name='flipswitch' class='flipswitch-cb' id='fs' checked>");
      $(".flipswitch").wrap("<div class='flipswitch-switch'></div>");
      $(".flipswitch").append("<p> включить фиктивные цвета</p>");
      $(".flipswitch").wrap("<div class='swich_background'></div>");*/
     
      //slider.append("<span class='twentytwenty-" + beforeDirection + "-arrow'></span>");
      //slider.append("<span class='twentytwenty-" + afterDirection + "-arrow'></span>");
      container.addClass("twentytwenty-container");
      beforeImg.addClass("twentytwenty-before");
      afterImg.addClass("twentytwenty-after");
      
      //var overlay = container.find(".twentytwenty-overlay");
      //overlay.append("<div class='twentytwenty-before-label'></div>");
      //overlay.append("<div class='twentytwenty-after-label'></div>");

      var calcOffset = function(dimensionPct) {
        var w = beforeImg.width();
        var h = beforeImg.height();
        return {
          w: w+"px",
          h: h+"px",
          cw: (dimensionPct*w*0.1)+"%",
          ch: (dimensionPct*h*0.1)+"%",
          cwt: (dimensionPct*w*0.1+26)+"%",
          cwb: (dimensionPct*w*0.1-35)+"%"
        };
      };

      var adjustContainer = function(offset) {
      	if (sliderOrientation === 'vertical') {
      	  beforeImg.css({
            "-webkit-clip-path" : "polygon( 0,"+offset.w+","+offset.ch+",0)",
            "clip-path" : "polygon( 0,"+offset.w+","+offset.ch+",0)"
          });
          //console.log(offset.w);
      	}
      	else {
          beforeImg.css({
            "-webkit-clip-path" : "polygon("+offset.cwt +" 0, 0 0, 0 100%, "+offset.cwb+" 100%)",
            "clip-path" : "polygon("+offset.cwt +" 0, 0 0, 0 100%, "+offset.cwb+" 100%)"
          });
          //console.log(offset.cw);
    	}
        container.css("height", offset.h);
      };

      var adjustSlider = function(pct) {
        var offset = calcOffset(pct);
        slider.css((sliderOrientation==="vertical") ? "top" : "left", (sliderOrientation==="vertical") ? offset.ch : offset.cw);
        adjustContainer(offset);
      };

      $(window).on("resize.twentytwenty", function(e) {
        adjustSlider(sliderPct);
      });

      var offsetX = 0;
      var imgWidth = 0;
      
      slider.on("movestart", function(e) {
        if (((e.distX > e.distY && e.distX < -e.distY) || (e.distX < e.distY && e.distX > -e.distY)) && sliderOrientation !== 'vertical') {
          e.preventDefault();
        }
        else if (((e.distX < e.distY && e.distX < -e.distY) || (e.distX > e.distY && e.distX > -e.distY)) && sliderOrientation === 'vertical') {
          e.preventDefault();
        }
        container.addClass("active");
        offsetX = container.offset().left;
        offsetY = container.offset().top;
        imgWidth = beforeImg.width(); 
        imgHeight = beforeImg.height();          
      });

      slider.on("moveend", function(e) {
        container.removeClass("active");
      });

      slider.on("move", function(e) {
        if (container.hasClass("active")) {
          sliderPct = (sliderOrientation === 'vertical') ? (e.pageY-offsetY)/imgHeight : (e.pageX-offsetX)/imgWidth;
          if (sliderPct < 0) {
            sliderPct = 0;
          }
          if (sliderPct > 1) {
            sliderPct = 1;
          }
          adjustSlider(sliderPct);
        }
      });

      container.find("img").on("mousedown", function(event) {
        event.preventDefault();
      });

      $(window).trigger("resize.twentytwenty");
    });
  };

})(jQuery);
