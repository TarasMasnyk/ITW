$('#top_slider').slideme({
    autoslide : true,
    autoslideHoverStop : true,
    interval : 6000,
    loop : true,
    speed : 1000,
    transition : 'zoom',

    arrows: true,
    resizable:
    {
        width: 1170,
        height: 600
    },

    labels : {
        next: '',
        prev: ''
    }
});

$('#pf_slider').slideme({
    loop : true,
    speed : 1000,
    transition : 'slide',

    arrows: true,
    resizable:
    {
        width: 1170,
        height: 600
    },

    labels : {
        next: '',
        prev: ''
    }
});

$(document).on("click", ".project__filter__categories__list > li", function () {
    var category = $(this).attr('data-category');

    $(this).parent().find('li').removeClass('active');
    $(this).addClass('active');

    if (category == 'all')
    {
        $('.projects .project__items article').fadeIn();
    }
    else
    {
        $('.projects .project__items article').hide();
        $('.projects .project__items article[data-category=\'' + category + '\']').fadeIn();
    }
});

$(window).load(function(){
    var imgContainer = $("div[id^='twentytwenty-id-']");
    imgContainer.twentytwenty();
    imgContainer.each(function(index) {
        $(this).find("input[id^='fs-']").attr('id', 'fs-'+index);
        $(this).find("label[for^='fs-']").attr('for', 'fs-'+index);
    });
});
$(document).on('click', 'input[id^="fs-"]', function(){
    if($(this).prop("checked")){
        $(this).parent().parent().parent().parent().find('img:first').attr('src', $(this).parent().parent().parent().parent().find('img:first').attr('data-href'));
        $(this).parent().parent().parent().parent().find('img:last-of-type').attr('src', $(this).parent().parent().parent().parent().find('img:last-of-type').attr('data-href'));
        console.log($(this).attr('id'));
    } else {
        $(this).parent().parent().parent().parent().find('img:first').attr('src', $(this).parent().parent().parent().parent().find('img:first').attr('data-src'));
        $(this).parent().parent().parent().parent().find('img:last-of-type').attr('src', $(this).parent().parent().parent().parent().find('img:last-of-type').attr('data-src'));
        console.log($(this).attr('id'));
    }
});
$(document).on('click', 'div.block-led', function(){
    if($('img.lampe').attr('src') == $('img.lampe').attr('data-src')) {
        $('img.lampe').attr('src', $('img.lampe').attr('data-href'));
        console.log('if');
    } else {
        $('img.lampe').attr('src', $('img.lampe').attr('data-src'));
        console.log('else');
    }
});


// $('.block_employer').hover(function(){
//      $(this).toggleClass('active');
// });


$(document).on("click", ".tab__navigation .tabs__list > li", function () {
    var tab = $(this).attr('data-tab');

    $('.tab__navigation .tabs__list li').removeClass('btn-yellow').addClass('btn-green');
    $('.tab__navigation .tabs__list li[data-tab="' + tab + '"]').addClass('btn-yellow');

    $('.tab__navigation .tabs__content .tabs__content__item').removeClass('active');
    $('.tab__navigation .tabs__content .tabs__content__item[data-tab="' + tab + '"]').addClass('active');
});

$(document).on("click", ".mapLocationBlocker", function () {
    $('.mapLocationBlocker').hide();
});

$(document).on("mouseover", ".yandexMapLocation", function () {
    clearTimeout(window.mapBlockerTimer);
});

$(document).on("mouseout", ".yandexMapLocation", function () {
    window.mapBlockerTimer = setTimeout(function () {
        $('.mapLocationBlocker').fadeIn();
    }, 5000);
});

$(document).on("click", "#searchbarButton", function () {
    $('#searchform').submit();
});

$(document).on("click", "#searchAsideButton", function () {
    $('#searchAsideform').submit();
});

var f_slider_v = $(".range_catalog_filter_vt").ionRangeSlider({
    type: "double",
    grid: false,
    min: 0,
    max: 2900,
    from: 480,
    to: 2100,
    postfix: "р.",
    onFinish : function (data) {
        $('.products .products__item').each(function () {
            $(this).hide();

            if ($(this).attr('data-vt') > data.from && $(this).attr('data-vt') < data.to)
                $(this).show();
        });
    }
});
var f_slider_l = $(".range_catalog_filter_lm").ionRangeSlider({
    type: "double",
    grid: false,
    min: 0,
    max: 2900,
    from: 480,
    to: 2100,
    postfix: "р.",
    onFinish : function (data) {
        $('.products .products__item').each(function () {
            $(this).hide();

            if ($(this).attr('data-lm') > data.from && $(this).attr('data-lm') < data.to)
                $(this).show();
        });
    }
});
var f_slider_p = $(".range_catalog_filter_price").ionRangeSlider({
    type: "double",
    grid: false,
    min: 0,
    max: 2900,
    from: 480,
    to: 2100,
    postfix: "р.",
    onFinish : function (data) {
        $('.products .products__item').each(function () {
            $(this).hide();

            if ($(this).attr('data-price') > data.from && $(this).attr('data-price') < data.to)
                $(this).show();
        });
    }
});

$('a.colorbox').colorbox({height: "90%"});

$(document).on("click", "#gallery", function (click) {
    console.log(click);
});

$('[data-toggle="popover"]').popover();

$(document).ready(function() {
    $('#accordion').each(function(){
        $( this ).accordion({
            heightStyle: "content"
        });
    });
    var href = document.location.hash.split('#')[1];
    $('.project__filter__categories__list > li').each(function(){
        var category = $(this).attr('data-category');
        if (category == href) {
            $(".project__filter__categories__list > li").removeClass('active');
            $(this).addClass('active');
            $('.projects .project__items article').hide();
            $('.projects .project__items article[data-category=\'' + category + '\']').fadeIn();
            console.log(category);
            return false;
        }
    });
    
    
});

/*Single Page Slider*/




$(document).ready(function() {
  $('.block-carousel').each(function(){
    var sync1 = $(this).find("#sync1");
    var sync2 = $(this).find("#sync2");
    var slidesPerPage = 4;
    
    var syncedSecondary = true;

    sync1
    .owlCarousel({
      items : 1,
      slideSpeed : 2000,
      nav: false,
      autoplay: false,
      dots: false,
      loop: true,
      pagination:false,
      responsiveRefreshRate : 200
      }).on('changed.owl.carousel', syncPosition);

    // sync2
    //   .on('initialized.owl.carousel', function () {
    //     sync2.find(".owl-item").eq(0).addClass("current");
    //   })
    //   .owlCarousel({
    //   pagination:false,
    //   dots: false,
    //   nav: false,
    //   loop:true,
    //   smartSpeed: 200,
    //   slideSpeed : 500,
    //   responsiveRefreshRate : 100
    // });

  if ($('.block-carousel').hasClass('middle')){
          sync2
          .owlCarousel({
            slideBy: 1, 
            items : 3,
            pagination:false,
            dots: false,
            nav: false,
            loop:true,
            smartSpeed: 200,
            slideSpeed : 500,          
            itemsDesktop      : [1199,3],
            itemsDesktopSmall     : [979,3],
            itemsTablet       : [768,3],
            itemsMobile       : [479,1],
            responsiveRefreshRate : 100
          }).on('changed.owl.carousel', syncPosition2);
      } else {
        sync2.owlCarousel({
          items : 3,
          slideBy: 1,           
          itemsDesktop      : [1199,3],
          itemsDesktopSmall     : [979,3],
          itemsTablet       : [768,2],
          itemsMobile       : [479,1],
           pagination:false,
          dots: false,
          nav: false,
          loop:false,
          smartSpeed: 200,
          slideSpeed : 500,          
          responsiveRefreshRate : 100
        }).on('changed.owl.carousel', syncPosition2);
      }

  function syncPosition(el) {
    //if you set loop to false, you have to restore this next line
    var current = el.item.index;
    
    //if you disable loop you have to comment this block
    // var count = el.item.count-1;
    // var current = Math.round(el.item.index - (el.item.count/2) - .5);
    // if(current < 0) {
    //   current = count;
    // }
    // if(current > count) {
    //   current = 0;
    // }    
    //end block

    sync2
      .find(".owl-item")
      .removeClass("current")
      .eq(current)
      .addClass("current");
    var onscreen = sync2.find('.owl-item.active').length-1;
    var start = sync2.find('.owl-item.active').first().index();
    var end = sync2.find('.owl-item.active').last().index();
    
    if (current > end) {
      sync2.data('owl.carousel').to(current, 100, true);
    }
    if (current < start) {
      sync2.data('owl.carousel').to(current - onscreen, 100, true);
    }
  }
  
  function syncPosition2(el) {
    if(syncedSecondary) {
      var number = el.item.index;
      sync1.data('owl.carousel').to(number, 100, true);
    }
  }

  //  $('.bottom-car').on('mousewheel', function(e) {
  //     var test = e.originalEvent.deltaY;
  //     e.preventDefault ? e.preventDefault() : (e.returnValue = false);
  //     var sum=+test;
  //     console.log(test);
  //     $('.bottom-car .owl-stage').css({
  //       'transform': 'translate3d(' + sum + 'px, 0px, 0px)'
  //     })
  // });
  

sync2.on("click", ".owl-item", function(e){
    e.preventDefault();
    var number = $(this).index();
    sync1.data('owl.carousel').to(number, 300, true);
    
  });
  });
});
// $(document).ready(function() {

//   var sync1 = $("#sync1");
//   var sync2 = $("#sync2");
//   var slidesPerPage = 4; //globaly define number of elements per page
//   var syncedSecondary = true;

//   sync1.owlCarousel({
//     items : 1,
//     slideSpeed : 2000,
//     nav: true,
//     autoplay: true,
//     dots: true,
//     loop: true,
//     responsiveRefreshRate : 200,
//   });

//   sync2
//     .on('initialized.owl.carousel', function () {
//       sync2.find(".owl-item").eq(0).addClass("current");
//     })
//     .owlCarousel({
//     items : slidesPerPage,
//     dots: true,
//     nav: true,
//     smartSpeed: 200,
//     slideSpeed : 500,
//      //alternatively you can slide by 1, this way the active slide will stick to the first item in the second carousel
//     responsiveRefreshRate : 100
//   }).on('changed.owl.carousel', syncPosition2);

//   function syncPosition(el) {
   

//     sync2
//       .find(".owl-item")
//       .removeClass("current")
//       .eq(current)
//       .addClass("current");
//     var onscreen = sync2.find('.owl-item.active').length - 1;
//     var start = sync2.find('.owl-item.active').first().index();
//     var end = sync2.find('.owl-item.active').last().index();
    
//     if (current > end) {
//       sync2.data('owl.carousel').to(current, 100, true);
//     }
//     if (current < start) {
//       sync2.data('owl.carousel').to(current - onscreen, 100, true);
//     }
//   }
  
//   function syncPosition2(el) {
//     if(syncedSecondary) {
//       var number = el.item.index;
//       sync1.data('owl.carousel').to(number, 100, true);
//     }
//   }
  
//   sync2.on("click", ".owl-item", function(e){
//     e.preventDefault();
//     var number = $(this).index();
//     sync1.data('owl.carousel').to(number, 300, true);
//   });
// });
  // sync1.owlCarousel({
  //   singleItem : true,
  //   slideSpeed : 1000,
  //   navigation: true,
  //   pagination : false,
  //   afterAction : syncPosition,
  //   responsiveRefreshRate : 200,
  // });
 
  // sync2.owlCarousel({
  //   items : 3,
  //   itemsDesktop      : [1199,3],
  //   itemsDesktopSmall     : [979,3],
  //   itemsTablet       : [768,2],
  //   itemsMobile       : [479,1],
  //   pagination:false,
  //   responsiveRefreshRate : 100,
  //   afterInit : function(el){
  //     el.find(".owl-item").eq(0).addClass("synced");
  //   }
  // });
 
  // function syncPosition(el){
  //   var current = this.currentItem;
  //   $("#sync2")
  //     .find(".owl-item")
  //     .removeClass("synced")
  //     .eq(current)
  //     .addClass("synced")
  //   if($("#sync2").data("owlCarousel") !== undefined){
  //     center(current)
  //   }
  // }
 
  // $("#sync2").on("click", ".owl-item", function(e){
  //   e.preventDefault();
  //   var number = $(this).data("owlItem");
  //   sync1.trigger("owl.goTo",number);
  // });
 
  // function center(number){
  //   var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
  //   var num = number;
  //   var found = false;
  //   for(var i in sync2visible){
  //     if(num === sync2visible[i]){
  //       var found = true;
  //     }
  //   }
 
  //   if(found===false){
  //     if(num>sync2visible[sync2visible.length-1]){
  //       sync2.trigger("owl.goTo", num - sync2visible.length+2)
  //     }else{
  //       if(num - 1 === -1){
  //         num = 0;
  //       }
  //       sync2.trigger("owl.goTo", num);
  //     }
  //   } else if(num === sync2visible[sync2visible.length-1]){
  //     sync2.trigger("owl.goTo", sync2visible[1])
  //   } else if(num === sync2visible[0]){
  //     sync2.trigger("owl.goTo", num-1)
  //   }
    
  // }
 
