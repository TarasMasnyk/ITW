<?php get_header(); ?>
<?php $options = get_option( 'wpuniq_theme_options' ); ?>
<?php  $l = getLangM(); ?>
    
    <section class="mainContent__block block__content catalog new">
        <!-- <div class="mainContainer breadcrumbs_block">
        <?php custom_breadcrumbs(); ?>
     <div class="clearfix"></div>
    </div> -->
        <div class="heading-block">
            <h2 class="block__title"><?php echo $l == 'ru' ? "Каталог" : "Catalog"; ?></h2>
            <a href="<?php echo $options[download_project]; ?>" class="download-catalogue-btn" download><i class="dwnld-icon"></i><?php echo $l == 'ru' ? "Скачать каталог" : "Download"; ?></a>
        </div>  
       
        <div class="catalog__content products mainContainer">
            <?php  if ( have_posts()):
                 while (have_posts()): the_post();
                 $attachmentId = get_post_thumbnail_id($post->ID);
                 $thymbUrl = wp_get_attachment_url($attachmentId, 'full', true); // берем URL миниатюры, которая относится к записи
                 $the_filter_cats= get_the_terms( $post->ID, 'item' );
                 foreach($the_filter_cats as $the_filter_cat){
                 $the_single_cat =$the_filter_cat->name;
                 } 
                 // $item_off = get_post_custom_values('item_off', $post->ID); 
                 // $item_on= get_post_custom_values('item_on',$post->ID);
                 ?> 
            <product class="products__item ">
                <a href="<?php the_permalink() ?>"><figure >
                    <img  src="<?php the_field('imege_slid_1'); ?>" />
                   <img src="<?php the_field('imege_slid_2'); ?>" />
                </figure></a>
                <div class="products__item__title"><?php the_title(); ?></div>
                <hr />
                    <div class="products__item__actions">
                        <a href="<?php the_permalink() ?>" class="products__item__buy button btn-green" data-product-id="1"><?php echo $l == 'ru' ? "Подробнее" : "Details"; ?></a>
                        <div class="clearfix"></div>
                    </div>
            </product>
             <?php endwhile;
                 else :
                    echo '<p>No content found</p>';
                    endif;
                    get_template_part( 'content-bottom', get_post_format() );
                ?>                            
             <!-- ADDED BLOCK -->
            <div class="btn-block">
                <a href="<?php echo $options[download_project]; ?>" class="download-catalogue-btn" download><i class="dwnld-icon"></i><?php echo $l == 'ru' ? "Скачать каталог" : "Download"; ?></a>
            </div>
            <div class="clearfix"></div>
        </div>
    </section> 
<?php get_footer(); ?>