<?php

class ProductionDevelopCalculator
{
    private static $instance = null;

    public static function getInstance(){
        if (self::$instance == NULL){
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function generate ($type_id = 'default', $l_mm = 220, $l_lm = 1680, $l_vt = 75, $amount = '3', $width = '6', $height = '5', $length = '5')
    {
        $volume = $width * $height * $length;
        $block = '<div class="production__block__item" data-volume="' . $volume . '" data-light-amount="' . $this->getLightAmount ($type_id, $amount, true) . '"><span></span></div>';

        $href = dirname(__FILE__);
        $theme = str_replace(array('/home/u617653556/public_html', 'includes'), array('http://'.$_SERVER["HTTP_HOST"], ''), $href);

        $content = '<link type="text/css" rel="stylesheet" href="' . $theme . '/libs/bootstrap/css/bootstrap.css" media="screen" />';
        $content .= '<link type="text/css" rel="stylesheet" href="' . $theme . '/libs/bootstrap/css/bootstrap-theme.css" media="screen" />';
        $content .= '<link type="text/css" rel="stylesheet" href="' . $theme . '/libs/fonts/awesome/scss/font-awesome.css" media="screen" />';
        $content .= '<link type="text/css" rel="stylesheet" href="' . $theme . '/css/theme.css" media="screen" />';

        $content .= '<style type="text/css">.production__container {transform-origin: 0 0; transform: scale(1);}.production__block__light {transform-origin: 0 0; transform: scale(' . ($this->getLightAmount ($type_id, 1) / $amount) / 100 . ');}</style>';

        $content .= '<section class="production__container" style="width: ' . $width * 100 . 'px">';
        $content .= '<div class="production__block__light" style="width: ' . $l_mm / 10 . 'px; height:' . $l_mm / 10 . 'px; margin: -' . (($l_mm / 10) + ($l_mm / 10 / 2)) . 'px 0 0 -' . (($l_mm / 10) + ($l_mm / 10 / 2)) . 'px; background: rgba(255,255,255, ' . $l_lm / 15000 . ');"><span style="background: rgba(255,255,255, ' . $l_vt / 300 . ');"></span></div>';

        $light_count_ls = $length / $amount;
        $light_count_ws = $width / $amount;

        for ($l = 1; $l <= $length; $l++)
        {
            $content .= '<div class="production__item__line">';

            for ($w = 1; $w <= $width; $w++)
            {
                $content .= $block;
            }

            $content .= '<div class="clearfix"></div>';
            $content .= '</div>';
        }

        $content .= '</section>';

        $href = dirname(__FILE__) . '/calculated_form.html';

        $fp = fopen($href, 'w');
        fwrite($fp, $content);
        fclose($fp);
    }

    public function structure ($type_id = 'default', $l_mm = 220, $l_lm = 1680, $l_vt = 75, $amount = '3', $width = '6', $height = '5', $length = '5')
    {
        $volume = $width * $height * $length;

        $block = '<div class="production__block__item" data-volume="' . $volume . '" data-light-amount="' . $this->getLightAmount ($type_id, $amount, true) . '"><span></span></div>';

        $href = dirname(__FILE__);
        $theme = str_replace(array('/home/u617653556/public_html', 'includes'), array('http://'.$_SERVER["HTTP_HOST"], ''), $href);

        $content = '<link type="text/css" rel="stylesheet" href="' . $theme . '/libs/bootstrap/css/bootstrap.css" media="screen" />';
        $content .= '<link type="text/css" rel="stylesheet" href="' . $theme . '/libs/bootstrap/css/bootstrap-theme.css" media="screen" />';
        $content .= '<link type="text/css" rel="stylesheet" href="' . $theme . '/libs/fonts/awesome/scss/font-awesome.css" media="screen" />';
        $content .= '<link type="text/css" rel="stylesheet" href="' . $theme . '/css/theme.css" media="screen" />';

        $content .= '<style type="text/css">.production__container {transform-origin: 0 0; transform: scale(1);}.production__block__light {transform-origin: 0 0; transform: scale(' . ($this->getLightAmount ($type_id, 1) / $amount) / 100 . ');}</style>';

        $content .= '<section class="production__container" style="width: ' . $width * 100 . 'px">';
        $content .= '<div class="production__block__light" style="width: ' . $l_mm / 10 . 'px; height:' . $l_mm / 10 . 'px; margin: -' . (($l_mm / 10) + ($l_mm / 10 / 2)) . 'px 0 0 -' . (($l_mm / 10) + ($l_mm / 10 / 2)) . 'px; background: rgba(255,255,255, ' . $l_lm / 15000 . ');"><span style="background: rgba(255,255,255, ' . $l_vt / 300 . ');"></span></div>';

        $light_count_ls = $length / $amount;
        $light_count_ws = $width / $amount;

        $light_count_l = round($length / $light_count_ls);
        $light_count_w = round($width / $light_count_ws);

        for ($l = 1; $l <= $length; $l++)
        {
            $content .= '<div class="production__item__line">';

            for ($w = 1; $w <= $width; $w++)
            {
                $content .= $block;
            }

            $content .= '<div class="clearfix"></div>';
            $content .= '</div>';
        }

        $content .= '</section>';

        $href = dirname(__FILE__) . '/calculated_form.html';

        $fp = fopen($href, 'w');
        fwrite($fp, $content);
        fclose($fp);

        $href = str_replace('/home/u617653556/public_html', 'http://'.$_SERVER["HTTP_HOST"], $href);

        echo '<iframe src="' . $href . '" style="border: 0; width: 100%; height: 545px;;" frameborder=\'0\' scrolling="no" id="calcblocksrelated"></iframe>';
    }

    public function getLightAmount ($type_id, $amount, $vt = false, $lm = false)
    {
        require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-config.php');
        global $post;

        $post = get_post($type_id);
        $custom_fields = get_post_custom($post->ID);

        // Кол-во ват/люма на все помещение
        $light_amount_vt = $custom_fields['product_light_vt'][0] * $amount;
        $light_amount_lm = $custom_fields['product_light_lm'][0] * $amount;

        if ($vt)
            return $custom_fields['product_light_vt'][0];
        elseif ($lm)
            return $custom_fields['product_light_lm'][0];
        else
            return $light_amount_vt + $light_amount_lm;
    }
}