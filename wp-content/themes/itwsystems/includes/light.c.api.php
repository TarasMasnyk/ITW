<?php

CLASS LightCalculatorAPI
{
    public static $instance = null;
    public static function getInstance ()
    {
        if (self::$instance == NULL)
            self::$instance = new self();

        return self::$instance;
    }

    function __construct ()
    {

    }

    /**
     * В зависимости от высоты установки светильника - подбирается тип светильника (отражатели) и его световой поток.
     * Пока работает нормально с высоты 5,5-25м (отражатель тип-Д).
     *
     * Для каждого типа помещения и типа отражателя будет своя база данных (индекс помещения (70/40/20) и коэффициент шага),
     * по которой по формулам необходимо будет пересчитывать световой поток и кол-во светильников.
     *
     * @param float $a_length     - Длинна аллеи
     * @param float $a_width      - Ширина аллеи
     * @param float $a_amount     - Кол-во аллей
     * @param float $l_height     - Высота подвеса светильника
     * @param float $illumination - Нормируемая освещенность
     * @param float $workplane    - Рабочая плоскость
     * @param float $safety       - Коэфициент запаса
     *
     * @return string
     */
    public function get_warehouse ($a_length, $a_width, $a_amount, $l_height, $illumination, $workplane, $safety)
    {
        $answer = array('code' => 200, 'error_msg' => false);

        # # # # # # # # # # # # # # # # # # # # # # # # # # #

        #

        # # # # # # # # # # # # # # # # # # # # # # # # # # #

        if($answer['code'] == 200 || !$answer['error_msg']) unset($answer['error_msg']);
        return json_encode($answer);
    }

    public function get_office ()
    {

    }

    private function Database ()
    {
        $database = array(
            array (

            )
        );
    }
}