<div class="border-tab__wrapper tab__navigation calculator">
    <ul class="tabs__list list-unstyled">
        <li class="button btn-yellow btn-big" data-tab="shelving"><i class="fa fa-building"></i> Стелажное хранение</li>
        <li class="button btn-green btn-big" data-tab="office"><i class="fa fa-laptop"></i> Офисные помещения</li>
    </ul>
    <section class="tabs__content">
        <div class="tabs__content__item shelving active" data-tab="shelving">
            <div class="pull-left" style="width: 460px;">
                <div class="parameter_filter">
                    <header>Длина аллеи<small></small></header>
                    <input type="text" id="a_length" name="A" />
                </div>
                <div class="parameter_filter">
                    <header>Ширина аллеи<small></small></header>
                    <input type="text" id="b_width" name="B" />
                </div>
                <div class="parameter_filter">
                    <header>Количество аллей<small></small></header>
                    <input type="text" id="N_amount" name="Nal" />
                </div>
                <div class="parameter_filter">
                    <header>Высота подвеса светильника<small></small></header>
                    <input type="text" id="Hp_height" name="Np" />
                </div>
                <div class="parameter_filter">
                    <header>Нормируемая освещенность<small></small></header>
                    <input type="text" id="e_illumination" name="E" />
                </div>
                <div class="parameter_filter">
                    <header>Рабочая плоскость<small></small></header>
                    <input type="text" id="hp_over" name="hp" />
                </div>
                <div class="parameter_filter">
                    <abbr class="pull-right"
                          data-container="body"
                          data-html="true"
                          data-toggle="popover"
                          data-placement="top"
                          data-trigger="hover"
                          data-content="
                                      <p>К=1 - пустое помещение, освещенность без запаса, при загрязнении элементов отражателей, плафона или помещения средняя освещенность будет менее указанной.</p>
                                      <p>К=1,25 - заполненное помещение мебелью, оборудованием и тд, при загрязнении элементов отражателей, плафона или помещения средняя освещенность будет менее указанной.</p>
                                      <p>К= 1,42 - заполненное помещение мебелью, оборудованием и тд, при загрязнении элементов отражателей, плафона или помещения средняя освещенность будет не менее указанной.</p>

                                      Не трогайте поле если не уверены в выборе значения.
                                      ">
                        <i class="fa fa-2x fa-question-circle"></i>
                    </abbr>
                    <header>Коэффициент запаса<small>Укажите какой коэф-т запаса вы хотите</small></header>
                    <input type="text" id="Kz_safety" name="Kz" />
                </div>
                <script type="text/javascript">
                    var filter_a = $("#a_length").ionRangeSlider({
                        type: "single",
                        grid: false,
                        min: 1,
                        max: 120,
                        from: 40,
                        step: 0.1,
                        keyboard: true,
                        keyboard_step: 0.1,
                        postfix: " м",
                        onFinish : function (data) { $(data.input[0]).val(data.from); }
                    });
                    var filter_b = $("#b_width").ionRangeSlider({
                        type: "single",
                        grid: false,
                        min: 3,
                        max: 4,
                        from: 3.3,
                        step: 0.1,
                        keyboard: true,
                        keyboard_step: 0.1,
                        postfix: " м",
                        onFinish : function (data) { $(data.input[0]).val(data.from); }
                    });
                    var filter_H = $("#N_amount").ionRangeSlider({
                        type: "single",
                        grid: false,
                        min: 1,
                        max: 100,
                        from: 1,
                        step: 1,
                        keyboard: true,
                        keyboard_step: 1,
                        postfix: " шт",
                        onFinish : function (data) { $(data.input[0]).val(data.from); }
                    });
                    var filter_Hp = $("#Hp_height").ionRangeSlider({
                        type: "single",
                        grid: false,
                        min: 3,
                        max: 25,
                        from: 7,
                        step: 0.1,
                        keyboard: true,
                        keyboard_step: 0.1,
                        postfix: " м",
                        onFinish : function (data) { $(data.input[0]).val(data.from); }
                    });
                    var filter_E = $("#e_illumination").ionRangeSlider({
                        type: "single",
                        grid: false,
                        min: 50,
                        max: 2000,
                        from: 200,
                        step: 25,
                        postfix: " лк",
                        onFinish : function (data) { $(data.input[0]).val(data.from); }
                    });
                    var filter_hp = $("#hp_over").ionRangeSlider({
                        type: "single",
                        grid: false,
                        min: 0,
                        max: 8,
                        from: 0,
                        step: 0.1,
                        postfix: " м",
                        onFinish : function (data) { $(data.input[0]).val(data.from); }
                    });
                    var filter_Kz = $("#Kz_safety").ionRangeSlider({
                        type: "single",
                        grid: false,
                        min: 0.5,
                        max: 2,
                        from: 1.25,
                        step: 0.05,
                        keyboard: true,
                        keyboard_step: 0.1,
                        postfix: "",
                        onFinish : function (data) { $(data.input[0]).val(data.from); }
                    });
                </script>
            </div>
            <div class="pull-right" style="width: 600px;">
                <div class="result_field text-center">
                    <p class="lead">Результат расчетов<br /><small class="text-info">Расчет результата может занять до нескольких минут, не обновляйте страницу!</small></p>
                    <span class="button btn-big btn-green" id="getResult">Расчитать результат</span>
                </div>
                <script type="text/javascript">
                    $(document).on('click', ".shelving #getResult", function () {
                        var filters = $(".calculator .shelving .parameter_filter").find('input[type=text]');
                        var data = {};

                        $.each (filters, function (index, input)
                        {
                            data[$(input).attr('name')] = input.value;
                        });

                        $.ajax({
                            url: "<?php echo get_template_directory_uri(); ?>/api/calculator/warehouse/",
                            method: "POST",
                            data: data,
                            dataType: "json",
                            beforeSend: function ()
                            {
                                $(".shelving #getResult").replaceWith($('<span>', {
                                    'class': "fa fa-spin fa-cog fa-5x",
                                    'id'   : "spinner"
                                }));
                            },
                            success: function (answer)
                            {
                                $(".shelving #spinner").replaceWith($('<span>', {
                                    'class': "button btn-big btn-green",
                                    'id'   : "getResult"
                                }).html("Расчитать еще раз"));

                                var answer_field = $('<table>', {class:"answer_data table table-striped table-bordered", style: "margin: 15px 0 0 0;"});

                                $.each (answer['data'], function (title, field)
                                {
                                    $(answer_field).append($('<tr>')
                                        .append($('<td>', {style: "width: 75%;"}).html(getRealTitleWarehouse(title)))
                                        .append($('<td>').html(field)));
                                });

                                function getRealTitleWarehouse(title)
                                {
                                    switch (title)
                                    {
                                        case "A":   title = "Длина аллеи";
                                            break;
                                        case "B":   title = "Ширина аллеи";
                                            break;
                                        case "Nal": title = "Кол-во аллей";
                                            break;
                                        case "NAL": title = "Кол-во аллей (подгонка)";
                                            break;
                                        case "Np":  title = "Высота подвеса светильника";
                                            break;
                                        case "E":   title = "Нормируемая освещенность";
                                            break;
                                        case "hp":  title = "Рабочая плоскость";
                                            break;
                                        case "Kz":  title = "Коэффициент запаса";
                                            break;
                                        case "nUD": title = "Коэф. Шага Д-Алюминий";
                                            break;
                                        case "nUC": title = "Коэф. Шага С-Алюминий";
                                            break;
                                        case "z":   title = "Коэффициент неравномерности освещения";
                                            break;
                                        case "Hp":  title = "Высота подвеса светильников над рабочей поверхностью";
                                            break;
                                        case "S":   title = "Площадь освещаемого помещения";
                                            break;
                                        case "Na":  title = "Кол-во светильников на 1 аллеи";
                                            break;
                                        case "Nb":  title = "Количество аллей";
                                            break;
                                        case "N":   title = "Количество светильников";
                                            break;
                                        case "T":   title = "Расстояние между светильниками";
                                            break;
                                        case "i":   title = "Индекс помещения Д-Алюминий";
                                            break;
                                        case "KiD": title = "Коэффициент использования светового потока Д отражатель";
                                            break;
                                        case "KiC": title = "Коэффициент использования светового потока С отражатель";
                                            break;
                                        case "LfD": title = "Световой поток Д-Алюминий";
                                            break;
                                        case "LfC": title = "Световой поток C-Алюминий";
                                            break;
                                    }

                                    return title;
                                }

                                $('.shelving .answer_data').remove();
                                $('.shelving .result_field').after(answer_field);
                                $(answer_field).before($('<p>', {class:"lead answer_data", style:"margin: 20px 0 0 0;"}).html("Результат расчета:"));
                            },
                            error: function ()
                            {
                                if ($(".shelving #spinner").length > 0) $("#spinner").replaceWith($('<span>', {
                                    'class': "text-danger lead",
                                    'id'   : "resultError"
                                }).html("Системная ошибка. Обновите страницу и попробуйте снова.<br />").append($('<small>', {
                                    'class': "text-info"
                                }).html("Если ошибка повторится, обратитесь к администрации сайта.")));
                            }
                        });
                    });
                </script>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="tabs__content__item office" data-tab="office">
            <div class="pull-left" style="width: 460px;">
                <div class="parameter_filter">
                    <header>Длина помещения<small></small></header>
                    <input type="text" id="of_a_length" name="A" />
                </div>
                <div class="parameter_filter">
                    <header>Ширина помещения<small></small></header>
                    <input type="text" id="of_b_width" name="B" />
                </div>
                <div class="parameter_filter">
                    <header>Высота подвеса светильника<small></small></header>
                    <input type="text" id="of_Hp_height" name="Np" />
                </div>
                <div class="parameter_filter">
                    <header>Рабочая плоскость<small></small></header>
                    <input type="text" id="of_hp_over" name="hp" />
                </div>
                <div class="parameter_filter">
                    <header>Нормируемая освещенность<small></small></header>
                    <input type="text" id="of_e_illumination" name="E" />
                </div>
                <div class="parameter_filter">
                    <abbr class="pull-right"
                          data-container="body"
                          data-html="true"
                          data-toggle="popover"
                          data-placement="top"
                          data-trigger="hover"
                          data-content="
                                      <p>К=1 - пустое помещение, освещенность без запаса, при загрязнении элементов отражателей, плафона или помещения средняя освещенность будет менее указанной.</p>
                                      <p>К=1,25 - заполненное помещение мебелью, оборудованием и тд, при загрязнении элементов отражателей, плафона или помещения средняя освещенность будет менее указанной.</p>
                                      <p>К= 1,42 - заполненное помещение мебелью, оборудованием и тд, при загрязнении элементов отражателей, плафона или помещения средняя освещенность будет не менее указанной.</p>

                                      Не трогайте поле если не уверены в выборе значения.
                                      ">
                        <i class="fa fa-2x fa-question-circle"></i>
                    </abbr>
                    <header>Коэффициент запаса<small>Укажите какой коэф-т запаса вы хотите</small></header>
                    <input type="text" id="of_Kz_safety" name="Kz" />
                </div>
                <script type="text/javascript">
                    var filter_a = $("#of_a_length").ionRangeSlider({
                        type: "single",
                        grid: false,
                        min: 1,
                        max: 40,
                        from: 10.7,
                        step: 0.1,
                        keyboard: true,
                        keyboard_step: 0.1,
                        postfix: " м",
                        onFinish : function (data) { $(data.input[0]).val(data.from); }
                    });
                    var filter_b = $("#of_b_width").ionRangeSlider({
                        type: "single",
                        grid: false,
                        min: 1,
                        max: 40,
                        from: 6,
                        step: 0.1,
                        keyboard: true,
                        keyboard_step: 0.1,
                        postfix: " м",
                        onFinish : function (data) { $(data.input[0]).val(data.from); }
                    });
                    var filter_Hp = $("#of_Hp_height").ionRangeSlider({
                        type: "single",
                        grid: false,
                        min: 2.5,
                        max: 5,
                        from: 3,
                        step: 0.1,
                        keyboard: true,
                        keyboard_step: 0.1,
                        postfix: " м",
                        onFinish : function (data) { $(data.input[0]).val(data.from); }
                    });
                    var filter_hp = $("#of_hp_over").ionRangeSlider({
                        type: "single",
                        grid: false,
                        min: 0,
                        max: 8,
                        from: 0,
                        step: 0.1,
                        postfix: " м",
                        onFinish : function (data) { $(data.input[0]).val(data.from); }
                    });
                    var filter_E = $("#of_e_illumination").ionRangeSlider({
                        type: "single",
                        grid: false,
                        min: 50,
                        max: 2000,
                        from: 400,
                        step: 25,
                        postfix: " лк",
                        onFinish : function (data) { $(data.input[0]).val(data.from); }
                    });
                    var filter_Kz = $("#of_Kz_safety").ionRangeSlider({
                        type: "single",
                        grid: false,
                        min: 0.5,
                        max: 2,
                        from: 1.25,
                        step: 0.05,
                        keyboard: true,
                        keyboard_step: 0.1,
                        postfix: "",
                        onFinish : function (data) { $(data.input[0]).val(data.from); }
                    });
                </script>
            </div>
            <div class="pull-right" style="width: 600px;">
                <div class="result_field text-center">
                    <p class="lead">Результат расчетов<br /><small class="text-info">Расчет результата может занять до нескольких минут, не обновляйте страницу!</small></p>
                    <span class="button btn-big btn-green" id="getResult">Расчитать результат</span>
                </div>
                <script type="text/javascript">
                    $(document).on('click', ".office #getResult", function () {
                        var filters = $(".calculator .office .parameter_filter").find('input[type=text]');
                        var data = {};

                        $.each (filters, function (index, input)
                        {
                            data[$(input).attr('name')] = input.value;
                        });

                        $.ajax({
                            url: "<?php echo get_template_directory_uri(); ?>/api/calculator/office/",
                            method: "POST",
                            data: data,
                            dataType: "json",
                            beforeSend: function ()
                            {
                                $(".office #getResult").replaceWith($('<span>', {
                                    'class': "fa fa-spin fa-cog fa-5x",
                                    'id'   : "spinner"
                                }));
                            },
                            success: function (answer)
                            {
                                $(".office #spinner").replaceWith($('<span>', {
                                    'class': "button btn-big btn-green",
                                    'id'   : "getResult"
                                }).html("Расчитать еще раз"));

                                var answer_field = $('<table>', {class:"answer_data table table-striped table-bordered", style: "margin: 15px 0 0 0;"});

                                $.each (answer['data'], function (title, field)
                                {
                                    $(answer_field).append($('<tr>')
                                        .append($('<td>', {style: "width: 75%;"}).html(getRealTitleOffice(title)))
                                        .append($('<td>').html(field)));
                                });

                                function getRealTitleOffice(title)
                                {
                                    switch (title)
                                    {
                                        case "A":   title = "Длина помещения";
                                            break;
                                        case "B":   title = "Ширина помещения";
                                            break;
                                        case "Np":  title = "Высота подвеса светильника";
                                            break;
                                        case "E":   title = "Нормируемая освещенность";
                                            break;
                                        case "hp":  title = "Рабочая плоскость";
                                            break;
                                        case "Kz":  title = "Коэффициент запаса";
                                            break;
                                        case "z":   title = "Коэффициент неравномерности освещения";
                                            break;
                                        case "Ks":  title = "Коэффициент шага";
                                            break;
                                        case "Hp":  title = "Высота подвеса светильников над рабочей поверхностью";
                                            break;
                                        case "S":   title = "Площадь освещаемого помещения";
                                            break;
                                        case "i":   title = "Индекс помещения админ";
                                            break;
                                        case "KiA": title = "Коэффициент использования светового потока админ";
                                            break;
                                        case "KiP": title = "Коэффициент использования светового потока пром";
                                            break;
                                        case "FlA": title = "Световой поток админ на помещение";
                                            break;
                                        case "FlP": title = "Световой поток пром на помещение";
                                            break;
                                        case "Na":  title = "Количество светильников по стороне А";
                                            break;
                                        case "NbP": title = "Количество светильников по стороне B (пром)";
                                            break;
                                        case "NbA": title = "Количество светильников по стороне B (админ)";
                                            break;
                                        case "NA":  title = "Необходимое количество админ светильников";
                                            break;
                                        case "NP":  title = "Необходимое количество пром светильников";
                                            break;
                                        case "Fa":  title = "Световой поток административного cветильника";
                                            break;
                                        case "Fp":  title = "Световой поток пром ветильника без отражателей";
                                            break;
                                    }

                                    return title;
                                }

                                $('.office .answer_data').remove();
                                $('.office .result_field').after(answer_field);
                                $(answer_field).before($('<p>', {class:"lead answer_data", style:"margin: 20px 0 0 0;"}).html("Результат расчета:"));
                            },
                            error: function ()
                            {
                                if ($(".office #spinner").length > 0) $("#spinner").replaceWith($('<span>', {
                                    'class': "text-danger lead",
                                    'id'   : "resultError"
                                }).html("Системная ошибка. Обновите страницу и попробуйте снова.<br />").append($('<small>', {
                                    'class': "text-info"
                                }).html("Если ошибка повторится, обратитесь к администрации сайта.")));
                            }
                        });
                    });
                </script>
            </div>
            <div class="clearfix"></div>
        </div>
    </section>
</div>