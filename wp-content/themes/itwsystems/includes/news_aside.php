<aside class="blog__sidebars">
    <div class="blog__sidebars__block">
        <div class="block__title">Архив нвостей</div>
        <div class="block__content">
            <?php
            $yearly = array(
                'type'            => 'yearly',
                'cat'             => 16,
                'limit'           => false,
                'format'          => 'html',
                'before'          => false,
                'after'           => ' год',
                'show_post_count' => false,
                'echo'            => 1
            );
            $monthly = array(
                'type'            => 'monthly',
                'cat'             => 16,
                'limit'           => false,
                'format'          => 'html',
                'before'          => false,
                'after'           => false,
                'show_post_count' => false,
                'echo'            => 1
            );
            ?>
            <ul class="list-unstyled archive_list">
                <?php wp_get_archives($yearly); ?>
                <li class="archive__list__month">
                    <ul class="list-unstyled"><?php wp_get_archives($monthly); ?></ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="blog__sidebars__block">
        <div class="block__title">Популярные теги</div>
        <div class="block__content">
            <?php
            function trending_tags ()
            {
                $tags_content = '<div class="trending_topics">' . "\n";
                $tags_content .= "\t" . '<ul class="list-unstyled">' . "\n";

                $t_categories =  get_categories(array('taxonomy' => 'post_tag', 'orderby' => 'count', 'number' => '10' ));
                foreach ($t_categories as $t_category)
                {
                    $term_name = $t_category->cat_name;
                    $tax_term_id = $t_category->term_taxonomy_id;

                    $tags_content .= "\t\t" . '<li><a href="/tags/' . $t_category->slug . '/">' . $term_name . '</a></li>';
                }

                $tags_content .= "\t" . '</ul>' . "\n";
                $tags_content .=  "\t" . '<div class="clearfix"></div>' . "\n";
                $tags_content .= '</div>';

                return $tags_content;
            }
            ?>
            <?php echo trending_tags(); ?>
        </div>
    </div>
    <div class="blog__sidebars__block">
        <div class="block__title">Поиск по новостям</div>
        <div class="block__content">
            <aside class="searchToolBarAside">
                <form action="http://zeromind.info" method="get" id="searchAsideform">
                    <div class="genericSearchBox">
                        <label><input type="search" id="s" name="s"></label>
                        <span class="glyphicon glyphicon-search" id="searchAsideButton"></span>
                        <div class="clearfix"></div>
                    </div>
                </form>
            </aside>
        </div>
    </div>
</aside>