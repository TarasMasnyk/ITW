<?php
    include_once ('calculator.functions.php');

    $Calculator = ProductionDevelopCalculator::getInstance();
?>
<div class="border-tab__wrapper tab__navigation">
    <ul class="tabs__list list-unstyled">
        <li class="button btn-yellow btn-big" data-tab="diy"><i class="fa fa-cogs"></i> Сделай сам</li>
        <li class="button btn-green btn-big" data-tab="analog"><i class="fa fa-recycle"></i> Аналог</li>
        <li class="button btn-green btn-big" data-tab="listed"><i class="fa fa-list"></i> Выбор из списка</li>
    </ul>
    <section class="tabs__content">
        <div class="tabs__content__item active" data-tab="diy">
            <div class="pull-left" style="width: 460px;">
                <div class="calc_light_items">
                <?php
                $args = array(
                    'posts_per_page'    => 6,
                    'cat'               => 11,
                    'orderby'           => 'views',
                    'order'             => 'ASC'
                );

                $custom_query = new WP_Query($args);

                $item = 1;
                while($custom_query->have_posts()) {
                    $custom_query->the_post();
                    $thumbnail_attributes = wp_get_attachment_image_src(get_post_thumbnail_id(), 'source');


                    ?>
                    <div class="calc_products__item<?php echo $post->ID == 117 ? ' active' : ''; ?>" data-id="<?php echo $post->ID; ?>">
                        <figure><img src="<?php echo $thumbnail_attributes[0]; ?>" /></figure>
                        <div class="products__item__title"><?php the_title(); ?></div>
                    </div>
                    <?php
                    $item++;
                }

                wp_reset_query();
                ?>
                    <script type="text/javascript">
                        $(document).on("click", ".calc_products__item", function () {
                            $('.calc_light_items').find('.calc_products__item').removeClass('active');
                            $(this).addClass('active');
                        });
                    </script>
                    <div class="clearfix"></div>
                </div>
                <div class="parametr_calc_search">
                    <header>Задайте размер светильника, мм:</header>
                    <input type="text" id="calc_light_mm" name="example_name" value="" />
                    <script type="text/javascript">
                        var calc_light_mm = $("#calc_light_mm").ionRangeSlider({
                            type: "single",
                            grid: false,
                            min: 30,
                            max: 2000,
                            from: 220,
                            step: 10,
                            postfix: " мм",
                            onFinish : function (data) {
                                $('#calculateButton').attr('data-mm', data['from']);
                            }
                        });
                    </script>
                </div>
                <div class="parametr_calc_search">
                    <header>Задайте световой потом, люмен:</header>
                    <input type="text" id="calc_light_lm" name="example_name" value="" />
                    <script type="text/javascript">
                        var calc_light_lm = $("#calc_light_lm").ionRangeSlider({
                            type: "single",
                            grid: false,
                            min: 1000,
                            max: 15000,
                            from: 1680,
                            step: 100,
                            postfix: " лм",
                            onFinish : function (data) {
                                $('#calculateButton').attr('data-lm', data['from']);
                            }
                        });
                    </script>
                </div>
                <div class="parametr_calc_search">
                    <header>Задайте мощность, Вт:</header>
                    <input type="text" id="calc_light_vt" name="example_name" value="" />
                    <script type="text/javascript">
                        var calc_light_vt = $("#calc_light_vt").ionRangeSlider({
                            type: "single",
                            grid: false,
                            min: 35,
                            max: 300,
                            from: 75,
                            step: 5,
                            postfix: " Вт",
                            onChange : function (data)
                            {
                                $('#calculateButton').attr('data-vt', data['from']);
                            },
                            onFinish : function (data) {
                                $('#calculateButton').attr('data-vt', data['from']);
                            }
                        });
                    </script>
                </div>
            </div>
            <div class="pull-right" style="width: 600px; height: 650px;">
                <?php $Calculator->structure(117); ?>
                <span class="button btn-big btn-yellow" id="calculateButton">Расчитать световой поток</span>
                <script type="text/javascript">
                    $(document).on("click", "#calculateButton", function () {
                        var mm = $(this).attr('data-mm'),
                            lm = $(this).attr('data-lm'),
                            vt = $(this).attr('data-vt'),
                            id = $('.calc_products__item.active').attr('data-id');

                        if (typeof mm == 'undefined') mm = 220;
                        if (typeof lm == 'undefined') lm = 1680;
                        if (typeof vt == 'undefined') vt = 75;
                        if (typeof id == 'undefined') id = 117;

                        <?php
                        $url = str_replace('/home/u617653556/public_html', '', dirname(__FILE__) . '/calc.api.php');
                        ?>

                        $.ajax({
                            url: "<?php echo $url; ?>",
                            method: "GET",
                            data: {
                                'mm': mm,
                                'lm': lm,
                                'vt': vt,
                                'id': id
                            },
                            success: function (data) {
                                document.getElementById('calcblocksrelated').contentDocument.location.reload(true);
                            }
                        });
                    });
                </script>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="tabs__content__item" data-tab="analog">
            <?php
            $args = array(
                'posts_per_page'    => 12,
                'cat'               => 11,
                'orderby'           => 'date',
                'order'             => 'ASC',
            );

            $custom_query = new WP_Query( $args );

            while($custom_query->have_posts()) {
                $custom_query->the_post();
                $thumbnail_attributes = wp_get_attachment_image_src(get_post_thumbnail_id(), 'source');
                $custom_fields = get_post_custom();
                ?>
                <div class="pull-left" style="width: 370px; padding: 15px;">
                    <figure class="pull-left" style="border: 1px solid #dddddd;"><img src="<?php echo $thumbnail_attributes[0]; ?>" style="width: 150px; height: 135px;" /></figure>
                    <div style="width: 188px; height: 135px;" class="pull-right">
                        <strong style="margin: 5px 0 8px 10px; font-size: 16px; display: block;"><?php the_title(); ?></strong>
                        <table class="table table-stripped">
                            <tr>
                                <td style="width: 55%;">Мошность</td>
                                <td><?php echo $custom_fields['product_light_vt'][0]; ?>  Вт</td>
                            </tr>
                            <tr>
                                <td style="width: 55%;">Свет люма</td>
                                <td><?php echo $custom_fields['product_light_lm'][0]; ?> лм</td>
                            </tr>
                            <tr>
                                <td style="width: 55%;">Цена</td>
                                <td><?php echo $custom_fields['product_price'][0]; ?> р.</td>
                            </tr>
                        </table>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <?php
            }

            wp_reset_query();
            ?>
            <div class="clearfix"></div>
        </div>
        <div class="tabs__content__item" data-tab="listed">
            <?php
            $args = array(
                'posts_per_page'    => 24,
                'cat'               => 11,
                'orderby'           => 'name',
                'order'             => 'DESC',
            );

            $custom_query = new WP_Query( $args );

            while($custom_query->have_posts()) {
                $custom_query->the_post();
                $thumbnail_attributes = wp_get_attachment_image_src(get_post_thumbnail_id(), 'source');
                $custom_fields = get_post_custom();
                ?>
                <div class="pull-left" style="width: 185px; padding: 15px;">
                    <figure style="border: 1px solid #dddddd;"><img src="<?php echo $thumbnail_attributes[0]; ?>" style="width: 155px; height: 155px;" /></figure>
                    <div style="width: 155px;">
                        <strong style="margin: 5px 0 8px 10px; font-size: 14px; display: block;"><?php the_title(); ?></strong>
                        <table class="table table-stripped">
                            <tr>
                                <td style="width: 30px;"><i class="fa fa-bolt"></i></td>
                                <td><?php echo $custom_fields['product_light_vt'][0]; ?>  Вт</td>
                            </tr>
                            <tr>
                                <td style="width: 30px;"><i class="fa fa-lightbulb-o"></i></td>
                                <td><?php echo $custom_fields['product_light_lm'][0]; ?> лм</td>
                            </tr>
                            <tr>
                                <td style="width: 30px;"><i class="fa fa-money"></i></td>
                                <td><?php echo $custom_fields['product_price'][0]; ?> р.</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <?php
            }

            wp_reset_query();
            ?>
            <div class="clearfix"></div>
        </div>
    </section>
</div>