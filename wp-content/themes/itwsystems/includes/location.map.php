<script type="text/javascript">
    ymaps.ready(yMapsInit);

    function yMapsInit () {
        var map = new ymaps.Map('YMapsID', {
            center: [50.4561375, 30.4779716],
            zoom: 16,
            controls: []
        });

        var mapPlacemark = new ymaps.Placemark([50.4561375, 30.4779716], {
            name: "Kiev"
        }, {
            iconLayout: 'default#image',
            iconImageHref: '<?php echo get_template_directory_uri(); ?>/images/mapTick.png',
            iconImageSize: [46, 61],
            iconImageOffset: [-46, -61]
        });

        map.geoObjects.add(mapPlacemark);
    }

    $(document).on("click", ".ymaps-2-1-29-events-pane", function (event) {
        console.log(event);
    });
</script>
<div class="mapLocationBlocker"></div>
<div id="YMapsID" class="yandexMapLocation" style="width: 100%; height: 600px;"></div>