<?php
function getLang2 ()
{
    $current_pack_uri = array_filter(explode('/', $_SERVER['REQUEST_URI']));
    return $current_pack_uri[1] == $_COOKIE['qtrans_front_language'] ? $_COOKIE['qtrans_front_language'] : $current_pack_uri[1];
}
$l = getLang2();
?>
<form action="/wp-content/themes/itwsystems/api/feedback/" method="POST" id="contacts-form">
    <label class="block__contacts__field">
        <span><?php echo $l == 'ru' ? "Ваше имя:" : "Your name"; ?></span>
        <input type="text" name="bc_name" required />
    </label>
    <label class="block__contacts__field">
        <span><?php echo $l == 'ru' ? "Компания которую вы представляете:" : "Company you represent"; ?></span>
        <input type="text" name="bc_company" />
    </label>
    <label class="block__contacts__field">
        <span><?php echo $l == 'ru' ? "Ваш телефон для связи:" : "Phone number"; ?></span>
        <input type="tel" name="bc_phone" required />
    </label>
    <label class="block__contacts__field">
        <span><?php echo $l == 'ru' ? "Удобное время для разговора:" : "Comfortable time for a call"; ?></span>
        <input type="time" name="bc_time" value="16:00" min="10:00" max="19:00" />
    </label>
    <label class="block__contacts__field long">
        <span><?php echo $l == 'ru' ? "Ваш комментарий:" : "Comment"; ?></span>
        <textarea name="bc_comment"></textarea>
    </label>
    <button class="button btn-big btn-yellow" type="submit"><?php echo $l == 'ru' ? "Отправить заявку" : "Send a request "; ?></button>
</form>
<script type="text/javascript">
    $("#contacts-form").submit(function () {
        var form = $(this),
            data = {
            'bc_name'       : $(this).find('input[name=bc_name]').val(),
            'bc_company'    : $(this).find('input[name=bc_company]').val(),
            'bc_phone'      : $(this).find('input[name=bc_phone]').val(),
            'bc_time'       : $(this).find('input[name=bc_time]').val(),
            'bc_comment'    : $(this).find('textarea[name=bc_comment]').val()
        };

        $.ajax({
            url: "<?php echo get_template_directory_uri(); ?>/api/feedback/",
            method: "POST",
            data: data,
            dataType: "json",
            beforeSend: function ()
            {
                console.log("ok")
            },
            success: function (answer)
            {
                if (answer['code'] == '200')
                {
                    $(form).reset();
                }
            },
            error: function ()
            {

            }
        });

        return false;
    });

    $(document).on("submit", "#catalog-get-form", function () {
        var data = {
            'email'     : $(this).find('input[name=email]').val(),
            'name'      : $(this).find('input[name=full-name]').val(),
            'company'   : $(this).find('input[name=company]').val()
        };

        $.ajax({
            url: "<?php echo get_template_directory_uri(); ?>/api/mail/",
            method: "POST",
            data: data,
            dataType: "json",
            beforeSend: function ()
            {

            },
            success: function (answer)
            {
                if (answer['code'] == '200')
                {
                    var win = window.open('http://itw-systems.com/catalog.pdf', '_blank');
                    if(win){
                        win.focus();
                    }else{
                        alert('Please allow popups for this site');
                    }
                }
            },
            error: function ()
            {

            }
        });

        return false;
    });
</script>