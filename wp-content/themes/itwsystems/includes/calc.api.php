<?php

if (file_exists(dirname(__FILE__) . '/calculator.functions.php'))
{
    include_once (dirname(__FILE__) . '/calculator.functions.php');

    if (isset($_GET['id']) && is_numeric($_GET['id']))
        $light_id = $_GET['id'];
    else $light_id = 117;

    $mm = $_GET['mm']; $lm = $_GET['lm']; $vt = $_GET['vt'];

    $Calculator = ProductionDevelopCalculator::getInstance();
    $Calculator->generate($light_id, $mm, $lm, $vt);
}
else echo 'File not found';

