<?php get_header(); setPostViews(get_the_ID()); ?>
    <section class="blog__container">
        <section class="mainContainer">
            <h2 class="main__title"><span>Наш блог</span></h2>
            <section class="blog__posts">
                <?php
                $thumbnail_attributes = wp_get_attachment_image_src(get_post_thumbnail_id(), 'source');
                ?>
                <article class="blog__article">
                    <div class="blog__article__title"><?php the_title(); ?></div>
                    <div class="blog__article__data"><?php echo get_the_date("j F Y"); ?> | Posted by <span><?php echo the_author_meta('user_nicename' , $post->post_author); ?></span></div>
                    <div class="blog__article__counters">
                        <span class="blog__article__counters__views"><i class="fa fa-eye"></i> <?php echo getPostViews(get_the_ID()); ?></span>
                        <span class="blog__article__counters__comments"><i class="fa fa-comments"></i> <?php echo get_comments_number(get_the_ID()); ?></span>
                    </div>
                    <picture class="blog__article__picture">
                        <img src="<?php echo $thumbnail_attributes[0]; ?>" />
                    </picture>
                    <div class="text"><?php echo nl2br($post->post_content); ?></div>
                    <div class="blog__article__tags">
                        <?php
                        $tags = wp_get_post_tags($post->ID);
                        foreach ($tags as $tag)
                        {
                            echo '<span class="tag">' . $tag->name . '</span>';
                        }
                        ?>
                    </div>
                </article>
                <section class="comments__wrapper">
                    <?php comments_template(); ?>
                </section>
            </section>
            <?php include("includes/blog_aside.php"); ?>
            <div class="clearfix"></div>
        </section>
    </section>
<?php get_footer(); ?>