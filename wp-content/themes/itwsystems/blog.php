<?php /* Template Name: Blog [Template] */ ?>
<?php get_header(); ?>
    <section class="blog__container">
        <section class="mainContainer">
            <h2 class="main__title"><span><?php the_title(); ?></span></h2>
            <section class="blog__posts">

                <?php
                $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

                $args = array(
                    'posts_per_page'    => 3,
                    'cat'               => 10,
                    'orderby'           => 'name',
                    'order'             => 'ASC',
                    'paged'             => $paged
                );

                $custom_query = new WP_Query( $args );

                while($custom_query->have_posts()) {
                    $custom_query->the_post();

                    $thumbnail_attributes = wp_get_attachment_image_src(get_post_thumbnail_id(), 'source');
                    ?>
                    <article class="blog__article">
                        <div class="blog__article__title"><?php the_title(); ?></div>
                        <div class="blog__article__data"><?php echo get_the_date("j F Y"); ?> | Posted by <span><?php echo get_the_author(); ?></span></div>
                        <div class="blog__article__counters">
                            <span class="blog__article__counters__views"><i class="fa fa-eye"></i> <?php echo getPostViews(get_the_ID()); ?></span>
                            <span class="blog__article__counters__comments"><i class="fa fa-comments"></i> <?php echo get_comments_number(get_the_ID()); ?></span>
                        </div>
                        <picture class="blog__article__picture">
                            <img src="<?php echo $thumbnail_attributes[0]; ?>" />
                        </picture>
                        <div class="text"><?php $content = get_the_content(); echo strcrop($content, 0); ?></div>
                        <a class="button btn-big btn-green" href="<?php the_permalink() ?>">Читать далее</a>
                    </article>
                    <?php
                }

                pagination($custom_query->max_num_pages);
                ?>
            </section>
            <?php include("includes/blog_aside.php"); ?>
            <div class="clearfix"></div>
        </section>
    </section>
<?php get_footer(); ?>