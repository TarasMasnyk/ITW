<?php /* Template Name: Product [Template] */ ?>
<?php get_header(); ?>

    <section class="mainContent__block block__content catalog">
        <section class="mainContainer">
            <?php $l = $_COOKIE['qtrans_front_language']; ?>
            <aside class="catalog__block">
                <ul class="catalog__categories list-unstyled">
                    <li class="current"><a href="/catalog/"><span class="fa fa-chevron-left"></span><?php echo $l == 'ru' ? "Весь каталог" : "All catalog"; ?></a></li>
                    <?php
                    $category = get_the_category($post->ID);

                    if ($category[0]->cat_ID == 11) $category = $category[1];
                    else $category = $category[0];
                    ?>
                    <li class="current"><span class="fa fa-lightbulb-o"></span> <?php echo $category->name; ?></li>
                    <ul class="list-unstyled">
                        <?php
                        $current_post = $post;
                        $args = array(
                            'posts_per_page'    => 10,
                            'cat'               => 11,
                            'orderby'           => 'views',
                            'order'             => 'DESC'
                        );

                        $custom_query = new WP_Query($args);

                        while($custom_query->have_posts())
                        {
                            $custom_query->the_post();

                            if ($current_post->ID != get_the_ID())
                            {
                                if (preg_match("#-" . $_COOKIE['itw_lang'] . "$#", $post->slug) == 1)
                                {
                                    ?>
                                    <li><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>
                                    <?php
                                }
                            }
                        }

                        wp_reset_query();
                        ?>
                    </ul>
                </ul>
            </aside>
            <div class="catalog__content product">
                <?php
                $thumbnail_attributes = wp_get_attachment_image_src(get_post_thumbnail_id(), 'source');
                $content = get_the_content();
                $o_content = $content;

                $content = explode("<!--more-->", $content);
                ?>
                <div>
                    <figure class="pull-left">
                        <img src="<?php echo $thumbnail_attributes[0]; ?>" style="max-width: 370px; max-height: 370px;" />
                        <span></span>
                    </figure>
                    <div class="pull-right" style="width: 475px;">
                        <h2 style="font-size: 28px; font-weight: 300;"> <?php the_title(); ?></h2>
                        <p class="text"><?php echo $content[0]; ?></p>
                        <div style="border-top: 3px dotted #D0d0d0; margin: 25px 0 0 0; padding: 20px 0 0 0;">
<!--                            <div style="font-size: 42px; font-weight: 500; line-height: 45px; color: #FFB303; float: left;">1680 p.</div>-->
                            
                            <button type="button" class="btn btn-primary btn-lg button btn-big btn-green pull-right" data-toggle="modal" data-target="#myModal">
                                <?php echo $l == 'ru' ? "Скачать каталог" : "Download catalog"; ?>
                            </button>

                            <!-- Modal -->
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"><?php echo $l == 'ru' ? "Скачать каталог" : "Download catalog"; ?></h4>
                                  </div>
                                    <form name="personal-data" method="post" id="catalog-get-form">
                                      <div class="modal-body">
                                            <input type="email" name="email" placeholder="Email" size="40" class="download-form" required>
                                            <input type="text" name="full-name" placeholder="Name" size="40" class="download-form" required>
                                            <input type="text" name="company" placeholder="Company" size="40" class="download-form" required>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-link" data-dismiss="modal"><?php echo $l == 'ru' ? "Закрыть" : "Close"; ?></button>
                                        <button type="submit" class="button btn-big btn-green"><?php echo $l == 'ru' ? "Скачать каталог" : "Download catalog"; ?></button>
                                      </div>
                                        <script type="text/javascript">
                                            $(document).on("submit", "#catalog-get-form", function () {
                                                var data = {
                                                    'email'     : $(this).find('input[name=email]').val(),
                                                    'name'      : $(this).find('input[name=full-name]').val(),
                                                    'company'   : $(this).find('input[name=company]').val()
                                                };

                                                $.ajax({
                                                    url: "<?php echo get_template_directory_uri(); ?>/api/mail/",
                                                    method: "POST",
                                                    data: data,
                                                    dataType: "json",
                                                    success: function (answer)
                                                    {
                                                        if (answer['code'] == '200')
                                                        {
                                                            window.location = "http://itw-systems.com/catalog.php?download";
                                                        }
                                                    }
                                                });

                                                return false;
                                            });
                                        </script>
                                    </form>
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
                <div class="border-tab__wrapper tab__navigation">
                    <ul class="tabs__list list-unstyled">
                        <li class="button btn-yellow btn-big pp" data-tab="description"><?php echo $l == 'ru' ? "описание" : "Details"; ?></li>
<!--                        <li class="button btn-green btn-big pp" data-tab="specials">Характеристики</li>-->
<!--                        <li class="button btn-green btn-big pp" data-tab="examples">Примеры проектов</li>-->
                    </ul>

<!--                    <section class="tabs__content">-->
<!--                        <div class="tabs__content__item active" data-tab="description">--><?php //echo $content[1]; ?><!--</div>-->
<!--                        <div class="tabs__content__item" data-tab="specials">--><?php //echo $content[2]; ?><!--</div>-->
<!--                        <div class="tabs__content__item" data-tab="examples">--><?php //echo $content[3]; ?><!--</div>-->
<!--                    </section>-->

                    <section class="tabs__content">
                        <div class="tabs__content__item active" data-tab="description"><?php echo $content[1] . $content[2] . $content[3]; ?></div>
                    </section>
                </div>
            </div>
            <div class="clearfix"></div>
        </section>
    </section>
<?php get_footer(); ?>