<?php /* Template Name: Team [Template] */ ?>
<?php get_header(); ?>
    <section class="mainContent__block block__content">
        <h2 class="block__title">Результаты поиска: <span><?php the_title(); ?></span></h2>
        <section class="mainContainer">
            <section id="primary" class="content-area">
                <div id="content" class="site-content" role="main">
                    <?php
                    if (have_posts())
                    {
                        while (have_posts())
                        {
                            the_post();
                            get_template_part( 'content', 'search' );
                        }
                    }
                    else get_template_part( 'no-results', 'search' );
                    ?>
                </div>
            </section>
        </section>
    </section>
<?php get_footer(); ?>