
<?php get_header(); ?>
<?php  $l = getLangM(); ?>
<?php
	if (have_posts())
         {
         while (have_posts())
          {
           the_post();
           $logoUrl = get_post_custom_values('logo', $post->ID); 
           /*Slider Image */
           // $item_off = get_post_custom_values('item_off', $post->ID); 
           //  $item_on= get_post_custom_values('item_on',$post->ID);
            // $ft_befor= get_post_custom_values('ft_befor',$post->ID);
            // $threeD_befor= get_post_custom_values('threeD_befor',$post->ID);
            // $ft_after= get_post_custom_values('ft_after',$post->ID);
            // $threeD_after= get_post_custom_values('threeD_after',$post->ID);

            /* Item Details */ 
		                         
            $item_detaile_1= get_post_custom_values('item_detaile_1',$post->ID);
            $item_detaile_2= get_post_custom_values('item_detaile_2',$post->ID);
            $item_detaile_3= get_post_custom_values('item_detaile_3',$post->ID);
            $item_detaile_4= get_post_custom_values('item_detaile_4',$post->ID);
            $item_detaile_5= get_post_custom_values('item_detaile_5',$post->ID);
            $item_detaile_6= get_post_custom_values('item_detaile_6',$post->ID);
            $item_detaile_7= get_post_custom_values('item_detaile_7',$post->ID);
            $item_detaile_8= get_post_custom_values('item_detaile_8',$post->ID);
            $item_detaile_9= get_post_custom_values('item_detaile_9',$post->ID);
            $item_detaile_10= get_post_custom_values('item_detaile_10',$post->ID);
            $item_detaile_11= get_post_custom_values('item_detaile_11',$post->ID);
            $item_detaile_12= get_post_custom_values('item_detaile_12',$post->ID);

                           
            /*Taxonomy*/
             $the_types= get_the_terms( $post->ID, 'type' );          
            foreach($the_types as $the_type){
            $the_single_type=$the_type->slug;
            
        }
                        ?>
   <div class="container cf">
		<!-- <div class="block-information-catalog cf">
			<img src="icon-home.png" alt="" class="icon-home">
			<p class="text-catalog"><span class="triangle">Каталог</span><span class="triangle">Светильник линейный</span></p>
		</div> -->

		<div class="block-carousel owl-carousel-one">

			<div id="sync1" class="owl-carousel owl-theme cf">
				<div class="item">
					<div class="block-led">
					</div>
					<div class="block-degree"></div>
					<img  data-href="<?php the_field('imege_slid_2'); ?>" data-src="<?php the_field('imege_slid_1'); ?>" src="<?php the_field('imege_slid_1'); ?>" class="lampe imege_slid_1">
				</div>
				<div class="item">
					<img src="<?php the_field('imege_slid_3'); ?>" alt="#" class="imege_slid_3">
				</div>
				<div class="item">
					<img src="<?php the_field('imege_slid_4'); ?>" alt="#" class="imege_slid_4">
				</div>
				<?php
					  if (( $the_single_type == 'lamp-road' )||($the_single_type == 'lamp-hcs' )||($the_single_type == 'lamp-rec' )){?>
						<div class="item">
							<img src="<?php the_field('imege_slid_5'); ?>" alt="#" class="imege_slid_5">
						</div>
						<div class="item">
							<img src="<?php the_field('imege_slid_6'); ?>" alt="#" class="imege_slid_6">
						</div>
					<?php }?>
			</div>
			<div id="sync2" class="owl-carousel owl-theme cf">
				<div class="item">
					<img src="<?php the_field('imege_slid_1'); ?>"  class="imege_slid_1">
				</div>
				<div class="item">
					<img src="<?php the_field('imege_slid_3'); ?>" alt="#" class="imege_slid_3">
				</div>
				<div class="item">
					<img src="<?php the_field('imege_slid_4'); ?>" alt="#" class="imege_slid_4">
				</div>
			  <?php
			  if(( $the_single_type == 'lamp-road') ||($the_single_type == 'lamp-hcs' )||($the_single_type == 'lamp-rec' )){?>
					<div class="item">
						<img src="<?php the_field('imege_slid_5'); ?>" alt="#" class="imege_slid_5">
				    </div>
				    <div class="item">
				    	<img src="<?php the_field('imege_slid_6'); ?>" alt="#" class="imege_slid_6">
					</div>
			<?php }?>
			</div>
		</div>
		<?php if(($the_single_type !== 'lamp-road') ||($the_single_type !== 'lamp-hcs' )||($the_single_type !== 'lamp-rec' )||($the_single_type !== 'block-alarm' )){?>
		<div class="block-about_led">
			<h2 class="about_led_title"><?php the_title(); ?></h2>
			<p class="about_led_text"><?php the_content();?></p>
			<h3 class="about-criteria-title"><?php echo $l == 'ru' ? "Критерии выбора светильника" : "Criteria for selection of the luminaire"; ?></h3>
			<table class="table-block">
				<tbody>
					<?php if( $the_single_type == 'lamp-streat'){?>
					<tr class="tbody_tr1">
						<td class="tr_1-td_1 tr_4-td-1-first">
							<?php echo $l == 'ru' ? "Тип" : "Type"; ?></td>
							<?php $labels = array();
									$field = get_field_object('tipe_streat');
									$values = get_field('tipe_streat');?>
									<?php foreach ($values as $value) {?>
   									<td class="tr_3-td-2">
									<input type="radio" class="variant" data-name="<?php echo $field['name']; ?>" name="raz" value="<?php echo $value;?>"/><span><?php echo $value;?></span>
   									</td>
									<?php }?>
							<!-- <td colspan="2">
							<div class="dropdown_option">
								<select name="select" id="number-mm">
<!--									--><?php //$labels = array();
//									$field = get_field_object('tipe_streat');
//									$values = get_field('tipe_streat');?>
<!--									--><?php //foreach ($values as $value) {?>
<!--   								<option value="--><?php //echo $value;?><!--">--><?php //echo $value;?><!--</option>-->
<!--									--><?php //}?>
								
<!--								</select>-->
<!--							</div>-->
						</td>

					</tr>
					<?php }?>
					
					<?php if(( $the_single_type == 'lamp-streat') ||($the_single_type !== 'lamp-rast' )){?>
					<tr class="tbody_tr1">
						<td class="tr_1-td_1 tr_4-td-1-first">
							<?php echo $l == 'ru' ? "Длина" : "Length"; ?>, мм</td>
							<td colspan="2">
							<div class="dropdown_option">
								<?php $labels = array();
								$field = get_field_object('length');
								$values = get_field('length');?>
								<select name="select" id="number-mm" class="variant" data-name="<?php echo $field['name']; ?>">
									<?php foreach ($values as $value) {?>
   								<option value="<?php echo $value; ?>"><?php echo $value;?></option>
									<?php }?>
								
								</select>
							</div>
						</td>
						<td></td>
					</tr>
					<?php }?>
					<?php if(( $the_single_type == 'lamp-line-otr') ||($the_single_type == 'lamp-line' )){?>
					<tr class="tbody_tr2">
						<td class="tr_2-td-1 tr_4-td-1-first"><?php echo $l == 'ru' ? "Источник питания" : "Source of power"; ?>:</td>
						<form action="#">
								<?php $labels = array();
									$field = get_field_object('source_power');
									$values = get_field('source_power');?>
									<?php foreach ($values as $value) {?>
   									<td class="tr_3-td-2">
									<input type="radio" name="dva"  class="radio-variants variant" data-name="<?php echo $field['name']; ?>" value="<?php echo $value;?>"/><span><?php echo $value;?></span>
   									</td>
									<?php }?>
									</form>
						<!--  <form action="#">
						<td class="tr_2-td-2">

								<input type="radio" checked="checked" name="raz"/><span><?php echo $l == 'ru' ? "внутренний" : "iexternal"; ?></span>
						</td>
						<td class="tr_2-td-3">

								<input type="radio" name="raz"/><span><span><?php echo $l == 'ru' ? "внешний" : "external"; ?></span>
						</td>
						 </form> -->
					</tr>
					<?php }?>
					<?php if($the_single_type == 'lamp-line-otr' ){?>
					<tr class="tbody_tr3" >
						<td class="tr_3-td-1 tr_4-td-1-first">
							<?php echo $l == 'ru' ? "Отражатель" : "Reflector"; ?>:</td>
							<td colspan="2">
							<div class="dropdown_option">
								<?php $labels = array();
								$field = get_field_object('tip_otroshartel');
								$values = get_field('tip_otroshartel');?>
								<select name="select" id="number-mm" class="variant" data-name="<?php echo $field['name']; ?>">
									<?php foreach ($values as $value) {?>
   								<option value="<?php echo $value;?>"><?php echo $value;?></option>
									<?php }?>
								
								</select>
							</div>
						</td>
						<td></td>
					</tr>
					<?php }?>
					<?php if($the_single_type == 'lamp-line-otr' ){?>
					<tr class="tbody_tr3">
						<td class="tr_3-td-1 tr_4-td-1-first">
							<?php echo $l == 'ru' ? "Тип отражателя" : "Reflector type"; ?>:</td>
							<?php $labels = array();
									$field = get_field_object('tip_otroshartel_d');
									$values = get_field('tip_otroshartel_d');?>
									<?php foreach ($values as $value) {?>
   									<td class="tr_3-td-2">
									<input type="radio" class="variant" name="tree" data-name="<?php echo $field['name']; ?>" value="<?php echo $value;?>"/><span><?php echo $value;?></span>
   									</td>
									<?php }?>
							<!-- <td colspan="2">
							<div class="dropdown_option">
								<select name="select" id="number-mm">
<!--									--><?php //$labels = array();
//									$field = get_field_object('tip_otroshartel_d');
//									$values = get_field('tip_otroshartel_d');?>
<!--									--><?php //foreach ($values as $value) {?>
<!--   								<option value="--><?php //echo $value;?><!--">--><?php //echo $value;?><!--</option>-->
<!--									--><?php //}?>
								
<!--								</select>-->
<!--							</div>-->
						</td>
						<td></td>
					</tr>
					<?php }?>
					<?php if($the_single_type == 'lamp-line' ){?>
					<tr class="tbody_tr3 hide-element">
						<td class="tr_3-td-1 tr_4-td-1-first"><?php echo $l == 'ru' ? "Отражатель" : "Reflector"; ?>:</td>
						<?php $labels = array();
									$field = get_field_object('source_power_in');
									$values = get_field('source_power_in');?>
									<?php foreach ($values as $value) {?>
   									<td class="tr_3-td-2">
									<input type="radio"  class="reflector variant" data-name="<?php echo $field['name']; ?>" name="cheturi" value="<?php echo $value;?>"/><span><?php echo $value;?></span>
   									</td>
									<?php }?>
						<!-- <form action="#">
							<td class="tr_3-td-2">
								<input type="radio" checked="checked"  name="raz"/><span><?php echo $l == 'ru' ? "односторонний" : "unilateral"; ?></span>
							</td>
							<td class="tr_3-td-3">
								<input type="radio" name="raz"/><span><?php echo $l == 'ru' ? "двухсторонний" : "double-sided"; ?></span>						
							</td>
						</form> -->
					</tr>
					<?php }?>
					<?php if(( $the_single_type == 'lamp-line-otr') ||($the_single_type == 'lamp-line' )||($the_single_type == 'lamp-alarm' )){?>
					<tr class="tbody_tr4">
						<td class="tr_4-td-1 tr_4-td-1-first"><?php echo $l == 'ru' ? "Крепеж" : "Fasteners"; ?></td>
						<td colspan="2">
							<div class="dropdown_option">
								<?php $labels = array();
								$field = get_field_object('mount');
								$values = get_field('mount');?>
								<select name="select" id="number-mm" class="variant" data-name="<?php echo $field['name']; ?>">
									<?php foreach ($values as $value) {?>
   								<option value="<?php echo $value;?>"><?php echo $value;?></option>
									<?php }?>
								
								</select>
							</div>
						</td>
						<td></td>
					</tr>
					<?php }?>
					<?php if( $the_single_type == 'lamp-rast'){?>
					<tr class="tbody_tr1">
						<td class="tr_1-td_1 tr_4-td-1-first">
							<?php echo $l == 'ru' ? "Тип" : "Type"; ?></td>
							<?php $labels = array();
									$field = get_field_object('tipe_rast');
									$values = get_field('tipe_rast');?>
									<?php foreach ($values as $value) {?>
   									<td class="tr_3-td-2">
									<input type="radio" class="variant" data-name="<?php echo $field['name']; ?>" name="piyt6" value="<?php echo $value;?>"/><span><?php echo $value;?></span>
   									</td>
									<?php }?>
							<!-- <td colspan="2">
							<div class="dropdown_option">
								<select name="select" id="number-mm">
<!--									--><?php //$labels = array();
//									$field = get_field_object('tipe_rast');
//									$values = get_field('tipe_rast');?>
<!--									--><?php //foreach ($values as $value) {?>
<!--   								<option value="--><?php //echo $value;?><!--">--><?php //echo $value;?><!--</option>-->
<!--									--><?php //}?>
								
<!--								</select>-->
<!--							</div>-->
						</td>
						<td></td>
					</tr>
					<?php }?>
					<?php if( $the_single_type == 'lamp-rast') {?>
					<tr class="tbody_tr4">
						<td class="tr_4-td-1 tr_4-td-1-first">
							<?php echo $l == 'ru' ? "Тип исполнения" : "Type of execution"; ?></td>
							<?php $labels = array();
									$field = get_field_object('tipe_lamp_rast');
									$values = get_field('tipe_lamp_rast');?>
									<?php foreach ($values as $value) {?>
   									<td class="tr_3-td-2">
									<input type="radio" name="shest6" class="variant" data-name="<?php echo $field['name']; ?>" value="<?php echo $value;?>"/><span><?php echo $value;?></span>
   									</td>
									<?php }?>
							<!-- <td colspan="2">
							<div class="dropdown_option">
								<select name="select" id="number-mm">
<!--									--><?php //$labels = array();
//									$field = get_field_object('tipe_lamp_rast');
//									$values = get_field('tipe_lamp_rast');?>
<!--									--><?php //foreach ($values as $value) {?>
<!--   								<option value="--><?php //echo $value;?><!--">--><?php //echo $value;?><!--</option>-->
<!--									--><?php //}?>
								
<!--								</select>-->
<!--							</div>-->
						</td>

					</tr>
					<?php }
					$item_catalog= get_post_custom_values('item_catalog',$post->ID);
					?>
				</tbody>
			</table>
			<div class="block-download_information">
				<a href="<?php the_field('download_description'); ?>" class="link_information link_catalog" download><?php echo $l == 'ru' ? "скачать описание " : "download description"; ?></a>
				<a href="<?php echo get_template_directory_uri(); ?>/upload/<?php echo $item_catalog[0]; ?>" class="link_information link_description" download><?php echo $l == 'ru' ? "скачать каталог" : "download catalog"; ?></a>
				<a href="<?php the_field('download_ies'); ?>" class="link_information link_ies" download><?php echo $l == 'ru' ? "скачать ies" : "download ies"; ?></a>
			</div>
		</div>
		<?php }?>
	</div>
	<div class="presentation">
		<div class="block_slider middle-block cf">
			<div class="block-carousel middle">
				<div class="background_carusel">
				<div id="sync1" class="owl-carousel owl-theme owl-carousel-3 cf ">
					<div class="item">
						<img src="<?php the_field('single_middle_1'); ?>" alt="#" class="single_middle_1">
					</div>
					<div class="item">
						<img src="<?php the_field('single_middle_2'); ?>" alt="#" class="single_middle_2">
					</div>
					<div class="item">
						<img src="<?php the_field('single_middle_3'); ?>" alt="#" class="single_middle_3">
					</div>
					<div class="item">
						<img src="<?php the_field('single_middle_4'); ?>" alt="#" class="single_middle_4">
					</div>
					<div class="item">
						<img src="<?php the_field('single_middle_5'); ?>" alt="#" class="single_middle_5">
					</div>
					<div class="item">
						<img src="<?php the_field('single_middle_6'); ?>" alt="#" class="single_middle_6">
					</div>
					<div class="item">
						<img src="<?php the_field('single_middle_7'); ?>" alt="#" class="single_middle_7">
					</div>
				</div>
				<div id="sync2" class="owl-carousel owl-theme owl-carousel-4 cf bottom-car">
				  	<div class="item">
						<img src="<?php the_field('single_middle_1'); ?>" alt="#" class="single_middle_1">
					</div>
					<div class="item">
						<img src="<?php the_field('single_middle_2'); ?>" alt="#" class="single_middle_2">
					</div>
					<div class="item">
						<img src="<?php the_field('single_middle_3'); ?>" alt="#" class="single_middle_3">
					</div>
					<div class="item">
						<img src="<?php the_field('single_middle_4'); ?>" alt="#" class="single_middle_4">
					</div>
					<div class="item">
						<img src="<?php the_field('single_middle_5'); ?>" alt="#" class="single_middle_5">
					</div>
					<div class="item">
						<img src="<?php the_field('single_middle_6'); ?>" alt="#" class="single_middle_6">
					</div>
					<div class="item">
						<img src="<?php the_field('single_middle_7'); ?>" alt="#" class="single_middle_7">
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>
	<div class="container">
		<div class="block-iformation-about-caruusel">
			<h3 class="block-iformation-about-title"><?php echo $l == 'ru' ? "характеристики" : "characteristics"; ?></h3>
			<ul class="block-iformation-about-item">
				<li class="block-iformation-about-link cf">
					<span class="block-iformation-about-link-left"><?php echo $l == 'ru' ? "Производитель светодиодов" : "LED Manufacturer"; ?>:</span>
					<span class="block-iformation-about-link-right item_detaile_1"><?php echo $item_detaile_1[0]; ?></span>
				</li>
				<li class="block-iformation-about-link cf">
					<span class="block-iformation-about-link-left"><?php echo $l == 'ru' ? "Плафон" : "Plafond"; ?>:</span>
					<span class="block-iformation-about-link-right item_detaile_2"><?php echo $item_detaile_2[0]; ?></span>
				</li>
				<li class="block-iformation-about-link cf">
					<span class="block-iformation-about-link-left"><?php echo $l == 'ru' ? "Степень защиты" : "Degree of protection"; ?>:</span>
					<span class="block-iformation-about-link-right item_detaile_3"><?php echo $item_detaile_3[0]; ?></span>
				</li>
				<li class="block-iformation-about-link cf">
					<span class="block-iformation-about-link-left"><?php echo $l == 'ru' ? "Источник питания" : "Source of power"; ?>:</span>
					<span class="block-iformation-about-link-right item_detaile_4"><?php echo $item_detaile_4[0]; ?></span>
				</li>
				<li class="block-iformation-about-link cf">
					<span class="block-iformation-about-link-left"><?php echo $l == 'ru' ? "Коэффициент мощности" : "Power factor"; ?>:</span>
					<span class="block-iformation-about-link-right item_detaile_5"><?php echo $item_detaile_5[0]; ?></span>
				</li>
				<li class="block-iformation-about-link cf">
					<span class="block-iformation-about-link-left"><?php echo $l == 'ru' ? "Оптическая система" : "Optical system"; ?>:</span>
					<span class="block-iformation-about-link-right item_detaile_6"><?php echo $item_detaile_6[0]; ?></span>
				</li>
				<li class="block-iformation-about-link cf">
					<span class="block-iformation-about-link-left"><?php echo $l == 'ru' ? "Температура эксплуатации" : "Operating temperature"; ?>:</span>
					<span class="block-iformation-about-link-right item_detaile_7"><?php echo $item_detaile_7[0]; ?></span>
				</li>
				<li class="block-iformation-about-link cf">
					<span class="block-iformation-about-link-left"><?php echo $l == 'ru' ? "(тяжелых условий эксплуатации)" : "(Heavy Duty)"; ?> </span>
					<span class="block-iformation-about-link-right item_detaile_8"><?php echo $item_detaile_8[0]; ?></span>
				</li>
				<li class="block-iformation-about-link cf">
					<span class="block-iformation-about-link-left"><?php echo $l == 'ru' ? "сВозможный выбор длины" : "Possible choice of length"; ?>:</span>
					<span class="block-iformation-about-link-right item_detaile_9"><?php echo $item_detaile_9[0]; ?></span>
				</li>
				<li class="block-iformation-about-link cf">
					<span class="block-iformation-about-link-left"><?php echo $l == 'ru' ? "Световый поток" : "luminous flux"; ?>:</span>
					<span class="block-iformation-about-link-right item_detaile_10"><?php echo $item_detaile_10[0]; ?></span>
				</li>
				<li class="block-iformation-about-link cf">
					<span class="block-iformation-about-link-left"><?php echo $l == 'ru' ? "Мощность" : "Power"; ?></span>
					<span class="block-iformation-about-link-right item_detaile_11"><?php echo $item_detaile_11[0]; ?></span>
				</li>
				<li class="block-iformation-about-link cf">
					<span class="block-iformation-about-link-left"><?php echo $l == 'ru' ? "Расположение источника питания" : "Location power source"; ?>:</span>
					<span class="block-iformation-about-link-right item_detaile_12"><?php echo $item_detaile_12[0]; ?></span>
				</li>
			</ul>
		</div>
	</div>

        <?php }
    } else { ?><p>No items</p> <?php } ?>

	<script>
		function hideElement(element='Внутренний') {
			$('.radio-variants').each(function (i, elem){
				if (elem.checked && elem.value  === element) {
					$('.reflector').prop('checked', false);
					$('.hide-element').hide();
				} else if(elem.checked && elem.value  !== element) {
					$('.hide-element').show();
				}
			});
		};

		$(document).on('change', ".variant", function () {
			hideElement('Внутренний');
			var dict = {};
			$('.variant').each (function (i,elem) {
				var $this = $(this);
					if (elem.type === 'radio' && elem.checked) {
						dict[$this.attr("data-name")] = elem.value;
					} else if (elem.type !== 'radio') {
						dict[$this.attr("data-name")] = elem.value;
					}

			});
			dict['taxonomy'] = "<?php echo $the_single_type ?>";
			console.log('Before ajax')
			console.log(dict);



			$.ajax({
				type: 'POST',
				url: "<?php echo get_home_url() ?>/slim/api/object/",

				data: dict,
				dataType: 'json',
				success: function (response) {
					$('.imege_slid_1').attr('data-src', response['imege_slid_1']);
					$('.imege_slid_1').attr('data-href', response['imege_slid_2']);


					$('.imege_slid_1').attr('src', response['imege_slid_1']);
//					$('.imege_slid_2').attr('src', response['imege_slid_2']);
					$('.imege_slid_3').attr('src', response['imege_slid_3']);
					$('.imege_slid_4').attr('src', response['imege_slid_4']);
					$('.imege_slid_5').attr('src', response['imege_slid_5']);
					$('.imege_slid_6').attr('src', response['imege_slid_6']);
					$('.link_description').attr('href', response['download_description']);
					$('.link_catalog').attr('href', response['download_catalog']);
					$('.link_ies').attr('href', response['download_ies']);

					for (i = 1; i <= 12; i++) {
						var clas = '.item_detaile_'+i;
						var data = 'item_detaile_'+i;
						$(clas).text(response[data]);
					}

					for (i = 1; i <= 7; i++) {
						var clas = '.single_middle_'+i;
						var data = 'single_middle_'+i;
						$(clas).text(response[data]);
					}
//					  response['imege_slid_1']
					console.log('After ajax')
					console.log(response);
				}
			});
		});

	</script>


<?php get_footer(); ?>