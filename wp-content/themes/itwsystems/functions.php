<?php
require_once('project_post.php');
require_once('item_post.php');
require_once('breadcrumbs.php');
add_theme_support('post-thumbnails');
register_nav_menus(array('id' => __('mainMenu')));

function getLangM ()
{
    // $current_pack_uri = array_filter(explode('/', $_SERVER['REQUEST_URI']));
    // return $current_pack_uri[1] == $_COOKIE['qtrans_front_language'] ? $_COOKIE['qtrans_front_language'] : $current_pack_uri[1];
    if (isset ($_COOKIE['qtrans_front_language'])) $current_language = $_COOKIE['qtrans_front_language'];
    else $current_language = 'en';

    $uri = explode ('/', $_SERVER['REQUEST_URI']);
    $current_language = $uri[1];

    if (isset ($array[$current_language])) return $array[$current_language];
    return $current_language;
}

function get_custom_cat_template ($single_template)
{
    global $post;

    if (in_category('blog')) $single_template = dirname( __FILE__ ) . '/single-blog.php';
    if (in_category('news')) $single_template = dirname( __FILE__ ) . '/single-news.php';
    if (in_category('projects')) $single_template = dirname( __FILE__ ) . '/single-project.php';
    if (in_category('products')) $single_template = dirname( __FILE__ ) . '/single-product.php';

    return $single_template;
}

function strcrop ($string, $limit = 0, $break_words = false, $etc = '...')
{
    if($limit == 0) return '';

    if(strlen($string) <= $limit) return $string;

    if($break_words) $string = rtrim(substr($string, 0, $limit)) . $etc;
    else
    {
        $string = explode('<!--more-->', wordwrap($string, $limit, '<!--more-->', false));
        $string = $string[0];
        $string = $string . $etc;
    }

    return $string;
}

function pagination ($pages = '', $range = 5)
{
    $showitems = ($range * 2) + 1;

    global $paged;
    if (empty($paged)) $paged = 1;

    if ($pages == '') {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if (!$pages) {
            $pages = 1;
        }
    }

    if (1 != $pages) {
        echo "<div class=\"pagination\">";

        if ($paged > 1 && $showitems < $pages) echo "<a href='" . get_pagenum_link($paged - 1) . "'><i class=\"fa fa-chevron-left\"></i></a>";

        for ($i = 1; $i <= $pages; $i++) {
            if (1 != $pages && (!($i >= $paged + $range + 1 || $i <= $paged - $range - 1) || $pages <= $showitems)) {
                if ($paged == $i) echo "<span class=\"button btn-yellow\">" . $i . "</span>";
                else echo "<a class=\"button btn-green\" href='" . get_pagenum_link($i) . "' >" . $i . "</a>";
            }
        }

        if ($paged < $pages && $showitems < $pages) echo "<a href=\"" . get_pagenum_link($paged + 1) . "\"><i class=\"fa fa-chevron-right\"></i></a>";
        echo "</div>";
    }
}

function getPostViews ($postID)
{
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);

    if ($count == '')
    {
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0";
    }

    return $count;
}

function setPostViews ($postID)
{
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);

    if ($count == '')
    {
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }
    else
    {
        $count++;
        update_post_meta($postID, $count_key, $count);
    }

    return true;
}

function itw_comment ($comment, $args, $depth)
{
    $GLOBALS['comment'] = $comment;
    extract($args, EXTR_SKIP);

    if ('div' == $args['style'])
    {
        $tag = 'div';
        $add_below = 'comment';
    } else {
        $tag = 'li';
        $add_below = 'div-comment';
    }
    ?>
    <<?php echo $tag ?> <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID() ?>">

    <?php if ( $comment->comment_approved == '0' ) : ?>
    <em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.' ); ?></em>
    <?php endif; ?>

    <?php if ( 'div' != $args['style'] ) : ?>
    <div id="div-comment-<?php comment_ID() ?>" class="comment-body">
    <?php endif; ?>

        <div class="comment-author vcard">
            <?php if ( $args['avatar_size'] != 0 ) echo '<img src="' . get_template_directory_uri() . '/images/icon_manager.png" />'; ?>
        </div>

        <div class="comment-text">
            <div class="comment-author">
                <?php printf( __('<strong>%s</strong>'), get_comment_author_link()); ?>
                <?php edit_comment_link( __( '(Edit)' ), '  ', '' ); ?>
            </div>
            <?php comment_text(); ?>
            <div class="clearfix"></div>
            <div class="comment-meta commentmetadata">
                <a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ); ?>">
                    <?php printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time() ); ?>
                </a>
            </div>
            <div class="comment-reply">
                <?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    <?php if ( 'div' != $args['style'] ) : ?></div><?php endif; ?>
    <?php
}

// Remove issues with prefetching adding extra views
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

add_filter( "single_template", "get_custom_cat_template" ) ;

/**
 * @param $phrase
 * @return mixed
 *
 * Language file
 */
function getLangPartOf ($phrase)
{
    $lang_list = array();

    if (isset($lang_list[getLang(true)][$phrase]))
        return $lang_list[getLang(true)][$phrase];

    return $phrase;
}

function itw_customize_register( $wp_customize ) {
    $wp_customize->add_section(
        'data_site_section',
        array(
            'title' => 'Настройка шрифта',
            'capability' => 'edit_theme_options',
            'description' => "Настройки шрифта сайта"
        )
    );

    $wp_customize->add_setting(
        'theme_fontslh',
        array(
            'default' => '14px/20px',
            'type' => 'option'
        )
    );

    $wp_customize->add_control(
        'theme_contacttext_control',
        array(
            'type' => 'text',
            'label' => "Размер шрифта (размер шрифта/высота строки)",
            'section' => 'data_site_section',
            'settings' => 'theme_fontslh'
        )
    );
}

add_action( 'customize_register', 'itw_customize_register' );

/*custome*/
function getField($field_name, $id) {
        return $field_name.$id;   
     }

/*Add Theme Option Page*/
add_action( 'admin_init', 'theme_options_init' );
add_action( 'admin_menu', 'theme_options_add_page' );
 
function theme_options_init(){
register_setting( 'wpuniq_options', 'wpuniq_theme_options');
}
 
function theme_options_add_page() {
add_theme_page( __( 'Настройки Theme', 'WP-Unique' ), __( 'Настройки Theme', 'WP-Unique' ), 'edit_theme_options', 'theme_options', 'theme_options_do_page' );
}
function theme_options_do_page() { global $select_options; if ( ! isset( $_REQUEST['settings-updated'] ) ) $_REQUEST['settings-updated'] = false;
?>
 
<div class="wrap">
<?php screen_icon(); echo "<h2>". __( 'Настройки Theme', 'WP-Unique' ) . "</h2>"; ?>
<?php if ( false !== $_REQUEST['settings-updated'] ) : ?>
<div id="message" class="updated">
<p><strong><?php _e( 'Настройки сохранены', 'WP-Unique' ); ?></strong></p>
</div>
<?php endif; ?>
</div>
 
<form method="post" action="options.php">
<?php settings_fields( 'wpuniq_options' ); ?>
<?php $options = get_option( 'wpuniq_theme_options' ); ?>
<table width="600" border="0">
     <tr>
        <td>Вставте link PSD типовые решения </td>
        <td><input style="width:500px;" type="text" name="wpuniq_theme_options[download_project]" id="wpuniq_theme_options[download_project]" value="<?php echo $options[download_project];?>"  /></td>
    </tr>
    <tr>
        <td>Вставте link PSD каталог продуктов </td>
        <td><input style="width:500px;" type="text" name="wpuniq_theme_options[download_catalog]" id="wpuniq_theme_options[download_catalog]" value="<?php echo $options[download_catalog];?>"  /></td>
    </tr>
    <tr>
        <td>Vkontacte</td>
        <td><input style="width:300px;" type="text" name="wpuniq_theme_options[vkontacte]" id="wpuniq_theme_options[vkontacte]" value="<?php echo $options[vkontacte];?>" /></td>
    </tr>
    <tr>
        <td>Odnoklasniki</td>
        <td><input style="width:300px;" type="text" name="wpuniq_theme_options[odnoklasniki]" id="wpuniq_theme_options[odnoklasniki]" value="<?php echo $options[odnoklasniki];?>" /></td>
    </tr>
    <tr>
        <td>Facebook</td>
        <td><input style="width:300px;" type="text" name="wpuniq_theme_options[facebook]" id="wpuniq_theme_options[facebook]" value="<?php echo $options[facebook];?>" /></td>
    </tr>
    <tr>
        <td>Twitter</td>
        <td><input style="width:300px;" type="text" name="wpuniq_theme_options[twitter]" id="wpuniq_theme_options[twitter]" value="<?php echo $options[twitter];?>" /></td>
   </tr>
     <tr>
        <td>Telefone</td>
        <td><input style="width:300px;" type="text" name="wpuniq_theme_options[telefone]" id="wpuniq_theme_options[telefone]" value="<?php echo $options[telefone];?>" /></td>
    </tr>
    <tr>
        <td>E-mail</td>
        <td><input style="width:300px;" type="text" name="wpuniq_theme_options[email]" id="wpuniq_theme_options[email]" value="<?php echo $options[email];?>" /></td>
    </tr>
    <tr>
        <td>Text Footer Rus (введите текст без ")</td>
        <td><input style="width:500px;" type="text" name="wpuniq_theme_options[textfooterrus]" id="wpuniq_theme_options[textfooterrus]" value="<?php echo $options[textfooterrus];?>"  /></td>
    </tr>
    <tr>
        <td>Text Footer Eng (введите текст без ")</td>
        <td><input style="width:500px;" type="text" name="wpuniq_theme_options[textfootereng]" id="wpuniq_theme_options[textfootereng]" value="<?php echo $options[textfootereng];?>"  /></td>
    </tr>
    <tr>
        <td colspan="2"><input type="submit" value="Применить" /></td>
    </tr>
</table>
</form>
 
<?php
}  

