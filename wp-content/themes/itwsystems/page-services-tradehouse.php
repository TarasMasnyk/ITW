<?php /* Template Name: Service Trade House [Template] */ ?>
<?php get_header(); ?>
    <section class="mainContent__block block__content">
    	<div class="branches-slider text">
			<ul class="bxslider">
				<li>
					<img src="http://itw-systems.com/wp-content/uploads/2015/10/IMG_8170-1024x683.jpg" alt="" />
					<p class="text-center"></p>
				</li>
			</ul>
		</div>
    	
    	<?php if (have_posts()):
		while (have_posts()): the_post();?>
    	
        <section class="mainContainer text"><?php the_content(); ?></section>
	        <?php endwhile;
			else :
			echo '<p>No content found</p>';
			endif;?>
    	</section>
<?php get_footer(); ?>