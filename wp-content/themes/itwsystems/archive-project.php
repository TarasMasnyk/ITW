<?php get_header(); ?>
<?php $options = get_option( 'wpuniq_theme_options' ); ?>
    <section class="mainContent_Slider custom_style" id="top_slider">
        <ul class="mainContent_Slider_list slideme list-unstyled project_slider">
             
            <?php $l = getLangM(); 
             
            $args = array( 'post_type' => 'project' );
            $myposts = get_posts( $args );
                foreach ($myposts as $post) :setup_postdata($post);  
                $attachmentId = get_post_thumbnail_id($post->ID);
                $thymbUrl = wp_get_attachment_url($attachmentId, 'full', true); // берем URL миниатюры, которая относится к записи                     
                            ?>
                            <li data-sliderID="<?php echo $slider; ?>" class="list_style">
                                 <div class="mainContainer top_slider_style">
                                    <h2 class="abstract-title" ><?php the_title(); ?></h2>
                                    <div class="project_content"><?php echo the_content(); ?> 
                                    <a href="<?php echo the_permalink(); ?>"><p class="more_btn"><?php echo $l == 'ru' ? "Подробнее" : "More"; ?></p></a>
                                    </div> 
                                </div>
                                <div class="img_hover_project">
                                    <img src="<?php the_field('slider_1_image'); ?>" >
                                </div>
                            </li>
                            
             <?php endforeach;
             wp_reset_postdata();?>
        </ul>
    </section>
     <section class="mainContent__block block__content projects">
        <h2 class="block__title"><?php echo $l == 'ru' ? "Наши проекты" : "Our Projects"; ?></h2>
           <?php
             // берем все имеющиеся термы (категории нашего портфолио) из таксономии category_portfolio
             $categories = get_terms( 'object', array(
             'orderby'    => 'count', // в начале показывает категории с большим количеством работ
            'hide_empty' => true, // скрывает пустые категории
              ) );?>
        
        <div class="mainContainer">
            <aside class="project__filter__categories">
                <ul class="project__filter__categories__list list-unstyled">
                    <li class="active" data-category="all"><span><?php echo $l == 'ru' ? "Все проекты" : "All projects"; ?></span></li>
                    <li data-category="warehouses"><span><?php echo $l == 'ru' ? "Складское освещение" : "Warehouse lighting"; ?></span></li>
                     <li data-category="industrial"><span><?php echo $l == 'ru' ? "Промышленное освещение" : "Industrial lighting"; ?></span></li>
                     <li data-category="trade"><span><?php echo $l == 'ru' ? "Торговое освещение" : "Retail lighting"; ?></span></li>
                     <li data-category="office"><span><?php echo $l == 'ru' ? "Офисное освещение" : "Office lighting"; ?></span></li>
                     <li data-category="outdoor"><span><?php echo $l == 'ru' ? "Уличное освещение" : "Outdoor lighting"; ?></span></li>   
                     <li class="public" data-category="public"><span><?php echo $l == 'ru' ? "Жилищно-коммунальное освещение" : "Housing and public lighting"; ?></span></li>
                  <!-- <?php 
                   
                    if ($categories)
                    {
                        foreach ($categories as $category)
                        {
                            ?><li data-category="<?php echo $category->slug; ?>"><span><?php echo $category->name; ?></span></li><?php
                        }
                    }
                    ?>-->
                   <a href="<?php echo $options[download_project]; ?>" download><li class="btn_download"> скачать типовые решения</li></a> 
                </ul>
            </aside>
            
                    <div class="project__items">
                    <?php  if ( have_posts()):
                        while (have_posts()): the_post();
                            $attachmentId = get_post_thumbnail_id($post->ID);
                            $thymbUrl = wp_get_attachment_url($attachmentId, 'full', true); // берем URL миниатюры, которая относится к записи
                            $the_filter_cats= get_the_terms( $post->ID, 'object' );
                            foreach($the_filter_cats as $the_filter_cat){
                            $the_single_cat =$the_filter_cat;
                            }
                                            
                             ?>            
                                <article class="project__article" data-category="<?php echo $the_single_cat->slug; ?>">
                                    <a href="<?php the_permalink() ?>">
                                        <img src="<?php echo $thymbUrl;?>" />
                                        <p><?php the_title(); ?></p>
                                        <span><?php echo $the_single_cat->name; ?></span>
                                        <?php  $the_single_cat=' ';?>
                                    </a>
                                </article>         
                        <?php endwhile;

                        else :
                        echo '<p>No content found</p>';
                        endif;
                         get_template_part( 'content-bottom', get_post_format() );
                        ?>          
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </section>
<?php get_footer(); ?>