<?php /* Template Name: Category [Template] */ ?>
<?php get_header(); ?>
    <section class="mainContent__block block__content catalog">
        <h2 class="block__title"><?php echo get_cat_name(get_query_var('cat')); ?></h2>
        <section class="mainContainer">
            <aside class="catalog__block">
                <ul class="catalog__categories list-unstyled">
                    <li><a href="/catalog/"><span class="fa fa-lightbulb-o"></span> <?php echo getLangPartOf("Все товары"); ?></a></li>
                    <?php
                    $category_id = 11;

                    $args = array(
                        'type'                     => 'post',
                        'child_of'                 => $category_id,
                        'orderby'                  => 'name',
                        'order'                    => 'ASC',
                        'number'                   => 0,
                        'taxonomy'                 => 'category',
                        'hide_empty'               => 0
                    );
                    $categories = get_categories($args);
                    if ($categories)
                    {
                        foreach ($categories as $category)
                        {
                            if ($category->cat_ID == get_query_var('cat'))
                            {
                                ?><li class="current" data-category="<?php echo $category->slug; ?>"><span class="fa fa-<?php echo $category->slug; ?>"></span> <?php echo $category->name; ?></li><?php
                            }
                            else
                            {
                                ?><li data-category="<?php echo $category->slug; ?>"><a href="/category/products/<?php echo $category->slug; ?>/"><span class="fa fa-<?php echo $category->slug; ?>"></span> <?php echo $category->name; ?></a></li><?php
                            }
                        }
                    }
                    ?>
                </ul>

                <div class="catalog__filter">
                    <h4 class="text-center">Подбор по параметрам</h4>
                    <hr />
                    <div class="catalog__filter__param">
                        <div class="catalog__filter__param__title">Мощьность, Вт:</div>
                        <input type="text" class="range_catalog_filter_vt" name="filter_vt"
                               data-min="0" data-max="120" data-from="60" data-to="75" data-postfix=" Вт" />
                    </div>
                    <hr />
                    <div class="catalog__filter__param">
                        <div class="catalog__filter__param__title">Мощьность, Лм:</div>
                        <input type="text" class="range_catalog_filter_lm" name="filter_lm"
                               data-min="0" data-max="4200" data-from="110" data-to="2600" data-postfix=" Лм" />
                    </div>
                    <hr />
                    <div class="catalog__filter__param">
                        <div class="catalog__filter__param__title">Цена:</div>
                        <input type="text" class="range_catalog_filter_price" name="filter_price" />
                    </div>
                </div>

            </aside>
            <div class="catalog__content products">
                <?php
                $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

                $args = array(
                    'posts_per_page'    => 12,
                    'cat'               => get_query_var('cat'),
                    'orderby'           => 'date',
                    'order'             => 'ASC',
                    'paged'             => $paged
                );

                $custom_query = new WP_Query( $args );

                while($custom_query->have_posts())
                {
                    $custom_query->the_post();
                    $thumbnail_attributes = wp_get_attachment_image_src(get_post_thumbnail_id(), 'source');
                    $custom_fields = get_post_custom();

                    if (preg_match("#-" . $_COOKIE['itw_lang'] . "$#", $post->post_name) == 1)
                    {
                        ?>
                        <product class="products__item"
                                 data-lm="<?php echo $custom_fields['product_light_lm'][0]; ?>"
                                 data-vt="<?php echo $custom_fields['product_light_vt'][0]; ?>"
                                 data-price="<?php echo $custom_fields['product_price'][0]; ?>">
                            <figure><img src="<?php echo $thumbnail_attributes[0]; ?>" /></figure>
                            <div class="products__item__title"><?php the_title(); ?></div>
                            <div class="products__item__data">
                                <span><i class="fa fa-bolt"></i> <?php echo $custom_fields['product_light_vt'][0]; ?> Вт</span>
                                <span><i class="fa fa-lightbulb-o"></i> <?php echo $custom_fields['product_light_lm'][0]; ?> лм</span>
                            </div>
                            <hr />
                            <div class="products__item__actions">
                                <span class="products__item__price pull-left"><?php echo $custom_fields['product_price'][0]; ?> р.</span>
                                <a href="<?php the_permalink() ?>" class="products__item__buy button btn-green pull-right" data-product-id="1">Подробнее</a>
                                <div class="clearfix"></div>
                            </div>
                        </product>
                        <?php
                    }
                }

                wp_reset_query();
                ?>
                <div class="clearfix"></div>
                <?php
                pagination($custom_query->max_num_pages);
                ?>
            </div>
            <div class="clearfix"></div>
        </section>
    </section>
<?php get_footer(); ?>